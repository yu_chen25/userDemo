package com.userdemo.demo.service.base.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.userdemo.demo.base.BaseService;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.constant.base.Const;
import com.userdemo.demo.entity.base.PaParameter;
import com.userdemo.demo.entity.base.PaParameterSort;
import com.userdemo.demo.entity.base.dto.PaParameterDTO;
import com.userdemo.demo.entity.base.dto.PaParameterSortDTO;
import com.userdemo.demo.entity.base.vo.BaseUserVO;
import com.userdemo.demo.entity.base.vo.PaParameterSortVO;
import com.userdemo.demo.entity.base.vo.PaParameterVO;
import com.userdemo.demo.service.base.IPaParameterService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 　　* @description: 枚举服务层
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
@Service("iPaParameterService")
public class PaParameterServiceImpl extends BaseService implements IPaParameterService {

    /**
     * 新增枚举类型
     *
     * @param paParameterSortDTO
     * @param baseUserVO
     * @return
     */
    @Override
    public ServerResponse<String> addOrUpdateParameterSort(PaParameterSortDTO paParameterSortDTO, BaseUserVO baseUserVO) {
        if(null == paParameterSortDTO || null == baseUserVO || null == paParameterSortDTO.getPapaId()){
            return ServerResponse.createByErrorMessage("参数错误");
        }
        PaParameterSort paParameterSort = null;
        //判断papaId是否存在
        if(paParameterSortMapper.countNumByPapaId(paParameterSortDTO.getPapaId()) > 0){
           //有则更新
            paParameterSort = paParameterSortMapper.selectByPrimaryKey(paParameterSortDTO.getPapaId());
        }
        boolean isAdd = false;
        if(null == paParameterSort){
            //不存在则新增
            paParameterSort = new PaParameterSort();
            isAdd = true;
        }
        paParameterSort.setPapaName(paParameterSortDTO.getPapaName());
        paParameterSort.setPapaDesc(paParameterSortDTO.getPapaDesc());
        paParameterSort.setPapaType(paParameterSortDTO.getPapaType());
        if(isAdd){
            paParameterSort.setPapaId(paParameterSortDTO.getPapaId());
            if(paParameterSortMapper.insert(paParameterSort) > 0){
                return ServerResponse.createBySuccessMessage("新增成功");
            }
        }else{
            if(paParameterSortMapper.updateByPrimaryKeySelective(paParameterSort) > 0){
                return ServerResponse.createBySuccessMessage("更新成功");
            }
        }
        return ServerResponse.createByErrorMessage("操作失败");
    }

    /**
     * 删除枚举类型
     *
     * @param paParameterSortDTO
     * @param baseUserVO
     * @return
     */
    @Override
    public ServerResponse<String> delParameterSortByPapaId(PaParameterSortDTO paParameterSortDTO, BaseUserVO baseUserVO) {
        if(null == paParameterSortDTO || null == baseUserVO || null == paParameterSortDTO.getPapaId()){
            return ServerResponse.createByErrorMessage("参数错误");
        }
        //先删除枚举参数在删除枚举类型
        paParameterMapper.delByPapaId(paParameterSortDTO.getPapaId());
        //删除枚举类型
        if(paParameterSortMapper.deleteByPrimaryKey(paParameterSortDTO.getPapaId()) > 0){
            return ServerResponse.createBySuccessMessage("删除成功");
        }
        return ServerResponse.createByErrorMessage("删除失败");
    }

    /**
     * 查询枚举类型列表
     *
     * @param paParameterSortDTO
     * @param baseUserVO
     * @return
     */
    @Override
    public ServerResponse<PageInfo> selectListPaParameterSort(PaParameterSortDTO paParameterSortDTO, BaseUserVO baseUserVO) {
        if(null == paParameterSortDTO || null == baseUserVO){
            return ServerResponse.createByErrorMessage("参数错误");
        }
        if(null == paParameterSortDTO.getPageNum()){
            paParameterSortDTO.setPageNum(Const.INITPAGENUM);
        }
        if(null == paParameterSortDTO.getPageSize()){
            paParameterSortDTO.setPageSize(Const.INITPAGESIZE);
        }
        PageHelper.startPage(paParameterSortDTO.getPageNum(),paParameterSortDTO.getPageSize(),true,false,null);
        PageHelper.orderBy(" papa_id DESC");
        List<PaParameterSortVO> list = paParameterSortMapper.selectListPaParameterSort(paParameterSortDTO);
        if(null == list){
            return ServerResponse.createByErrorMessage("没有数据");
        }
        return ServerResponse.createBySuccess("查询成功",new PageInfo(list));
    }

    /**
     * 查询单个枚举类型
     *
     * @param paParameterSortDTO
     * @param baseUserVO
     * @return
     */
    @Override
    public ServerResponse<PaParameterSortVO> selectParameterSortByPapaId(PaParameterSortDTO paParameterSortDTO, BaseUserVO baseUserVO) {
        if(null == paParameterSortDTO || null == baseUserVO || null == paParameterSortDTO.getPapaId()){
            return ServerResponse.createByErrorMessage("参数错误");
        }
        PaParameterSortVO parameterSortVO = paParameterSortMapper.selectParameterSortByPapaId(paParameterSortDTO.getPapaId());
        if(null == parameterSortVO){
            return ServerResponse.createByErrorMessage("没有数据");
        }
        return ServerResponse.createBySuccess("查询成功",parameterSortVO);
    }

    /**
     * 新增/更新枚举参数
     *
     * @param paParameterDTO
     * @param baseUserVO
     * @return
     */
    @Override
    public ServerResponse<String> addOrUpdateParameter(PaParameterDTO paParameterDTO, BaseUserVO baseUserVO) {
        if(null == paParameterDTO || null == baseUserVO || null == paParameterDTO.getPapaId()
                || null == paParameterDTO.getParaId()){
            return ServerResponse.createByErrorMessage("参数错误");
        }
        PaParameter parameter = null;
        boolean isAdd = false;
        //判断paraId 是否存在
        if(paParameterMapper.countNumByParaIdAndPapaId(paParameterDTO.getPapaId(),
                paParameterDTO.getParaId()) > 0){
            parameter = paParameterMapper.selectByPapaIdAndParaId(paParameterDTO.getPapaId(),
                    paParameterDTO.getParaId());
        }
        if(null == parameter){
            parameter = new PaParameter();
            isAdd = true;
        }
        parameter.setPaName(paParameterDTO.getPaName());
        if(isAdd){
            parameter.setParaId(paParameterDTO.getParaId());
            parameter.setPapaId(paParameterDTO.getPapaId());
            if(paParameterMapper.insert(parameter) > 0){
                return ServerResponse.createBySuccessMessage("新增成功");
            }
        }else{
            if(paParameterMapper.updateByPrimaryKeySelective(parameter) > 0){
                return ServerResponse.createBySuccessMessage("更新成功");
            }
        }
        return ServerResponse.createByErrorMessage("操作失败");
    }

    /**
     * 删除枚举参数
     *
     * @param paParameterDTO
     * @param baseUserVO
     * @return
     */
    @Override
    public ServerResponse<String> delParameterByParaId(PaParameterDTO paParameterDTO, BaseUserVO baseUserVO) {
        if(null == paParameterDTO || null == baseUserVO || null == paParameterDTO.getParaId()
                || null == paParameterDTO.getPapaId()){
            return ServerResponse.createByErrorMessage("参数错误");
        }
        if(paParameterMapper.delByParaIdAndPapaId(paParameterDTO.getPapaId(),
                paParameterDTO.getParaId()) > 0){
            return ServerResponse.createBySuccessMessage("删除成功");
        }
        return ServerResponse.createByErrorMessage("删除失败");
    }

    /**
     * 查询枚举参数根据枚举类型ID
     *
     * @param paParameterDTO
     * @param baseUserVO
     * @return
     */
    @Override
    public ServerResponse<List<PaParameterVO>> selectListPaParameterByPapaId(PaParameterDTO paParameterDTO, BaseUserVO baseUserVO) {
        if(null == paParameterDTO || null == baseUserVO || null == paParameterDTO.getPapaId()){
            return ServerResponse.createByErrorMessage("参数错误");
        }
        List<PaParameterVO> list = paParameterMapper.selectListByPapaId(paParameterDTO.getPapaId());
        if(null == list || 0 == list.size()){
            return ServerResponse.createByErrorMessage("没有数据");
        }
        return ServerResponse.createBySuccess("查询成功",list);
    }

    /**
     * 查询单个枚举参数详情
     *
     * @param paParameterDTO
     * @param baseUserVO
     * @return
     */
    @Override
    public ServerResponse<PaParameterVO> selectPaParameterByParaId(PaParameterDTO paParameterDTO, BaseUserVO baseUserVO) {
        if(null == paParameterDTO || null == baseUserVO || null == paParameterDTO.getPapaId()
                || null == paParameterDTO.getParaId()){
            return ServerResponse.createByErrorMessage("参数错误");
        }
        PaParameterVO paParameterVO = paParameterMapper.selectVoByPapaIdAndParaId(paParameterDTO.getPapaId(),
                paParameterDTO.getParaId());
        if(null == paParameterDTO){
            return ServerResponse.createByErrorMessage("没有数据");
        }
        return ServerResponse.createBySuccess("查询成功",paParameterVO);
    }
}
