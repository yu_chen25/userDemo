package com.userdemo.demo.service.base;

import com.github.pagehelper.PageInfo;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.entity.base.dto.PaParameterDTO;
import com.userdemo.demo.entity.base.dto.PaParameterSortDTO;
import com.userdemo.demo.entity.base.vo.BaseUserVO;
import com.userdemo.demo.entity.base.vo.PaParameterSortVO;
import com.userdemo.demo.entity.base.vo.PaParameterVO;

import java.util.List;

/**
 * 　　* @description: 枚举服务层
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public interface IPaParameterService {

    /**
     * 新增枚举类型
     * @param paParameterSortDTO
     * @return
     */
    ServerResponse<String> addOrUpdateParameterSort(PaParameterSortDTO paParameterSortDTO, BaseUserVO baseUserVO);


    /**
     * 删除枚举类型
     * @param paParameterSortDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<String> delParameterSortByPapaId(PaParameterSortDTO paParameterSortDTO, BaseUserVO baseUserVO);


    /**
     * 查询枚举类型列表
     * @param paParameterSortDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<PageInfo> selectListPaParameterSort(PaParameterSortDTO paParameterSortDTO, BaseUserVO baseUserVO);


    /**
     * 查询单个枚举类型
     * @param paParameterSortDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<PaParameterSortVO> selectParameterSortByPapaId(PaParameterSortDTO paParameterSortDTO, BaseUserVO baseUserVO);


    /**
     * 新增/更新枚举参数
     * @param paParameterDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<String> addOrUpdateParameter(PaParameterDTO paParameterDTO, BaseUserVO baseUserVO);


    /**
     * 删除枚举参数
     * @param paParameterDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<String> delParameterByParaId(PaParameterDTO paParameterDTO, BaseUserVO baseUserVO);


    /**
     * 查询枚举参数根据枚举类型ID
     * @param paParameterDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<List<PaParameterVO>> selectListPaParameterByPapaId(PaParameterDTO paParameterDTO, BaseUserVO baseUserVO);


    /**
     * 查询单个枚举参数详情
     * @param paParameterDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<PaParameterVO> selectPaParameterByParaId(PaParameterDTO paParameterDTO, BaseUserVO baseUserVO);
}
