package com.userdemo.demo.service.base;

import com.alibaba.fastjson.JSONObject;
import com.userdemo.demo.common.sys.ServerResponse;

import java.util.Map;


/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public interface IElasticSearchService {

    /**
     * 测试创建索引
     * @return
     */
    ServerResponse<String> createIndex(String indexName);

    /**
     * 测试新增索引数据
     * @param index
     * @param object
     * @return
     */
    ServerResponse<String> addIndexData(String index, JSONObject object);

    /**
     * 测试查询所有索引数据
     * @param indexName
     * @param type
     * @return
     */
    ServerResponse<Map> selectAll(String indexName, String type);

    /**
     * ID条件查询数据
     * @param index
     * @param id
     * @return
     */
    ServerResponse<String> selectByMatch(String index, String type, String id);


    /**
     * 条件删除数据
     * @param index
     * @param type
     * @param id
     * @return
     */
    ServerResponse<String> deleteByMatch(String index, String type, String id);






}
