package com.userdemo.demo.service.base.impl;

import com.alibaba.fastjson.JSONObject;
import com.userdemo.demo.annotation.SysLog;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.service.base.IElasticSearchService;
import com.userdemo.demo.util.base.StringUtil;
import com.userdemo.demo.util.elasticsearch.ElasticsearchUtil;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
@Service("iElasticSearchService")
public class ElasticSearchServiceImpl implements IElasticSearchService {


    /**
     * 测试创建索引
     * @return
     */
    @SysLog("测试创建索引")
    @Override
    public ServerResponse<String> createIndex(String indexName)  {
        if(ElasticsearchUtil.checkIndexExist(indexName)){
            return ServerResponse.createByErrorMessage("索引已存在");
        }
        if(ElasticsearchUtil.createIndex(indexName)){
            return ServerResponse.createBySuccessMessage("创建索引成功");
        }
        return ServerResponse.createByErrorMessage("创建索引失败");
    }


    /**
     * 测试新增索引数据
     * @param index
     * @param object
     * @return
     */
    @SysLog("测试新增索引数据")
    @Override
    public ServerResponse<String> addIndexData(String index,JSONObject object) {
        String id = ElasticsearchUtil.addData(index,object);
        if(StringUtil.isNotEmpty(id)){
            return ServerResponse.createBySuccess("新增数据成功",id);
        }
        return ServerResponse.createByErrorMessage("新增数据失败");
    }

    /**
     * 测试查询所有索引数据
     * @param indexName
     * @param type
     * @return
     */
    @SysLog("测试查询所有索引数据")
    @Override
    public ServerResponse<Map> selectAll(String indexName,String type) {
        try {
            String endPoint = "/" + indexName + "/" + type + "/_search";
            Request request = new Request("GET", endPoint);
            Response response = ElasticsearchUtil.getLowLevelClient()
                    .performRequest(request);
            Map resultMap = (Map)JSONObject.parse(EntityUtils.toString(response.getEntity()));
            return ServerResponse.createBySuccess("查询成功",(Map)JSONObject.parse(resultMap.get("_source").toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorMessage("查询失败");
    }



    /**
     * ID条件查询数据
     * @param index
     * @param id
     * @return
     */
    @SysLog("ID条件查询数据")
    @Override
    public ServerResponse<String> selectByMatch(String index,String type, String id) {
        try {
            String endPoint = "/" + index + "/" + type + "/"+id;
            Request request = new Request("GET", endPoint);
            Response response = ElasticsearchUtil.getLowLevelClient().performRequest(request);
            Map resultMap = (Map)JSONObject.parse(EntityUtils.toString(response.getEntity()));
            resultMap = (Map)JSONObject.parse(resultMap.get("_source").toString());
            return ServerResponse.createBySuccess("查询成功",StringUtil.valueOfString(resultMap.get("str")).replaceAll("\\\"",""));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorMessage("查询失败");
    }

    /**
     * 条件删除数据
     * @param index
     * @param type
     * @param id
     * @return
     */
    @SysLog("条件删除数据")
    @Override
    public ServerResponse<String> deleteByMatch(String index, String type, String id) {
        try {
            String endPoint = "/" + index + "/" + type + "/"+id;
            Request request = new Request("DELETE", endPoint);
            Response response = ElasticsearchUtil.getLowLevelClient().performRequest(request);
            return ServerResponse.createBySuccess("删除成功", EntityUtils.toString(response.getEntity()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ServerResponse.createByErrorMessage("删除失败");
    }





}
