package com.userdemo.demo.service.base;

import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.entity.base.dto.BaseMenuDTO;
import com.userdemo.demo.entity.base.vo.BaseMenuVO;
import com.userdemo.demo.entity.base.vo.BaseUserVO;

import java.util.List;

/**
 * 　　* @description: 菜单服务层
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public interface IBaseMenuService {

    /**
     * 新增/更新菜单
     * @param baseMenuDTO
     * @param userVO
     * @return
     */
    ServerResponse<String> addOrUpdateMenu(BaseMenuDTO baseMenuDTO, BaseUserVO userVO);


    /**
     * 删除菜单
     * @param baseMenuDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<String> delById(BaseMenuDTO baseMenuDTO, BaseUserVO baseUserVO);


    /**
     * 列表查询菜单
     * @param baseMenuDTO
     * @return
     */
    ServerResponse<List<BaseMenuVO>> selectListMenu(BaseMenuDTO baseMenuDTO, BaseUserVO baseUserVO);


    /**
     * 查询树状用户菜单
     * @return
     */
    ServerResponse<List<BaseMenuVO>> selectTreeMenuByUserName(BaseUserVO baseUserVO);


    /**
     * 查询树形所有菜单
     * @return
     */
    ServerResponse<List<BaseMenuVO>> selectTreeMenu(BaseUserVO baseUserVO);


    /**
     * 查询单个菜单详情
     * @param baseMenuDTO
     * @return
     */
    ServerResponse<BaseMenuVO> selectMenuById(BaseMenuDTO baseMenuDTO, BaseUserVO baseUserVO);

}
