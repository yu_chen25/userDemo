package com.userdemo.demo.service.base.impl;

import com.userdemo.demo.annotation.SysLog;
import com.userdemo.demo.base.BaseService;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.dao.base.BaseOrgMechanismHistoricalMapper;
import com.userdemo.demo.entity.base.BaseOrgMechanism;
import com.userdemo.demo.entity.base.BaseOrgMechanismHistorical;
import com.userdemo.demo.entity.base.dto.BaseOrgMechanismDTO;
import com.userdemo.demo.entity.base.vo.BaseOrgMechanismHistoricalVO;
import com.userdemo.demo.entity.base.vo.BaseOrgMechanismVO;
import com.userdemo.demo.entity.base.vo.BaseUserVO;
import com.userdemo.demo.enumeration.base.UseTypeEnum;
import com.userdemo.demo.service.base.IBaseOrgMechanismService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 　　* @description: 组织机构服务层
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
@Service("iBaseOrgMechanismService")
public class BaseOrgMechanismServiceImpl extends BaseService implements IBaseOrgMechanismService {


    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * 新增/更新组织机构
     *
     * @param mechanismDTO
     * @param baseUserVO
     * @return
     */
    @SysLog("新增/更新组织机构")
    @Override
    public ServerResponse<String> addOrUpdateOrgMechanism(BaseOrgMechanismDTO mechanismDTO, BaseUserVO baseUserVO) {
        if(null == mechanismDTO || null == baseUserVO || null == mechanismDTO.getParentId()){
            return ServerResponse.createByErrorMessage("参数错误");
        }
        BaseOrgMechanism baseOrgMechanism = null;
        Date now  = new Date();
        boolean isAdd = false;
        if(null != mechanismDTO.getOrgId() && 0 < mechanismDTO.getOrgId()){
            baseOrgMechanism = baseOrgMechanismMapper.selectByPrimaryKey(mechanismDTO.getOrgId());
            baseOrgMechanism.setUpdateUser(baseUserVO.getUserName());

            if(!baseOrgMechanism.getOrgName().equals(mechanismDTO.getOrgName()))
            {
                //保存组织机构历史名字  申请科室 使用科室用
                BaseOrgMechanismHistorical record  = new BaseOrgMechanismHistorical();
                Long  orgHistoricalId   =  baseOrgMechanismHistoricalMapper.selectSeq();
                record.setId(orgHistoricalId);
                record.setOrgId(mechanismDTO.getOrgId());
                record.setParentId(baseOrgMechanism.getParentId());
                record.setOrgName(baseOrgMechanism.getOrgName());
                record.setIsTop(baseOrgMechanism.getIsTop());
                record.setOrderNum(baseOrgMechanism.getOrderNum());
                record.setStatus(1);
                record.setVersion(1);
                record.setUseType(baseOrgMechanism.getUseType());
                record.setCreateOrgId(baseOrgMechanism.getCreateOrgId());
                record.setCreateTopOrgId(baseOrgMechanism.getCreateTopOrgId());
                record.setCreateUser(baseOrgMechanism.getCreateUser());
                record.setCreateTime(baseOrgMechanism.getCreateTime());
                record.setUpdateTime(baseOrgMechanism.getUpdateTime());
                record.setUpdateUser(baseOrgMechanism.getUpdateUser());
                record.setCompanyId(baseOrgMechanism.getCompanyId());

                baseOrgMechanismHistoricalMapper.insertSelective(record);
            }
        }
        if(null == baseOrgMechanism){

            Long id =  baseOrgMechanismMapper.selectSeq();
            baseOrgMechanism = new BaseOrgMechanism();
            baseOrgMechanism.setId(id);
            baseOrgMechanism.setCreateUser(baseUserVO.getUserName());
            baseOrgMechanism.setUpdateUser(baseUserVO.getUserName());
            baseOrgMechanism.setCreateTime(now);
            baseOrgMechanism.setUpdateTime(now);
            baseOrgMechanism.setCreateOrgId(baseUserVO.getCreateOrgId());
            baseOrgMechanism.setCreateTopOrgId(baseUserVO.getCreateTopOrgId());
            baseOrgMechanism.setStatus(1);
            baseOrgMechanism.setVersion(1);
            baseOrgMechanism.setParentId(mechanismDTO.getParentId());


            if(mechanismDTO.getParentId() == 0)
            {
                baseOrgMechanism.setIsTop(1);
            }else
            {
                baseOrgMechanism.setIsTop(2);
            }

            isAdd = true;
        }
        baseOrgMechanism.setCompanyId(baseUserVO.getCompanyId());
        baseOrgMechanism.setOrgName(mechanismDTO.getOrgName());
        baseOrgMechanism.setUseType(baseUserVO.getUseType());

        if(isAdd){
            if(baseOrgMechanismMapper.insertSelective(baseOrgMechanism) > 0){
                return ServerResponse.createBySuccessMessage("新增成功");
            }
        }else{
            if(baseOrgMechanismMapper.updateByPrimaryKeySelective(baseOrgMechanism) > 0){
                return ServerResponse.createBySuccessMessage("更新成功");
            }
        }
        return ServerResponse.createByErrorMessage("操作失败");
    }

    /**
     * Id删除组织机构
     *
     * @param mechanismDTO
     * @param baseUserVO
     * @return
     */
    @SysLog("Id删除组织机构")
    @Override
    public ServerResponse<String> delOrgMechanismById(BaseOrgMechanismDTO mechanismDTO, BaseUserVO baseUserVO) {
        if(null == mechanismDTO || null == baseUserVO || null == mechanismDTO.getOrgId()){
            return ServerResponse.createByErrorMessage("参数错误");
        }
        if(baseOrgMechanismMapper.updateStatusByOrgid(mechanismDTO.getOrgId(),baseUserVO.getUserName()) > 0){
            return ServerResponse.createBySuccessMessage("删除成功");
        }
        return ServerResponse.createByErrorMessage("删除失败");
    }

    /**
     * 查询树状组织机构
     *
     * @param mechanismDTO
     * @param baseUserVO
     * @return
     */
    @SysLog("查询树状组织机构")
    @Override
    public ServerResponse<List<BaseOrgMechanismVO>> selectTreeBaseOrgMechanism(BaseOrgMechanismDTO mechanismDTO, BaseUserVO baseUserVO) {
        if(null == mechanismDTO || null == baseUserVO){
            return ServerResponse.createByErrorMessage("参数错误");
        }

         Long companyId = null;
        //非平台用户不允许做操作
        if(UseTypeEnum.PLATFORM.getCode()  !=  baseUserVO.getUseType())
        {
            companyId = baseUserVO.getCompanyId();
        }else
        {
            companyId = mechanismDTO.getCompanyId();
        }

        mechanismDTO.setCompanyId(companyId);


        List<BaseOrgMechanismVO> list = getTreeOrg(mechanismDTO);
        if(null == list || 0 == list.size()){
            return ServerResponse.createByErrorMessage("没有数据");
        }
        return ServerResponse.createBySuccess("查询成功",list);
    }

    /**
     * 查询该组织机构历史名称
     *
     * @param mechanismDTO
     * @param baseUserVO
     * @return
     */
    @Override
    public ServerResponse<List<BaseOrgMechanismHistoricalVO>> selectBaseOrgMechanismHistorical(BaseOrgMechanismDTO mechanismDTO, BaseUserVO baseUserVO) {
        if(null == mechanismDTO   ||  null == baseUserVO)
        {
            return ServerResponse.createByErrorMessage("参数错误");
        }

        List<BaseOrgMechanismHistoricalVO>  list  =   baseOrgMechanismHistoricalMapper.selectBaseOrgMechanismHistorical(mechanismDTO.getOrgId());

        if(list != null  &&  list.size() != 0)
        {
            return  ServerResponse.createBySuccess("查询成功", list);
        }else
        {
            return ServerResponse.createByErrorMessage("没有数据");
        }
    }


    /**
     * 查询树状组织机构
     * @param mechanismDTO
     * @return
     */
    List<BaseOrgMechanismVO> getTreeOrg(BaseOrgMechanismDTO mechanismDTO){

        List<BaseOrgMechanismVO> list = baseOrgMechanismMapper.selectTreeListOrg(mechanismDTO);
        if(null != list && 0 < list.size()){
            for (int i = 0; i < list.size(); i++) {
                BaseOrgMechanismDTO mechanismDTO1 = new BaseOrgMechanismDTO();
                mechanismDTO1.setCompanyId(mechanismDTO.getCompanyId());
                mechanismDTO1.setParentId(list.get(i).getOrgId());

                //需要查角色权限值时候就传
                if(mechanismDTO.getRoleId() != null  &&  mechanismDTO.getRoleId() != 0)
                {
                    mechanismDTO1.setRoleId(mechanismDTO.getRoleId());

                    //查询该角色有没有该组织机构权限
                    int isPass =  baseRoleMechanismMapper.countIsPass(mechanismDTO1);

                    //有权限就是Y  无权限就是N
                    if(isPass > 0)
                    {
                        list.get(i).setIsPass("Y");
                    }else
                    {
                        list.get(i).setIsPass("N");
                    }

                }


                list.get(i).setChildren(getTreeOrg(mechanismDTO1));
            }
        }
        return list;
    }


}
