package com.userdemo.demo.service.base;

import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.entity.base.dto.BaseOrgMechanismDTO;
import com.userdemo.demo.entity.base.vo.BaseOrgMechanismHistoricalVO;
import com.userdemo.demo.entity.base.vo.BaseOrgMechanismVO;
import com.userdemo.demo.entity.base.vo.BaseUserVO;

import java.util.List;

/**
 * 　　* @description: 组织机构服务层
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public interface IBaseOrgMechanismService {

    /**
     * 新增/更新组织机构
     * @param mechanismDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<String> addOrUpdateOrgMechanism(BaseOrgMechanismDTO mechanismDTO, BaseUserVO baseUserVO);


    /**
     * Id删除组织机构
     * @param mechanismDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<String> delOrgMechanismById(BaseOrgMechanismDTO mechanismDTO, BaseUserVO baseUserVO);


    /**
     * 查询树状组织机构
     * @param mechanismDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<List<BaseOrgMechanismVO>> selectTreeBaseOrgMechanism(BaseOrgMechanismDTO mechanismDTO, BaseUserVO baseUserVO);


    /**
     * 查询该组织机构历史名称
     * @param mechanismDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<List<BaseOrgMechanismHistoricalVO>> selectBaseOrgMechanismHistorical(BaseOrgMechanismDTO mechanismDTO, BaseUserVO baseUserVO);
}
