package com.userdemo.demo.service.base.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.userdemo.demo.base.BaseService;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.constant.base.Const;
import com.userdemo.demo.entity.base.dto.BaseLogDTO;
import com.userdemo.demo.entity.base.vo.BaseLogVO;
import com.userdemo.demo.entity.base.vo.BaseUserVO;
import com.userdemo.demo.service.base.IBaseLogService;
import com.userdemo.demo.util.base.NumberUtil;
import com.userdemo.demo.util.base.StringUtil;
import com.userdemo.demo.util.elasticsearch.ElasticsearchUtil;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
@Service("iBaseLogService")
public class BaseLogServiceImpl extends BaseService implements IBaseLogService {



    /**
     * 查询系统日志记录
     *
     * @param baseLogDTO
     * @param baseUserVO
     * @return
     */
    @Override
    public ServerResponse<PageInfo> selectBaseLogList(BaseLogDTO baseLogDTO, BaseUserVO baseUserVO) {
        if(null == baseLogDTO || null == baseUserVO){
            return ServerResponse.createByErrorMessage("参数错误");
        }
        if(null == baseLogDTO.getPageNum()){
            baseLogDTO.setPageNum(Const.INITPAGENUM);
        }
        if(null == baseLogDTO.getPageSize()){
            baseLogDTO.setPageSize(Const.INITPAGESIZE);
        }
        PageHelper.startPage(baseLogDTO.getPageNum(),baseLogDTO.getPageSize(),true,false,null);
        PageHelper.orderBy(" id DESC");
        List<BaseLogVO> baseLogVOList = baseLogMapper.selectBaseLogList(baseLogDTO);
        if(null == baseLogVOList || 0 == baseLogVOList.size()){
            return ServerResponse.createByErrorMessage("没有数据");
        }
        return ServerResponse.createBySuccess("查询成功",new PageInfo(baseLogVOList));
    }

    /**
     * 批量删除日志记录
     *
     * @param baseLogDTO
     * @param baseUserVO
     * @return
     */
    @Override
    public ServerResponse<String> delBaseLogByIdList(BaseLogDTO baseLogDTO, BaseUserVO baseUserVO) {
        if(null == baseLogDTO || null == baseUserVO || StringUtil.isEmpty(baseLogDTO.getBaseLogIds())){
            return ServerResponse.createByErrorMessage("参数错误");
        }
        //截取ID
        String[] baseLogIdArr = baseLogDTO.getBaseLogIds().split(",");
        if(null != baseLogIdArr && 0 < baseLogIdArr.length){
            List<Long> baseIdList = Lists.newArrayList();
            for (String id : baseLogIdArr){
                baseIdList.add(NumberUtil.safeToLong(id));
            }
            //批量查询提交参数ID和返回内容ID
            List<Long> longList = Lists.newArrayList();
            List<BaseLogVO> baseLogVOList = baseLogMapper.selectParamIdList(baseIdList);
            for (BaseLogVO baseLogVO : baseLogVOList){
                //循环添加大字段ID
                longList.add(baseLogVO.getParamsId());
                longList.add(baseLogVO.getResponseId());
            }
            //删除es数据
            ElasticsearchUtil.delData(longList);
            //删除大字段表数据
            baseLogBigfieldMapper.delBaseLogBigfieldByList(longList);
            //批量删除日志记录
            baseLogMapper.delByList(baseIdList);
            return ServerResponse.createBySuccessMessage("删除成功");
        }
        return ServerResponse.createByErrorMessage("删除失败");
    }
}
