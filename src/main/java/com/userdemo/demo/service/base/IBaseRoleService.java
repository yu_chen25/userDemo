package com.userdemo.demo.service.base;

import com.github.pagehelper.PageInfo;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.entity.base.dto.BaseRoleDTO;
import com.userdemo.demo.entity.base.vo.BaseRoleVO;
import com.userdemo.demo.entity.base.vo.BaseUserVO;

import java.util.List;

/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public interface IBaseRoleService {

    /**
     * 新增/更新角色信息
     * @param baseRoleDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<String> addOrUpdateRole(BaseRoleDTO baseRoleDTO, BaseUserVO baseUserVO);


    /**
     * ID删除用户角色
     * @return
     */
    ServerResponse<String> delRoleById(BaseRoleDTO baseRoleDTO);


    /**
     * 列表查询角色
     * @param baseRoleDTO
     * @return
     */
    ServerResponse<PageInfo> selectListRole(BaseRoleDTO baseRoleDTO, BaseUserVO baseUserVO);

    /**
     * 查询角色列表
     * @param baseRoleDTO
     * @return
     */
    ServerResponse<List<BaseRoleVO>> selectAllRole(BaseRoleDTO baseRoleDTO, BaseUserVO baseUserVO);


    /**
     * ID查询角色信息
     * @param baseRoleDTO
     * @return
     */
    ServerResponse<BaseRoleVO> selectRoleById(BaseRoleDTO baseRoleDTO);

    /**
     * 启用禁用接口
     * @param baseRoleDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<String> updateIsPublish(BaseRoleDTO baseRoleDTO, BaseUserVO baseUserVO);

    /**
     * 设置数据权限接口
     * @param baseRoleDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<String> addRoleOrg(BaseRoleDTO baseRoleDTO, BaseUserVO baseUserVO);

    /**
     * 用户分配角色
     * @param baseRoleDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<String> addRoleUser(BaseRoleDTO baseRoleDTO, BaseUserVO baseUserVO);

    /**
     * 角色关联用户
     * @param baseRoleDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<String> addUserRole(BaseRoleDTO baseRoleDTO, BaseUserVO baseUserVO);
}
