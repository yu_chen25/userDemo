package com.userdemo.demo.service.base;

import com.userdemo.demo.common.sys.ServerResponse;

import javax.servlet.http.HttpServletRequest;

/**
 * 　　* @description: 手机短信服务层
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public interface IPhoneMessageService {

    /**
     * 短信发送校验
     * @param phone
     * @param request
     * @return
     */
    ServerResponse<String> sendMessageCheck(String phone, HttpServletRequest request);
}
