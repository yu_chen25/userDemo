package com.userdemo.demo.service.base;

import com.github.pagehelper.PageInfo;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.entity.base.dto.BaseLogDTO;
import com.userdemo.demo.entity.base.vo.BaseUserVO;

/**
 * 　　* @description: 系统日志服务层
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public interface IBaseLogService {

    /**
     * 查询系统日志记录
     * @param baseLogDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<PageInfo> selectBaseLogList(BaseLogDTO baseLogDTO, BaseUserVO baseUserVO);


    /**
     * 批量删除日志记录
     * @param baseLogDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<String> delBaseLogByIdList(BaseLogDTO baseLogDTO, BaseUserVO baseUserVO);

}
