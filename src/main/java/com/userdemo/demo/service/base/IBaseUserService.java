package com.userdemo.demo.service.base;

import com.github.pagehelper.PageInfo;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.entity.base.dto.BaseUserDTO;
import com.userdemo.demo.entity.base.vo.BaseUserVO;

import javax.servlet.http.HttpServletRequest;

/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public interface IBaseUserService {

    /**
     * 用户登录
     * @param userName
     * @param password
     * @return
     */
    ServerResponse<BaseUserVO> login(String userName, String password);

    /**
     * 新增/更新用户
     * @param baseUserDTO
     * @return
     */
    ServerResponse<String> addOrUpdateUser(BaseUserDTO baseUserDTO, BaseUserVO baseUserVO);



    /**
     * 获取请求用户
     * @param request
     * @return
     */
    BaseUserVO getRequestUser(HttpServletRequest request);


    /**
     * 根据ID删除用户
     * @param baseUserDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<String> delUserById(BaseUserDTO baseUserDTO, BaseUserVO baseUserVO);


    /**
     * 查询用户列表
     * @param baseUserDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<PageInfo> selectListUser(BaseUserDTO baseUserDTO, BaseUserVO baseUserVO);


    /**
     * 查询单个用户详情
     * @param baseUserDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<BaseUserVO> selectUpdateUserInfoById(BaseUserDTO baseUserDTO, BaseUserVO baseUserVO);

    /**
     * 查询项目负责人接口
     * @param baseUserDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<PageInfo> selectProjectLeader(BaseUserDTO baseUserDTO, BaseUserVO baseUserVO);

    /**
     * 用户名注册
     * @param baseUserDTO
     * @return
     */
    ServerResponse<String> userRegistration(BaseUserDTO baseUserDTO);


    /**
     * 查询供应商列表
     * @param baseUserDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<PageInfo> selectSuppliersList(BaseUserDTO baseUserDTO, BaseUserVO baseUserVO);


    /**
     * 注册代理机构用户和医务工作者
     * @param baseUserDTO
     * @return
     */
    ServerResponse<String> userCompanyRegistration(BaseUserDTO baseUserDTO);
}
