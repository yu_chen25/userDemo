package com.userdemo.demo.service.cp;

import com.github.pagehelper.PageInfo;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.entity.base.vo.BaseUserVO;
import com.userdemo.demo.entity.cp.dto.CpCrawlerEquipmentDTO;
import com.userdemo.demo.entity.cp.vo.*;

import java.text.ParseException;
import java.util.List;

public interface ICpAnnouncementService {

    /**
     * 同步oa中标设备库数据
     * @return
     */
    ServerResponse<String> addOaAnnouncementEquipment();

    /**
     * 同步爬虫设备数据
     * @return
     */
    ServerResponse<String> addCrawlerEquipment() throws ParseException;


    /**
     * 爬虫数据列表
     * @param cpCrawlerEquipmentDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<PageInfo> selectCrawlerEquipmentList(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO, BaseUserVO baseUserVO);

    /**
     * 查看单个爬虫数据信息
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    ServerResponse<CpCrawlerEquipmentVO> selectCrawlerEquipmentById(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO);

    /**
     * 查询爬虫数据招标配置信息
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    ServerResponse<List<CpCrawlerConfigurationVO>> selectCrawlerConfigurationById(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO);

    /**
     * 编辑爬虫设备数据
     * @param cpCrawlerEquipmentDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<String> updateCrawlerEquipment(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO, BaseUserVO baseUserVO);

    /**
     * 新增爬虫设备数据
     * @param cpCrawlerEquipmentDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<String> xinAddCrawlerEquipment(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO, BaseUserVO baseUserVO);

    /**
     * 批量移到回收站，或者批量恢复
     * @param cpCrawlerEquipmentDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<String> updateStatusCrawlerEquipment(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO, BaseUserVO baseUserVO);

    /**
     * 批量彻底删除
     * @param cpCrawlerEquipmentDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<String> deleteCrawlerEquipment(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO, BaseUserVO baseUserVO);


    /**
     * 前端查询接口
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    ServerResponse<PageInfo> getCrawlerEquipmentBeforelist(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO);

    /**
     * 获取OA设备招标配置
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    ServerResponse<BaseAttachmentVO> getOaConfiguration(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO);

    /**
     * 查看爬虫设备招标配置
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    ServerResponse<List<CpCrawlerConfigurationVO>> getCwConfiguration(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO);


    /**
     * 统计数据查询
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    ServerResponse<CrawlerEquipmentSumVO> getCrawlerEquipmentSum(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO);

    /**
     * 获取省市信息
     * @return
     */
    ServerResponse<List<CpCityNameVO>> getCpCityNameVO(Integer isRelease);

    /**
     * 处理数据
     * @param cpCrawlerEquipmentDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<String> setRelease(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO, BaseUserVO baseUserVO);

    /**
     * 同步型号缩写
     * @return
     */
    ServerResponse<String> mpdelAdd();

    /**
     * 获取制造商名称
     * @param cpCrawlerEquipmentDTO
     * @param baseUserVO
     * @return
     */
    ServerResponse<PageInfo> getManufacturerName(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO, BaseUserVO baseUserVO);

    /**
     * 同步更新设备数据
     * @return
     */
    ServerResponse<String> updateSynchronous() throws ParseException;
}
