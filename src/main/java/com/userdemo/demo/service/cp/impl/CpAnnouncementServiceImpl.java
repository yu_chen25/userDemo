package com.userdemo.demo.service.cp.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.constant.base.Const;
import com.userdemo.demo.dao.cp.CpAnnouncementEquipmentMapper;
import com.userdemo.demo.dao.cp.CpCrawlerConfigurationMapper;
import com.userdemo.demo.dao.cp.CpCrawlerEquipmentMapper;
import com.userdemo.demo.entity.base.vo.BaseUserVO;
import com.userdemo.demo.entity.cp.CpAnnouncementEquipment;
import com.userdemo.demo.entity.cp.CpCrawlerConfiguration;
import com.userdemo.demo.entity.cp.CpCrawlerEquipment;
import com.userdemo.demo.entity.cp.dto.CpCrawlerEquipmentDTO;
import com.userdemo.demo.entity.cp.vo.*;
import com.userdemo.demo.service.cp.ICpAnnouncementService;
import com.userdemo.demo.util.base.DateTimeUtil;
import com.userdemo.demo.util.base.NumberUtil;
import com.userdemo.demo.util.base.StringUtil;
import com.xiaoleilu.hutool.http.HttpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service("iCpAnnouncementService")
public class CpAnnouncementServiceImpl implements ICpAnnouncementService {

    Logger logger = LoggerFactory.getLogger(CpAnnouncementServiceImpl.class);

    @Resource
    private CpAnnouncementEquipmentMapper cpAnnouncementEquipmentMapper;

    @Resource
    private CpCrawlerEquipmentMapper  cpCrawlerEquipmentMapper;

    @Resource
    private CpCrawlerConfigurationMapper  cpCrawlerConfigurationMapper;

    /**
     *oa中标设备库
     */
    @Value("${oa.url1}")
    private String url1;

    @Value("${file.downUrl}")
    private String fileDownUrl;

    @Value("${file.url6}")
    private String url6;

    /**
     * 同步oa中标设备库数据
     *
     * @return
     */
    @Override
    public ServerResponse<String> addOaAnnouncementEquipment() {

      this.synchronousOaAnnouncementEquipment();
        return ServerResponse.createBySuccessMessage("同步成功");
    }

    /**
     * 同步爬虫设备数据
     *
     * @return
     */
    @Override
    public ServerResponse<String> addCrawlerEquipment() throws ParseException {

        this.synchronousCrawlerEquipment();
        return ServerResponse.createBySuccessMessage("同步成功");
    }


    //同步爬虫数据
    private void synchronousCrawlerEquipment() throws ParseException {


        //同步到的最大的Id
       Long maxCrawlerEquipment = cpCrawlerEquipmentMapper.getMaxSynchronousId();

       //查询最新同步爬虫设备
       List<Map>  pelist  =  cpCrawlerEquipmentMapper.getProvinceEquipment(maxCrawlerEquipment);

       if(pelist != null  &&  pelist.size() != 0)
       {

           for(Map  map : pelist)
           {


               //同步至设备表
               CpCrawlerEquipment cpCrawlerEquipment = new CpCrawlerEquipment();

               cpCrawlerEquipment.setId(cpCrawlerEquipmentMapper.selectSeq());
               cpCrawlerEquipment.setAnnouncementName(StringUtil.valueOfString(map.get("announcement_name")));
               cpCrawlerEquipment.setCityName(StringUtil.valueOfString(map.get("area")));
               cpCrawlerEquipment.setPurchaseName(StringUtil.valueOfString(map.get("select_person")));

               if(StringUtil.isNotEmpty(StringUtil.valueOfString(map.get("purchase_time"))))
               {
                   String purchaseTime = StringUtil.valueOfString(map.get("purchase_time"));

                   purchaseTime =  purchaseTime.replaceAll("/","-");


                   //时间格式转化
//                   SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
//                   formatter.setLenient(false);
//                   Date newDate= formatter.parse(StringUtil.valueOfString(map.get("purchase_time")));
                    SimpleDateFormat  formatter = new SimpleDateFormat("yyyy-MM-dd");
                    Date newDate= formatter.parse(purchaseTime);

                   cpCrawlerEquipment.setIssueTime(DateTimeUtil.strToDate2(formatter.format(newDate)));
               }

               cpCrawlerEquipment.setEquipmentName(StringUtil.valueOfString(map.get("equipment_name")));

               String  amount =  StringUtil.valueOfString(map.get("amout"));

               if(StringUtil.isNotEmpty(amount))
               {
                   try {
                       cpCrawlerEquipment.setAmount(new BigDecimal(amount.trim()));
                   }catch(Exception e) {
                       e.printStackTrace();
                   }

               }

               String  price =  StringUtil.valueOfString(map.get("price"));

               if(StringUtil.isNotEmpty(price))
               {
                   try {
                       cpCrawlerEquipment.setPrice(new BigDecimal(price.trim()));
                   }catch(Exception e) {
                       e.printStackTrace();
                   }

               }

               String  quantity =  StringUtil.valueOfString(map.get("quantity"));

               if(StringUtil.isNotEmpty(quantity))
               {
                   quantity = quantity.trim();
                   cpCrawlerEquipment.setQuantity(NumberUtil.safeToNumber(quantity));
               }

               cpCrawlerEquipment.setCompanyName(StringUtil.valueOfString(map.get("company_name")));
               cpCrawlerEquipment.setManufacturerName(StringUtil.valueOfString(map.get("manufacture_name")));
               cpCrawlerEquipment.setModel(StringUtil.valueOfString(map.get("model")));
               cpCrawlerEquipment.setAnnouncementLink(StringUtil.valueOfString(map.get("link")));
               cpCrawlerEquipment.setSynchronousId(NumberUtil.safeToLong(StringUtil.valueOfString(map.get("num_id"))));
               cpCrawlerEquipment.setStatus(1);
               cpCrawlerEquipment.setVersion(1);
               cpCrawlerEquipment.setCreateUser("tongbu");
               cpCrawlerEquipment.setCreateTime(new Date());
               cpCrawlerEquipment.setUpdateUser("tongbu");
               cpCrawlerEquipment.setUpdateTime(new Date());

               //同步设备招标参数
               List<Map>  cMap  =    cpCrawlerConfigurationMapper.getCrawlerConfiguration(StringUtil.valueOfString(map.get("num_id")));

               if(cMap != null  &&  cMap.size() != 0)
               {
                   for(Map reMap :  cMap)
                   {

                       CpCrawlerConfiguration cpCrawlerConfiguration  =  new CpCrawlerConfiguration();

                       cpCrawlerConfiguration.setId(cpCrawlerConfigurationMapper.selectSeq());
                       cpCrawlerConfiguration.setCrawlerId(cpCrawlerEquipment.getId());
                       cpCrawlerConfiguration.setCrawlerLink(StringUtil.valueOfString(reMap.get("parameter")));
                       cpCrawlerConfiguration.setSynchronousId(NumberUtil.safeToLong(StringUtil.valueOfString(reMap.get("fu_id"))));

                       cpCrawlerConfigurationMapper.insertSelective(cpCrawlerConfiguration);
                   }
                   cpCrawlerEquipment.setFileId(new Long(1));
               }

               cpCrawlerEquipmentMapper.insertSelective(cpCrawlerEquipment);

           }


       }


    }


    //同步oa中标设备方法
    private void synchronousOaAnnouncementEquipment() {

        String result = HttpUtil.get(url1);

        Date now = new Date();
        if (StringUtil.isNotEmpty(result)) {

            List<Map> resultMap = JSON.parseObject(result, List.class);

            for(Map  map : resultMap)
            {

                int count  =  cpAnnouncementEquipmentMapper.checkEquipment(StringUtil.valueOfString(map.get("equipmentId")));

                CpAnnouncementEquipment record = new CpAnnouncementEquipment();

                record.setId(NumberUtil.safeToLong(StringUtil.valueOfString(map.get("equipmentId"))));
                record.setAnnouncementId(NumberUtil.safeToLong(StringUtil.valueOfString(map.get("announcementId"))));
                record.setType(NumberUtil.safeToNumber(StringUtil.valueOfString(map.get("type"))));
                record.setAnnouncementName(StringUtil.valueOfString(map.get("announcementName")));
                record.setCityName(StringUtil.valueOfString(map.get("cityName")));
                record.setPurchaseName(StringUtil.valueOfString(map.get("purchaseName")));
                record.setPurchaseAgentName(StringUtil.valueOfString(map.get("purchaseAgentName")));
                record.setIssueTime(DateTimeUtil.strToDate(StringUtil.valueOfString(map.get("issueTime"))));
                record.setEquipmentName(StringUtil.valueOfString(map.get("equipmentName")));
                record.setAmount(new BigDecimal(StringUtil.valueOfString(map.get("amount"))));
                record.setPrice(new BigDecimal(StringUtil.valueOfString(map.get("price"))));
                record.setQuantity(NumberUtil.safeToNumber(StringUtil.valueOfString(map.get("quantity"))));
                record.setCompanyName(StringUtil.valueOfString(map.get("companyName")));
                record.setManufacturer(StringUtil.valueOfString(map.get("manufacturer")));
                record.setManufacturerName(StringUtil.valueOfString(map.get("manufacturerName")));
                record.setModel(StringUtil.valueOfString(map.get("model")));

                if(StringUtil.isNotEmpty(StringUtil.valueOfString(map.get("model"))))
                {
                    String  modelAdd   = StringUtil.valueOfString(map.get("model"));
                    modelAdd =  modelAdd.replaceAll("[\\pP\\p{Punct}]", "");
                    modelAdd =  modelAdd.replace(" ", "");
                    record.setModelAbb(modelAdd);
                }

                record.setFileId(NumberUtil.safeToLong(StringUtil.valueOfString(map.get("fileId"))));
                if(StringUtil.isNotEmpty(map.get("announcementLink")))
                {
                    record.setAnnouncementLink(StringUtil.valueOfString(map.get("announcementLink")));
                }
                record.setStatus(1);
                record.setVersion(1);
                record.setUpdateTime(now);
                record.setUpdateUser("tongbu");


                //存在就更新
                if(count >  0 )
                {
                    cpAnnouncementEquipmentMapper.updateByPrimaryKeySelective(record);

                }else//不存在就新增
                {

                    if(StringUtil.isNotEmpty(record.getCityName())  &&  "遵义市".equals(record.getCityName()) )
                    {
                        record.setProvinceName("贵州省");
                    }else
                    {
                        record.setProvinceName("湖南省");
                    }

                    record.setCreateTime(now);
                    record.setCreateUser("tongbu");
                    cpAnnouncementEquipmentMapper.insertSelective(record);
                }

            }

        }
    }

    /**
     * 爬虫数据列表
     *
     * @param cpCrawlerEquipmentDTO
     * @param baseUserVO
     * @return
     */
    @Override
    public ServerResponse<PageInfo> selectCrawlerEquipmentList(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO, BaseUserVO baseUserVO) {

        if(null == cpCrawlerEquipmentDTO ||  null == baseUserVO){
            return ServerResponse.createByErrorMessage("参数错误");
        }
        if(null == cpCrawlerEquipmentDTO.getPageNum()){
            cpCrawlerEquipmentDTO.setPageNum(Const.INITPAGENUM);
        }
        if(null == cpCrawlerEquipmentDTO.getPageSize()){
            cpCrawlerEquipmentDTO.setPageSize(Const.INITPAGESIZE);
        }
        PageHelper.startPage(cpCrawlerEquipmentDTO.getPageNum(),cpCrawlerEquipmentDTO.getPageSize(),true,false,null);

        //兼容需求    上一个版本没这个字段
        if(StringUtil.isNotEmpty(cpCrawlerEquipmentDTO.getIsRelease()))
        {
            //未处理的排序
            if( 2 == cpCrawlerEquipmentDTO.getIsRelease() )
            {
                if(StringUtil.isNotEmpty(cpCrawlerEquipmentDTO.getIsHistory()))
                {
                    if(cpCrawlerEquipmentDTO.getIsHistory() == 2)
                    {
                        PageHelper.orderBy("cce.issue_time desc,cce.id desc");
                    }else
                    {
                        PageHelper.orderBy("CONVERT( cce.city_name USING gbk ) COLLATE gbk_chinese_ci ASC,cce.issue_time desc");
                    }

                }else
                {
                    PageHelper.orderBy("CONVERT( cce.city_name USING gbk ) COLLATE gbk_chinese_ci ASC,cce.issue_time desc");
                }


            }else//处理的排序
            {
                PageHelper.orderBy("cce.release_time desc");
            }
        }else
        {
            PageHelper.orderBy("CONVERT( cce.city_name USING gbk ) COLLATE gbk_chinese_ci ASC,cce.issue_time desc");
        }


        List<CpCrawlerEquipmentVO> list = cpCrawlerEquipmentMapper.selectCrawlerEquipmentList(cpCrawlerEquipmentDTO);


        if(null == list || 0 == list.size()){
            return ServerResponse.createByErrorMessage("没有数据");
        }

        return ServerResponse.createBySuccess("查询成功",new PageInfo(list));
    }

    /**
     * 查看单个爬虫数据信息
     *
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    @Override
    public ServerResponse<CpCrawlerEquipmentVO> selectCrawlerEquipmentById(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO) {

        if(null == cpCrawlerEquipmentDTO){
            return ServerResponse.createByErrorMessage("参数错误");
        }


        CpCrawlerEquipmentVO  cpCrawlerEquipmentVO  =  cpCrawlerEquipmentMapper.selectCrawlerEquipmentById(cpCrawlerEquipmentDTO.getCpCrawlerEquipmentId());

        if(null == cpCrawlerEquipmentVO)
        {
            return ServerResponse.createByErrorMessage("没有数据");
        }

        //招标参数列表
        List<CpCrawlerConfigurationVO> clist =  cpCrawlerConfigurationMapper.selectCrawlerConfigurationList(cpCrawlerEquipmentVO.getCpCrawlerEquipmentId());

        if(clist != null &&  clist.size() != 0)
        {
            cpCrawlerEquipmentVO.setClist(clist);
        }


        return ServerResponse.createBySuccess("查询成功",cpCrawlerEquipmentVO);
    }

    /**
     * 查询爬虫数据招标配置信息
     *
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    @Override
    public ServerResponse<List<CpCrawlerConfigurationVO>> selectCrawlerConfigurationById(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO) {

        if(null == cpCrawlerEquipmentDTO ){
            return ServerResponse.createByErrorMessage("参数错误");
        }

        //招标参数列表
        List<CpCrawlerConfigurationVO> list =  cpCrawlerConfigurationMapper.selectCrawlerConfigurationList(cpCrawlerEquipmentDTO.getCpCrawlerEquipmentId());

        if(null == list || 0 == list.size()){
            return ServerResponse.createByErrorMessage("没有数据");
        }

        return ServerResponse.createBySuccess("查询成功",list);
    }

    /**
     * 编辑爬虫设备数据
     *
     * @param cpCrawlerEquipmentDTO
     * @param baseUserVO
     * @return
     */
    @Override
    public ServerResponse<String> updateCrawlerEquipment(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO, BaseUserVO baseUserVO) {

        if(null == cpCrawlerEquipmentDTO ||  null == baseUserVO){
            return ServerResponse.createByErrorMessage("参数错误");
        }

        Date now = new Date();

        CpCrawlerEquipment cpCrawlerEquipment  = new CpCrawlerEquipment();


        cpCrawlerEquipment.setId(cpCrawlerEquipmentDTO.getCpCrawlerEquipmentId());
        cpCrawlerEquipment.setEquipmentName(cpCrawlerEquipmentDTO.getEquipmentName());
        cpCrawlerEquipment.setAmount(cpCrawlerEquipmentDTO.getAmount());
        cpCrawlerEquipment.setPrice(cpCrawlerEquipmentDTO.getPrice());
        cpCrawlerEquipment.setQuantity(cpCrawlerEquipmentDTO.getQuantity());
        cpCrawlerEquipment.setCompanyName(cpCrawlerEquipmentDTO.getCompanyName());
        cpCrawlerEquipment.setAnnouncementName(cpCrawlerEquipmentDTO.getAnnouncementName());
        cpCrawlerEquipment.setPurchaseName(cpCrawlerEquipmentDTO.getPurchaseName());
        cpCrawlerEquipment.setManufacturerName(cpCrawlerEquipmentDTO.getManufacturerName());
        cpCrawlerEquipment.setModel(cpCrawlerEquipmentDTO.getModel());
        cpCrawlerEquipment.setUpdateTime(now);
        cpCrawlerEquipment.setUpdateUser(baseUserVO.getUserName());
        cpCrawlerEquipment.setCityName(cpCrawlerEquipmentDTO.getCityName());
        if(StringUtil.isNotEmpty(cpCrawlerEquipmentDTO.getIssueTime()))
        {
            cpCrawlerEquipment.setIssueTime(DateTimeUtil.strToDate(cpCrawlerEquipmentDTO.getIssueTime()));
        }

        int count = cpCrawlerEquipmentMapper.updateByPrimaryKeySelective(cpCrawlerEquipment);

        if(count > 0)
        {
            return ServerResponse.createBySuccessMessage("编辑成功");
        }else
        {
            return ServerResponse.createByErrorMessage("编辑失败");
        }

    }

    /**
     * 新增爬虫设备数据
     *
     * @param cpCrawlerEquipmentDTO
     * @param baseUserVO
     * @return
     */
    @Override
    public ServerResponse<String> xinAddCrawlerEquipment(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO, BaseUserVO baseUserVO) {

        if(null == cpCrawlerEquipmentDTO ||  null == baseUserVO){
            return ServerResponse.createByErrorMessage("参数错误");
        }

        Date now  = new Date();

        CpCrawlerEquipment  cpCrawlerEquipment  = cpCrawlerEquipmentMapper.selectByPrimaryKey(cpCrawlerEquipmentDTO.getCpCrawlerEquipmentId());

        cpCrawlerEquipment.setId(cpCrawlerEquipmentMapper.selectSeq());
        cpCrawlerEquipment.setCreateUser(baseUserVO.getUserName());
        cpCrawlerEquipment.setCreateTime(cpCrawlerEquipment.getCreateTime());
        cpCrawlerEquipment.setUpdateUser(baseUserVO.getUserName());
        cpCrawlerEquipment.setUpdateTime(cpCrawlerEquipment.getUpdateTime());
        cpCrawlerEquipmentMapper.insertSelective(cpCrawlerEquipment);


        //招标参数列表
        List<CpCrawlerConfigurationVO> list =  cpCrawlerConfigurationMapper.selectCrawlerConfigurationList(cpCrawlerEquipmentDTO.getCpCrawlerEquipmentId());


        if(list != null  &&  list.size() != 0)
        {
            for(CpCrawlerConfigurationVO  cpCrawlerConfigurationVO : list)
            {

                CpCrawlerConfiguration record  =  new CpCrawlerConfiguration();
                record.setId(cpCrawlerConfigurationMapper.selectSeq());
                record.setCrawlerId(cpCrawlerEquipment.getId());
                record.setCrawlerLink(cpCrawlerConfigurationVO.getCrawlerLink());

                cpCrawlerConfigurationMapper.insertSelective(record);
            }

        }

        return ServerResponse.createBySuccessMessage("新增成功");
    }

    /**
     * 批量移到回收站，或者批量恢复
     *
     * @param cpCrawlerEquipmentDTO
     * @param baseUserVO
     * @return
     */
    @Override
    public ServerResponse<String> updateStatusCrawlerEquipment(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO, BaseUserVO baseUserVO) {

        if(null == cpCrawlerEquipmentDTO ||  null == baseUserVO){
            return ServerResponse.createByErrorMessage("参数错误");
        }

        String[] split = cpCrawlerEquipmentDTO.getCpCrawlerEquipmentIds().split(",");
        List<String> asList = Arrays.asList(split);

        if(asList != null  &&  asList.size() != 0)
        {
            for(String id : asList)
            {
               if(id != null  &&  id != "")
               {
                   CpCrawlerEquipment record  =  new CpCrawlerEquipment();
                   record.setId(NumberUtil.safeToLong(id));
                   record.setStatus(cpCrawlerEquipmentDTO.getStatus());

                   cpCrawlerEquipmentMapper.updateByPrimaryKeySelective(record);

               }
            }

        }

        return ServerResponse.createBySuccessMessage("操作成功");
    }

    /**
     * 批量彻底删除
     *
     * @param cpCrawlerEquipmentDTO
     * @param baseUserVO
     * @return
     */
    @Override
    public ServerResponse<String> deleteCrawlerEquipment(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO, BaseUserVO baseUserVO) {
        if(null == cpCrawlerEquipmentDTO ||  null == baseUserVO){
            return ServerResponse.createByErrorMessage("参数错误");
        }

        String[] split = cpCrawlerEquipmentDTO.getCpCrawlerEquipmentIds().split(",");
        List<String> asList = Arrays.asList(split);

        if(asList != null  &&  asList.size() != 0)
        {
            for(String id : asList)
            {
                if(id != null  &&  id != "")
                {
                    //彻底删除设备
                    cpCrawlerEquipmentMapper.deleteByPrimaryKey(NumberUtil.safeToLong(id));

                    //彻底删除招标配置
                    cpCrawlerConfigurationMapper.deleteCrawlerConfiguration(id);
                }
            }

        }

        return ServerResponse.createBySuccessMessage("操作成功");
    }

    /**
     * 前端查询接口
     *
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    @Override
    public ServerResponse<PageInfo> getCrawlerEquipmentBeforelist(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO) {
        if(null == cpCrawlerEquipmentDTO){
            return ServerResponse.createByErrorMessage("参数错误");
        }
        if(null == cpCrawlerEquipmentDTO.getPageNum()){
            cpCrawlerEquipmentDTO.setPageNum(Const.INITPAGENUM);
        }
        if(null == cpCrawlerEquipmentDTO.getPageSize()){
            cpCrawlerEquipmentDTO.setPageSize(Const.INITPAGESIZE);
        }
        PageHelper.startPage(cpCrawlerEquipmentDTO.getPageNum(),cpCrawlerEquipmentDTO.getPageSize(),true,false,null);

        if(StringUtil.isNotEmpty(cpCrawlerEquipmentDTO.getBeforeOrAfter())  &&  StringUtil.isNotEmpty(cpCrawlerEquipmentDTO.getOrderBy()))
        {
            if(1 ==cpCrawlerEquipmentDTO.getBeforeOrAfter())
            {
                if(2 == cpCrawlerEquipmentDTO.getOrderBy())
                {
                    PageHelper.orderBy("team.issue_time");
                }else
                {
                    PageHelper.orderBy("team.price");
                }
            }

            if(2 == cpCrawlerEquipmentDTO.getBeforeOrAfter())
            {
                if(2 == cpCrawlerEquipmentDTO.getOrderBy())
                {
                    PageHelper.orderBy("team.issue_time DESC");
                }else
                {
                    PageHelper.orderBy("team.price DESC");
                }
            }
        }else
        {
            PageHelper.orderBy("team.price");
        }


        if(StringUtil.isNotEmpty(cpCrawlerEquipmentDTO.getModel()))
        {
            String  modelAdd   =  cpCrawlerEquipmentDTO.getModel().replaceAll("[\\pP\\p{Punct}]", "");
            modelAdd =  modelAdd.replace(" ", "");

            cpCrawlerEquipmentDTO.setModel(modelAdd);
        }

        List<CpCrawlerEquipmentVO> list = cpCrawlerEquipmentMapper.getCrawlerEquipmentBeforelist(cpCrawlerEquipmentDTO);

        if(null == list || 0 == list.size()){
            return ServerResponse.createByErrorMessage("没有数据");
        }

        return ServerResponse.createBySuccess("查询成功",new PageInfo(list));
    }

    /**
     * 获取OA设备招标配置
     *
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    @Override
    public ServerResponse<BaseAttachmentVO> getOaConfiguration(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO) {
        if(null == cpCrawlerEquipmentDTO){
            return ServerResponse.createByErrorMessage("参数错误");
        }

        Map map = new HashMap(16);
        map.put("fileId",cpCrawlerEquipmentDTO.getFileId());
        String result = HttpUtil.get(url6,map);
        if(StringUtil.isNotEmpty(result)) {
            //查询附件信息
            BaseAttachmentVO baseAttachmentVO = JSON.parseObject(result, BaseAttachmentVO.class);
            if(null != baseAttachmentVO){
                baseAttachmentVO.setFileDownPath(fileDownUrl+baseAttachmentVO.getFileDownPath());
            }

            return ServerResponse.createBySuccess("查询成功",baseAttachmentVO);
        }else
        {
            return ServerResponse.createByErrorMessage("没有数据");
        }

    }

    /**
     * 查看爬虫设备招标配置
     *
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    @Override
    public ServerResponse<List<CpCrawlerConfigurationVO>> getCwConfiguration(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO) {

        if(null == cpCrawlerEquipmentDTO){
            return ServerResponse.createByErrorMessage("参数错误");
        }

        //招标参数列表
        List<CpCrawlerConfigurationVO> list =  cpCrawlerConfigurationMapper.selectCrawlerConfigurationList(cpCrawlerEquipmentDTO.getCpCrawlerEquipmentId());

        if(null == list || 0 == list.size()){
            return ServerResponse.createByErrorMessage("没有数据");
        }

        return ServerResponse.createBySuccess("查询成功",list);
    }

    /**
     * 统计数据查询
     *
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    @Override
    public ServerResponse<CrawlerEquipmentSumVO> getCrawlerEquipmentSum(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO) {

        if(null == cpCrawlerEquipmentDTO){
            return ServerResponse.createByErrorMessage("参数错误");
        }

        if(StringUtil.isNotEmpty(cpCrawlerEquipmentDTO.getModel()))
        {
            String  modelAdd   =  cpCrawlerEquipmentDTO.getModel().replaceAll("[\\pP\\p{Punct}]", "");
            modelAdd =  modelAdd.replace(" ", "");

            cpCrawlerEquipmentDTO.setModel(modelAdd);
        }

        CrawlerEquipmentSumVO  ce =  cpCrawlerEquipmentMapper.getCrawlerEquipmentSum(cpCrawlerEquipmentDTO);

        if(ce != null)
        {

            ce.setAvgPrice( ce.getAvgPrice().setScale(2, RoundingMode.HALF_UP));

            //最低时间
            cpCrawlerEquipmentDTO.setMinOrMax(ce.getMinPrice());

            String  minPriceDate  =  cpCrawlerEquipmentMapper.getMinPriceDate(cpCrawlerEquipmentDTO);

            ce.setMinPriceDate(minPriceDate);


            //最高时间
            cpCrawlerEquipmentDTO.setMinOrMax(ce.getMaxPrice());

            String  maxPriceDate  =  cpCrawlerEquipmentMapper.getMinPriceDate(cpCrawlerEquipmentDTO);

            ce.setMaxPriceDate(maxPriceDate);

            return ServerResponse.createBySuccess("查询成功",ce);

        }else
        {
            return ServerResponse.createByErrorMessage("没有数据");
        }


    }

    /**
     * 获取省市信息
     *
     * @return
     */
    @Override
    public ServerResponse<List<CpCityNameVO>> getCpCityNameVO( Integer isRelease) {

        List<CpCityNameVO>  list = cpCrawlerEquipmentMapper.getCpCityNameVO(isRelease);

        if(null == list  && list.size() == 0)
        {
            return ServerResponse.createByErrorMessage("没有数据");
        }

        return ServerResponse.createBySuccess("查询成功",list);

    }

    /**
     * 处理数据
     *
     * @param cpCrawlerEquipmentDTO
     * @param baseUserVO
     * @return
     */
    @Override
    public ServerResponse<String> setRelease(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO, BaseUserVO baseUserVO) {

        if(null == cpCrawlerEquipmentDTO ||  null == baseUserVO){
            return ServerResponse.createByErrorMessage("参数错误");
        }

        //兼容上版本接口
        if(cpCrawlerEquipmentDTO.getIsRelease() == null || cpCrawlerEquipmentDTO.getIsRelease() == 0 ) {
            cpCrawlerEquipmentDTO.setIsRelease(1);
        }

        String[] split = cpCrawlerEquipmentDTO.getCpCrawlerEquipmentIds().split(",");
        List<String> asList = Arrays.asList(split);

        if(asList != null  &&  asList.size() != 0)
        {
            for(String id : asList)
            {
                if(id != null  &&  id != "")
                {
                    //处理数据更新
                    CpCrawlerEquipment record  =  new CpCrawlerEquipment();

                    record.setId(NumberUtil.safeToLong(id));
                    record.setIsRelease(cpCrawlerEquipmentDTO.getIsRelease());
                    record.setReleaseTime(new Date());
                    record.setUpdateTime(new Date());
                    record.setUpdateUser(baseUserVO.getUserName());

                    cpCrawlerEquipmentMapper.updateByPrimaryKeySelective(record);


                }
            }

        }

        return ServerResponse.createBySuccessMessage("操作成功");
    }

    /**
     * 同步型号缩写
     *
     * @return
     */
    @Override
    public ServerResponse<String> mpdelAdd() {

        List<CpCrawlerEquipmentVO> list = cpCrawlerEquipmentMapper.mpdelAdd();

        for(CpCrawlerEquipmentVO cpCrawlerEquipmentVO : list)
        {
            if(StringUtil.isNotEmpty(cpCrawlerEquipmentVO.getModel()))
            {
                String  modelAdd   =  cpCrawlerEquipmentVO.getModel().replaceAll("[\\pP\\p{Punct}]", "");
                modelAdd =  modelAdd.replace(" ", "");
                CpCrawlerEquipment record = new CpCrawlerEquipment();
                record.setId(cpCrawlerEquipmentVO.getCpCrawlerEquipmentId());
                record.setModelAbb(modelAdd);
                cpCrawlerEquipmentMapper.updateByPrimaryKeySelective(record);
            }
        }


        return ServerResponse.createBySuccessMessage("完成");
    }

    /**
     * 获取制造商名称
     *
     * @param cpCrawlerEquipmentDTO
     * @param baseUserVO
     * @return
     */
    @Override
    public ServerResponse<PageInfo> getManufacturerName(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO, BaseUserVO baseUserVO) {

        if(null == cpCrawlerEquipmentDTO ||  null == baseUserVO){
            return ServerResponse.createByErrorMessage("参数错误");
        }

        if(null == cpCrawlerEquipmentDTO.getPageNum()){
            cpCrawlerEquipmentDTO.setPageNum(Const.INITPAGENUM);
        }
        if(null == cpCrawlerEquipmentDTO.getPageSize()){
            cpCrawlerEquipmentDTO.setPageSize(Const.INITPAGESIZE);
        }
        PageHelper.startPage(cpCrawlerEquipmentDTO.getPageNum(),cpCrawlerEquipmentDTO.getPageSize(),true,false,null);
        PageHelper.orderBy("team.issue_time desc");

        List<CpCrawlerEquipmentVO> list = cpCrawlerEquipmentMapper.getManufacturerName(cpCrawlerEquipmentDTO);


        if(null == list || 0 == list.size()){
            return ServerResponse.createByErrorMessage("没有数据");
        }

        return ServerResponse.createBySuccess("查询成功",new PageInfo(list));
    }

    /**
     * 同步更新设备数据
     *
     * @return
     */
    @Override
    public ServerResponse<String> updateSynchronous()throws ParseException {

        List<Map>  pelist  =  cpCrawlerEquipmentMapper.getUpdateProvinceEquipment();

        if(pelist != null  &&  pelist.size() != 0)
        {

            for(Map  map : pelist)
            {

                CpCrawlerEquipmentVO cpCrawlerEquipmentVO = cpCrawlerEquipmentMapper.getSysCpCrawlerEquipmentVO(StringUtil.valueOfString(map.get("num_id")));


                //同步设备招标参数
                List<Map>  cMap  =    cpCrawlerConfigurationMapper.getCrawlerConfiguration(StringUtil.valueOfString(map.get("num_id")));

                if(cMap != null  &&  cMap.size() != 0)
                {
                    for(Map reMap :  cMap)
                    {

                        CpCrawlerConfiguration cpCrawlerConfiguration  =  new CpCrawlerConfiguration();


                        cpCrawlerConfiguration.setCrawlerId(cpCrawlerEquipmentVO.getCpCrawlerEquipmentId());
                        cpCrawlerConfiguration.setCrawlerLink(StringUtil.valueOfString(reMap.get("parameter")));
                        cpCrawlerConfiguration.setSynchronousId(NumberUtil.safeToLong(StringUtil.valueOfString(reMap.get("fu_id"))));

                        CpCrawlerConfiguration cc  = cpCrawlerConfigurationMapper.countConfiguration(cpCrawlerConfiguration.getSynchronousId());

                        if(cc != null)
                        {
                            cpCrawlerConfiguration.setId(cc.getId());
                            cpCrawlerConfigurationMapper.updateByPrimaryKeySelective(cpCrawlerConfiguration);
                        }else
                        {
                            cpCrawlerConfiguration.setId(cpCrawlerConfigurationMapper.selectSeq());
                            cpCrawlerConfigurationMapper.insertSelective(cpCrawlerConfiguration);
                        }


                    }
                }


                //重置爬虫表
                cpCrawlerEquipmentMapper.updateProvinceEquipment(NumberUtil.safeToLong(StringUtil.valueOfString(map.get("num_id"))));

            }


        }
        return ServerResponse.createBySuccessMessage("操作成功");
    }


}
