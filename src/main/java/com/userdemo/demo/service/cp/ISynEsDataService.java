package com.userdemo.demo.service.cp;

import com.userdemo.demo.common.sys.ServerResponse;

/**
 * 　　* @description: es数据同步服务层
 * 　　* @param
 * 　　* @return
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $ $
 *
 */
public interface ISynEsDataService {

    /**
     * 查询OA中标设备表数据
     * @param maxId
     * @return
     */
    String selectAnnouncementEquipment(Long maxId,Long minId);


    /**
     * 查询爬虫设备表数据
     * @param maxId
     * @return
     */
    String selectCrawlerEquipment(Long maxId,Long minId);


    /**
     * 查询OA中标设备表词汇
     * @param maxId
     * @return
     */
    String selectAnnouncementEquipmentSuggest(Long maxId,Long minId);


    /**
     * 查询爬虫设备表词汇
     * @param maxId
     * @return
     */
    String selectCrawlerEquipmentSuggest(Long maxId,Long minId);





}
