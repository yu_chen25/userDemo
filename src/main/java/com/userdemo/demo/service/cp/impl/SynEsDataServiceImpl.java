package com.userdemo.demo.service.cp.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.userdemo.demo.base.BaseService;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.entity.cp.vo.BaseAttachmentVO;
import com.userdemo.demo.service.cp.ISynEsDataService;
import com.userdemo.demo.util.base.NumberUtil;
import com.userdemo.demo.util.base.StringUtil;
import com.xiaoleilu.hutool.http.HttpUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 　　* @description: es数据同步服务层
 * 　　* @param
 * 　　* @return
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $ $
 *
 */

@Service("iSynEsDataService")
public class SynEsDataServiceImpl extends BaseService implements ISynEsDataService {

    @Value("${file.url6}")
    private String url6;

    @Value("${file.downUrl}")
    private String fileDownUrl;

    /**
     * 查询OA中标设备表数据
     *
     * @param maxId
     * @return
     */
    @Override
    public String selectAnnouncementEquipment(Long maxId,Long minId) {
        Map resultMap = new HashMap(16);
        List<Map> allList = Lists.newArrayList();
        //查询有附件的
        List<Map> resutList = synEsDataMapper.selectAnnouncementEquipment(maxId,minId,2);
        if(null != resutList && 0 < resutList.size()){
            for (Map map : resutList){
                Long fileId = NumberUtil.safeToLong(map.get("file_id"));
                String result = HttpUtil.get(url6+"?fileId="+fileId);
                if(StringUtil.isNotEmpty(result)) {
                    //查询附件信息
                    BaseAttachmentVO baseAttachmentVO = JSON.parseObject(result, BaseAttachmentVO.class);
                    if (null != baseAttachmentVO) {
                        map.put("fileUrl",fileDownUrl + baseAttachmentVO.getFileDownPath());
                    }
                }
            }
            allList.addAll(resutList);
        }
        //查询没有附件的
        List<Map> resultList2 = synEsDataMapper.selectAnnouncementEquipment(maxId,minId,1);
        if(null != resultList2 && 0 < resultList2.size()){
            allList.addAll(resultList2);
        }
        if(null != allList && 0 < allList.size()){
            for (Map map : allList){
                map.put("param",map.get("productName")+","+map.get("manufactor")+","+map.get("allManufactor")+","+map.get("productModel")+","+map.get("productSpecification"));

                //移除五一的附件
                if(StringUtil.isNotEmpty(map.get("purchAgent"))
                 && "湖南五一招标有限公司".equals(StringUtil.valueOfString(map.get("purchAgent"))) )
                {
                    map.remove("file_id");
                    map.put("fileUrl","");
                }
            }
            resultMap.put("allList",allList);
            resultMap.put("maxId",synEsDataMapper.selectAnnouncementEquipmentMaxId());
            return JSONObject.toJSONString(ServerResponse.createBySuccess("查询成功",resultMap));
        }
        return JSONObject.toJSONString(ServerResponse.createByErrorMessage("查询失败"));
    }

    /**
     * 查询爬虫设备表数据
     *
     * @param maxId
     * @return
     */
    @Override
    public String selectCrawlerEquipment(Long maxId,Long minId) {
        Map resultMap = new HashMap(16);
        List<Map> resutList = synEsDataMapper.selectCrawlerEquipment(maxId,minId);
        if(null != resutList && 0 < resutList.size()){
            for (Map map : resutList){
                map.put("param",map.get("productName")+","+map.get("allManufactor")+","+map.get("productModel"));
            }
            resultMap.put("allList",resutList);
            resultMap.put("maxId",synEsDataMapper.selectCrawlerEquipmentMaxId());
            return JSONObject.toJSONString(ServerResponse.createBySuccess("查询成功",resultMap));
        }
        return JSONObject.toJSONString(ServerResponse.createByErrorMessage("查询失败"));
    }

    /**
     * 查询OA中标设备表词汇
     *
     * @param maxId
     * @return
     */
    @Override
    public String selectAnnouncementEquipmentSuggest(Long maxId,Long minId) {
        if(null == maxId){
            maxId = 0L;
        }
        Map resultMap = new HashMap(16);
        Set<String> resultSet = Sets.newHashSet();
        List<Map> mapList = synEsDataMapper.selectAnnouncementEquipmentSuggest(maxId,minId);
        if(null != mapList && 0 < mapList.size()){
            for(Map map : mapList){
                String productName = StringUtil.valueOfString(map.get("productName"));
                String manufactor = StringUtil.valueOfString(map.get("manufactor"));
                String allManufactor = StringUtil.valueOfString(map.get("allManufactor"));
                String productModel = StringUtil.valueOfString(map.get("productModel"));
                String productSpecification = StringUtil.valueOfString(map.get("productSpecification"));
                if(StringUtil.isNotEmpty(productName)){
                    resultSet.add(productName);
                }
                if(StringUtil.isNotEmpty(manufactor)){
                    resultSet.add(manufactor);
                }
                if(StringUtil.isNotEmpty(allManufactor)){
                    resultSet.add(allManufactor);
                }
                if(StringUtil.isNotEmpty(productModel)){
                    resultSet.add(productModel);
                }
                if(StringUtil.isNotEmpty(productSpecification)){
                    resultSet.add(productSpecification);
                }
            }
        }
        if(resultSet.size() > 0){
            resultMap.put("set",resultSet);
            resultMap.put("maxId",synEsDataMapper.selectAnnouncementEquipmentMaxId());
            return JSONObject.toJSONString(ServerResponse.createBySuccess("查询成功",resultMap));
        }
        return JSONObject.toJSONString(ServerResponse.createByErrorMessage("查询失败"));
    }

    /**
     * 查询爬虫设备表词汇
     *
     * @param maxId
     * @return
     */
    @Override
    public String selectCrawlerEquipmentSuggest(Long maxId,Long minId) {
        if(null == maxId){
            maxId = 0L;
        }
        Set<String> resultSet = Sets.newHashSet();
        List<Map> mapList = synEsDataMapper.selectCrawlerEquipmentSuggest(maxId,minId);
        if(null != mapList && 0 < mapList.size()){
            for(Map map : mapList){
                String productName = StringUtil.valueOfString(map.get("productName"));
                String manufactor = StringUtil.valueOfString(map.get("manufactor"));
                String productModel = StringUtil.valueOfString(map.get("productModel"));
                String productSpecification = StringUtil.valueOfString(map.get("productSpecification"));
                if(StringUtil.isNotEmpty(productName)){
                    resultSet.add(productName);
                }
                if(StringUtil.isNotEmpty(manufactor)){
                    resultSet.add(manufactor);
                }
                if(StringUtil.isNotEmpty(productModel)){
                    resultSet.add(productModel);
                }
                if(StringUtil.isNotEmpty(productSpecification)){
                    resultSet.add(productSpecification);
                }

            }
        }
        if(resultSet.size() > 0){
            Map resultMap = new HashMap(16);
            resultMap.put("set",resultSet);
            resultMap.put("maxId",synEsDataMapper.selectCrawlerEquipmentMaxId());
            return JSONObject.toJSONString(ServerResponse.createBySuccess("查询成功",resultMap));
        }
        return JSONObject.toJSONString(ServerResponse.createByErrorMessage("查询失败"));
    }
}
