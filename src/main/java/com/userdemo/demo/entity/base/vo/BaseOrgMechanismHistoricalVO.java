package com.userdemo.demo.entity.base.vo;

import lombok.Data;

@Data
public class BaseOrgMechanismHistoricalVO {

    private Long orgHistoricalId;

    private Long orgId;

    private String orgName;
}
