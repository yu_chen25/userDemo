package com.userdemo.demo.entity.base.vo;

import java.util.List;

/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public class BaseRoleVO {

    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色描述
     */
    private String roleDescInfo;

    /**
     * 启用禁用中文
     */
    private String isPublishName;

    /**
     * 启用禁用(papa_id:22,1.启用 2.禁用 3.保存)
     */
    private Integer  isPublish;

    /**
     * 使用类型
     */
    private Integer useType;

    /**
     * 创建者组织机构ID
     */
    private Long createOrgId;

    /**
     * 创建者组织机构顶级ID
     */
    private Long createTopId;

    /**
     * 角色拥有的用户集合
     */
    private List<Long> userIds;

    /**
     * 角色组织机构ID
     */
    private List<Long> rmIds;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleDescInfo() {
        return roleDescInfo;
    }

    public void setRoleDescInfo(String roleDescInfo) {
        this.roleDescInfo = roleDescInfo;
    }

    public String getIsPublishName() {
        return isPublishName;
    }

    public void setIsPublishName(String isPublishName) {
        this.isPublishName = isPublishName;
    }

    public Integer getIsPublish() {
        return isPublish;
    }

    public void setIsPublish(Integer isPublish) {
        this.isPublish = isPublish;
    }


    public List<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Long> userIds) {
        this.userIds = userIds;
    }

    public List<Long> getRmIds() {
        return rmIds;
    }

    public void setRmIds(List<Long> rmIds) {
        this.rmIds = rmIds;
    }

    public Integer getUseType() {
        return useType;
    }

    public void setUseType(Integer useType) {
        this.useType = useType;
    }

    public Long getCreateOrgId() {
        return createOrgId;
    }

    public void setCreateOrgId(Long createOrgId) {
        this.createOrgId = createOrgId;
    }

    public Long getCreateTopId() {
        return createTopId;
    }

    public void setCreateTopId(Long createTopId) {
        this.createTopId = createTopId;
    }
}
