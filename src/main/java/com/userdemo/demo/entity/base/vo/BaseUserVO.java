package com.userdemo.demo.entity.base.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
@Data
public class BaseUserVO implements Serializable {

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户登录名
     */
    private String userName;

    /**
     * 用户昵称
     */
    private String displayName;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * 用户手机
     */
    private String phone;

    /**
     * 用户角色ID
     */
    private Long roleId;

    /**
     * 权限名称
     */
    private String roleName;

    /**
     * 用户机构ID
     */
    private Long orgId;

    /**
     * 组织机构代码
     */
    private String orgCode;

    /**
     * 组织机构名称
     */
    private String orgName;

    /**
     * 备注
     */
    private String remark;

    /**
     * 性别
     */
    private Integer sex;

    /**
     * 数据状态
     */
    private Integer status;

    /**
     * 数据状态名称
     */
    private String statusName;


    /**
     * 是否在职
     */
    private Integer isIn;

    /**
     * 是否在职名称
     */
    private String isInName;

    /**
     * 缓存标识
     */
    private String LoginToken;

    /**
     * 菜单ID集合
     */
    private String menuIds;


    /**
     * 角色编码集合
     */
    private String roleCodes;

    /**
     * 创建人组织机构ID
     */
    private Long createOrgId;

    /**
     * 创建人组织机构名称
     */
    private String createOrgName;

    /**
     * 创建人顶级组织机构ID
     */
    private Long createTopOrgId;

    /**
     * 创建人顶级组织机构名称
     */
    private String createTopOrgName;

    /**
     * 使用类型
     */
    private Integer useType;

    /**
     * 使用类型名称
     */
    private String useTypeName;

    /**
     * 公司名称
     */
    private Long companyId;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 角色集合
     */
    private List<Long> roldIds;

    /**
     * 角色组织机构ID
     */
    private List<Long> rmIds;

    /**
     * 域名
     */
    private String domainName;

    /**
     * 是否有CA
     */
    private Integer isHaveCa;

    /**
     * 公司联系人
     */
    private String concat;

    /**
     * 公司联系人手机号
     */
    private String concatPhone;


    /**
     * 公司联系地址
     */
    private String concatAddress;

    /**
     * 公司法定代表人
     */
    private String legalRepresentative;


    /**
     * 社会信用代码
     */
    private String organizationalCode;


    /**
     * 是否有签名图片
     */
    private Integer isHaveImage;

}
