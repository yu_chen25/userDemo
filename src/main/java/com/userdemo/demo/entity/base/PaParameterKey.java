package com.userdemo.demo.entity.base;

public class PaParameterKey {
    private Long paraId;

    private Long papaId;

    public PaParameterKey(Long paraId, Long papaId) {
        this.paraId = paraId;
        this.papaId = papaId;
    }

    public PaParameterKey() {
        super();
    }

    public Long getParaId() {
        return paraId;
    }

    public void setParaId(Long paraId) {
        this.paraId = paraId;
    }

    public Long getPapaId() {
        return papaId;
    }

    public void setPapaId(Long papaId) {
        this.papaId = papaId;
    }
}