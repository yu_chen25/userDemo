package com.userdemo.demo.entity.base.vo;

/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public class BaseLogVO {

    /**
     * 主键ID
     */
    private Long baseLogId;

    /**
     * 请求类型
     */
    private String type;

    /**
     * 日志标题
     */
    private String title;

    /**
     * 操作IP地址操作IP地址
     */
    private String remoteAddr;

    /**
     * 操作用户名
     */
    private String userName;

    /**
     * 操作用户昵称
     */
    private String displayName;

    /**
     * 请求URI
     */
    private String requestUri;

    /**
     * 操作方式
     */
    private String httpMethod;


    /**
     * 请求类型.方法
     */
    private String classMethod;

    /**
     * 操作提交的数据id
     */
    private Long paramsId;

    /**
     * 提交内容数据
     */
    private String paramsStr;

    /**
     * sessionId
     */
    private String sessionId;

    /**
     * 返回内容id
     */
    private Long responseId;

    /**
     * 返回内容数据
     */
    private String responseStr;

    /**
     * 方法执行时间
     */
    private Long useTime;

    /**
     * 浏览器信息
     */
    private String browser;

    /**
     * 地区
     */
    private String area;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 网络服务提供商
     */
    private String isp;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 创建人组织机构ID
     */
    private Long createOrgId;

    /**
     * 创建组织机构名称
     */
    private String createOrgName;

    /**
     * 创建人顶级组织机构
     */
    private Long createTopOrgId;

    /**
     * 创建人顶级组织机构名称
     */
    private String createTopOrgName;

    /**
     * 使用类型
     */
    private Integer useType;

    /**
     * 使用类型名称
     */
    private String useTypeName;

    public String getCreateOrgName() {
        return createOrgName;
    }

    public void setCreateOrgName(String createOrgName) {
        this.createOrgName = createOrgName;
    }

    public String getCreateTopOrgName() {
        return createTopOrgName;
    }

    public void setCreateTopOrgName(String createTopOrgName) {
        this.createTopOrgName = createTopOrgName;
    }

    public String getUseTypeName() {
        return useTypeName;
    }

    public void setUseTypeName(String useTypeName) {
        this.useTypeName = useTypeName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getParamsStr() {
        return paramsStr;
    }

    public void setParamsStr(String paramsStr) {
        this.paramsStr = paramsStr;
    }

    public String getResponseStr() {
        return responseStr;
    }

    public void setResponseStr(String responseStr) {
        this.responseStr = responseStr;
    }

    public Long getBaseLogId() {
        return baseLogId;
    }

    public Long getCreateOrgId() {
        return createOrgId;
    }

    public void setCreateOrgId(Long createOrgId) {
        this.createOrgId = createOrgId;
    }

    public Long getCreateTopOrgId() {
        return createTopOrgId;
    }

    public void setCreateTopOrgId(Long createTopOrgId) {
        this.createTopOrgId = createTopOrgId;
    }

    public Integer getUseType() {
        return useType;
    }

    public void setUseType(Integer useType) {
        this.useType = useType;
    }

    public void setBaseLogId(Long baseLogId) {
        this.baseLogId = baseLogId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRemoteAddr() {
        return remoteAddr;
    }

    public void setRemoteAddr(String remoteAddr) {
        this.remoteAddr = remoteAddr;
    }

    public String getRequestUri() {
        return requestUri;
    }

    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getClassMethod() {
        return classMethod;
    }

    public void setClassMethod(String classMethod) {
        this.classMethod = classMethod;
    }

    public Long getParamsId() {
        return paramsId;
    }

    public void setParamsId(Long paramsId) {
        this.paramsId = paramsId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Long getResponseId() {
        return responseId;
    }

    public void setResponseId(Long responseId) {
        this.responseId = responseId;
    }

    public Long getUseTime() {
        return useTime;
    }

    public void setUseTime(Long useTime) {
        this.useTime = useTime;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getIsp() {
        return isp;
    }

    public void setIsp(String isp) {
        this.isp = isp;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
