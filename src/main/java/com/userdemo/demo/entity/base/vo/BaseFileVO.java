package com.userdemo.demo.entity.base.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 　　* @description: 附件
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
@Data
public class BaseFileVO implements Serializable {

    /**
     * 附件ID
     */
    private Long baseAliyunossFileId;

    /**
     * 附件名称
     */
    private String fileName;


    /**
     * 附件下载地址
     */
    private String fileDownPath;

}
