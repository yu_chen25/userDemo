package com.userdemo.demo.entity.base.dto;

/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public class PaParameterDTO {

    /**
     * 参数ID
     */
    private Long paraId;

    /**
     * 参数父ID
     */
    private Long papaId;

    /**
     * 参数名称
     */
    private String paName;

    public Long getParaId() {
        return paraId;
    }

    public void setParaId(Long paraId) {
        this.paraId = paraId;
    }

    public Long getPapaId() {
        return papaId;
    }

    public void setPapaId(Long papaId) {
        this.papaId = papaId;
    }

    public String getPaName() {
        return paName;
    }

    public void setPaName(String paName) {
        this.paName = paName;
    }
}
