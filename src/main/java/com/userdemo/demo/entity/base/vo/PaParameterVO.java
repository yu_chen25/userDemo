package com.userdemo.demo.entity.base.vo;

/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public class PaParameterVO {

    /**
     * 枚举Id
     */
    private Long paraId;

    /**
     * 父参数ID
     */
    private Long papaId;

    /**
     * 枚举名称
     */
    private String paName;


    public Long getParaId() {
        return paraId;
    }

    public void setParaId(Long paraId) {
        this.paraId = paraId;
    }

    public Long getPapaId() {
        return papaId;
    }

    public void setPapaId(Long papaId) {
        this.papaId = papaId;
    }

    public String getPaName() {
        return paName;
    }

    public void setPaName(String paName) {
        this.paName = paName;
    }
}
