package com.userdemo.demo.entity.base;

import java.util.Date;

public class BaseLogBigfield {
    private Long id;

    private Integer version;

    private Date createTime;

    private Date updateTime;

    private String bigtext;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getBigtext() {
        return bigtext;
    }

    public void setBigtext(String bigtext) {
        this.bigtext = bigtext == null ? null : bigtext.trim();
    }
}