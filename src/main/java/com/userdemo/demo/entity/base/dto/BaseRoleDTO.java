package com.userdemo.demo.entity.base.dto;

import java.util.List;

/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public class BaseRoleDTO {


    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色编码
     */
    private String roleCode;

    /**
     * 角色描述
     */
    private String roleDescInfo;

    /**
     * 页码
     */
    private Integer pageNum;

    /**
     * 展示最大数
     */
    private Integer pageSize;

    /**
     * 组织机构ID
     */
    private Long createOrgId;

    /**
     * 启用禁用(papa_id:22,1.启用 2.禁用 3.保存)
     */
    private Integer isPublish;

    /**
     * 公司ID
     */
    private Long companyId;

    /**
     * 设置权限组织机构ID 字符串 用,号分割
     */
    private String orgIds;

    /**
     * 批量插入用
     */
    private List<String>  asList;

    /**
     * 用户名称
     */
    private String  userName;

    /**
     * 创建人顶级组织机构ID
     */
    private Long createTopOrgId;

    /**
     * 使用类型
     */
    private Integer useType;


    /**
     * 用户名称集合
     */
    private String  userNames;

    /**
     * 角色集合
     */
    private String  roleIds;

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleDescInfo() {
        return roleDescInfo;
    }

    public void setRoleDescInfo(String roleDescInfo) {
        this.roleDescInfo = roleDescInfo;
    }

    public Long getCreateOrgId() {
        return createOrgId;
    }

    public void setCreateOrgId(Long createOrgId) {
        this.createOrgId = createOrgId;
    }

    public Integer getIsPublish() {
        return isPublish;
    }

    public void setIsPublish(Integer isPublish) {
        this.isPublish = isPublish;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getOrgIds() {
        return orgIds;
    }

    public void setOrgIds(String orgIds) {
        this.orgIds = orgIds;
    }

    public List<String> getAsList() {
        return asList;
    }

    public void setAsList(List<String> asList) {
        this.asList = asList;
    }

    public Long getCreateTopOrgId() {
        return createTopOrgId;
    }

    public void setCreateTopOrgId(Long createTopOrgId) {
        this.createTopOrgId = createTopOrgId;
    }

    public Integer getUseType() {
        return useType;
    }

    public void setUseType(Integer useType) {
        this.useType = useType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserNames() {
        return userNames;
    }

    public void setUserNames(String userNames) {
        this.userNames = userNames;
    }

    public String getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(String roleIds) {
        this.roleIds = roleIds;
    }
}
