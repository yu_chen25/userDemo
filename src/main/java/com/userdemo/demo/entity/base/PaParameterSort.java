package com.userdemo.demo.entity.base;

public class PaParameterSort {
    private Long papaId;

    private String papaName;

    private String papaType;

    private String papaDesc;

    public PaParameterSort(Long papaId, String papaName, String papaType, String papaDesc) {
        this.papaId = papaId;
        this.papaName = papaName;
        this.papaType = papaType;
        this.papaDesc = papaDesc;
    }

    public PaParameterSort() {
        super();
    }

    public Long getPapaId() {
        return papaId;
    }

    public void setPapaId(Long papaId) {
        this.papaId = papaId;
    }

    public String getPapaName() {
        return papaName;
    }

    public void setPapaName(String papaName) {
        this.papaName = papaName == null ? null : papaName.trim();
    }

    public String getPapaType() {
        return papaType;
    }

    public void setPapaType(String papaType) {
        this.papaType = papaType == null ? null : papaType.trim();
    }

    public String getPapaDesc() {
        return papaDesc;
    }

    public void setPapaDesc(String papaDesc) {
        this.papaDesc = papaDesc == null ? null : papaDesc.trim();
    }
}