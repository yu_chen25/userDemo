package com.userdemo.demo.entity.base.vo;

import lombok.Data;

@Data
public class BaseMessageLogVO {

    /**
     * 推送给消息的用户名
     */
    private String userName;

    /**
     * 内容描述
     */
    private String messageInfo;

    /**
     * 推送用户公司ID
     */
    private Long companyId;

    /**
     *消息类型（papa_id:39,1.货物，2.服务，3.工程 4.业务）
     */
    private Integer messageType;

    /**
     * 业务类型（papa_id:40,1.购标申请，2.开票管理，3.入库管理 4.项目委托 5.合同管理）'
     */
    private Integer businessType;

    /**
     * 业务ID
     */
    private Long businessId;

    /**
     * 项目编号
     */
    private String projectNumber;

    /**
     * 是否处理 papa_id:1,1.是  2. 否 )
     */
    private Integer isHandle;

    /**
     * 消息ID
     */
    private Long messageId;

    /**
     * 结束时间
     */
    private  String endTime;

    /**
     * 业务类型中文
     */
    private  String  businessTypeName;

    /**
     * 消息时间
     */
    private String  createTime;
}
