package com.userdemo.demo.entity.base.vo;

import java.util.List;

/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public class BaseOrgMechanismVO {

    /**
     * 组织机构ID
     */
    private Long orgId;

    /**
     * 父节点ID
     */
    private Long parentId;

    /**
     * 组织机构名
     */
    private String orgName;

    /**
     * 使用类型（papa_id:21,1.平台，2.医院，3.代理机构）
     */
    private Integer useType;

    /**
     * 是否顶级（papa_id:1,1是，2否）
     */
    private  Integer  isTop;

    /**
     * 使用类型中文
     */
    private  String  useTypeName;

    /**
     * 公司ID
     */
    private  Long companyId;

    /**
     * 角色是否有权限
     */
    private  String  isPass;

    /**
     * 子节点
     */
    List<BaseOrgMechanismVO> children;

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Integer getUseType() {
        return useType;
    }

    public void setUseType(Integer useType) {
        this.useType = useType;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }

    public String getUseTypeName() {
        return useTypeName;
    }

    public void setUseTypeName(String useTypeName) {
        this.useTypeName = useTypeName;
    }

    public List<BaseOrgMechanismVO> getChildren() {
        return children;
    }

    public void setChildren(List<BaseOrgMechanismVO> children) {
        this.children = children;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getIsPass() {
        return isPass;
    }

    public void setIsPass(String isPass) {
        this.isPass = isPass;
    }
}
