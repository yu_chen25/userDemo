package com.userdemo.demo.entity.base.dto;

/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public class PaParameterSortDTO {

    /**
     * ID主键
     */
    private Long papaId;

    /**
     * 枚举名称
     */
    private String papaName;

    /**
     * 枚举类型
     */
    private String papaType;

    /**
     * 枚举描述
     */
    private String papaDesc;

    /**
     * 页码
     */
    private Integer pageNum;


    /**
     * 展示最大数
     */
    private Integer pageSize;

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Long getPapaId() {
        return papaId;
    }

    public void setPapaId(Long papaId) {
        this.papaId = papaId;
    }

    public String getPapaName() {
        return papaName;
    }

    public void setPapaName(String papaName) {
        this.papaName = papaName;
    }

    public String getPapaType() {
        return papaType;
    }

    public void setPapaType(String papaType) {
        this.papaType = papaType;
    }

    public String getPapaDesc() {
        return papaDesc;
    }

    public void setPapaDesc(String papaDesc) {
        this.papaDesc = papaDesc;
    }
}
