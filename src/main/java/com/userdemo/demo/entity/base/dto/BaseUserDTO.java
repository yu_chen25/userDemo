package com.userdemo.demo.entity.base.dto;

import lombok.Data;

/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
@Data
public class BaseUserDTO {

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户昵称
     */
    private String displayName;

    /**
     * 密码
     */
    private String password;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 组织机构ID
     */
    private Long orgId;

    /**
     * 创建人组织机构ID
     */
    private Long createOrgId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 性别
     */
    private Integer sex;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 是否在职
     */
    private Integer isIn;

    /**
     * 菜单ID集合
     */
    private String menuIds;

    /**
     * 角色编码集合
     */
    private String roleIds;

    /**
     * 页码
     */
    private Integer pageNum;

    /**
     * 展示最大数
     */
    private Integer pageSize;

    /**
     * 公司ID
     */
    private Long companyId;


    /**
     * 使用类型
     */
    private Integer useType;

    /**
     * 短信验证码
     */
    private String messgaeCode;


    /**
     * 用户名
     */
    private String userNo;

    /**
     * 公司名称
     */
    private String companyName;

}
