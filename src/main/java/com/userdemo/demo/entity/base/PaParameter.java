package com.userdemo.demo.entity.base;

public class PaParameter extends PaParameterKey {
    private String paName;

    public PaParameter(Long paraId, Long papaId, String paName) {
        super(paraId, papaId);
        this.paName = paName;
    }

    public PaParameter() {
        super();
    }

    public String getPaName() {
        return paName;
    }

    public void setPaName(String paName) {
        this.paName = paName == null ? null : paName.trim();
    }
}