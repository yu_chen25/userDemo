package com.userdemo.demo.entity.base.vo;

/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public class PaParameterSortVO {

    /**
     * ID主键
     */
    private Long papaId;

    /**
     * 枚举名称
     */
    private String papaName;

    /**
     * 枚举类型
     */
    private String papaType;

    /**
     * 枚举描述
     */
    private String papaDesc;

    public Long getPapaId() {
        return papaId;
    }

    public void setPapaId(Long papaId) {
        this.papaId = papaId;
    }

    public String getPapaName() {
        return papaName;
    }

    public void setPapaName(String papaName) {
        this.papaName = papaName;
    }

    public String getPapaType() {
        return papaType;
    }

    public void setPapaType(String papaType) {
        this.papaType = papaType;
    }

    public String getPapaDesc() {
        return papaDesc;
    }

    public void setPapaDesc(String papaDesc) {
        this.papaDesc = papaDesc;
    }
}
