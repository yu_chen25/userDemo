package com.userdemo.demo.entity.cp.vo;

import lombok.Data;

@Data
public class CpCityNameVO {

    private  String  cityName;

}
