package com.userdemo.demo.entity.cp.vo;

import lombok.Data;

@Data
public class CpCrawlerConfigurationVO {


    /**
     * 招标参数主键id
     */
    private Long cpCrawlerConfigurationId;

    /**
     * 设备id
     */
    private Long crawlerId;

    /**
     * 招标参数链接
     */
    private String crawlerLink;

}