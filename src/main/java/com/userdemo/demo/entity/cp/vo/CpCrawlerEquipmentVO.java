package com.userdemo.demo.entity.cp.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class CpCrawlerEquipmentVO {

    /**
     * 设备主键id
     */
    private Long  cpCrawlerEquipmentId;

    /**
     * 公告名称
     */
    private String announcementName;

    /**
     * 地域
     */
    private String cityName;

    /**
     * 采购人
     */
    private String purchaseName;

    /**
     * 采购时间
     */
    private String issueTime;

    /**
     * 设备名称
     */
    private String equipmentName;

    /**
     * 中标金额
     */
    private BigDecimal amount;

    /**
     * 单价
     */
    private BigDecimal price;

    /**
     * 数量
     */
    private Integer quantity;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 制造商
     */
    private String manufacturer;

    /**
     * 制造商品牌
     */
    private String manufacturerName;

    /**
     * 型号
     */
    private String model;

    /**
     * 公告链接
     */
    private String announcementLink;

    /**
     * 招标配置参数链接列表
     */
    private List<CpCrawlerConfigurationVO>   clist;

    /**
     * 文件id
     */
    private  Long fileId;

    /**
     * 类型  1 oa设备表  2爬虫设备表
     */
    private  String type;

    /**
     * 是否处理 1是2否
     */
    private Integer isRelease;

    /**
     * 处理完成时间
     */
    private  String releaseTime;

    /**
     * 最新更新人
     */
    private  String  updateUser;
}
