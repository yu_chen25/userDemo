package com.userdemo.demo.entity.cp.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CrawlerEquipmentSumVO {

    /**
     * 数量
     */
    private  Integer quantity;

    /**
     * 平均单价
     */
    private BigDecimal avgPrice;

    /**
     * 最低单价
     */
    private BigDecimal minPrice;

    /**
     * 最高单价
     */
    private BigDecimal maxPrice;

    /**
     * 最低单价时间
     */
    private String   minPriceDate;

    /**
     * 最高单价时间
     */
    private String   maxPriceDate;

}
