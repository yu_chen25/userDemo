package com.userdemo.demo.entity.cp;

public class CpCrawlerConfiguration {
    private Long id;

    private Long crawlerId;

    private String crawlerLink;

    private Long synchronousId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCrawlerId() {
        return crawlerId;
    }

    public void setCrawlerId(Long crawlerId) {
        this.crawlerId = crawlerId;
    }

    public String getCrawlerLink() {
        return crawlerLink;
    }

    public void setCrawlerLink(String crawlerLink) {
        this.crawlerLink = crawlerLink == null ? null : crawlerLink.trim();
    }

    public Long getSynchronousId() {
        return synchronousId;
    }

    public void setSynchronousId(Long synchronousId) {
        this.synchronousId = synchronousId;
    }
}