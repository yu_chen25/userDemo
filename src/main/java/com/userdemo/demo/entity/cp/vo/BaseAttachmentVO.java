package com.userdemo.demo.entity.cp.vo;

import lombok.Data;

@Data
public class BaseAttachmentVO {

    /**
     * 附件名称
     */
    private String fileName;

    /**
     * 附件下载路径
     */
    private String fileDownPath;
}
