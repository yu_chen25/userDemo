package com.userdemo.demo.entity.cp.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CpCrawlerEquipmentDTO {


    /**
     * 设备主键id
     */
    private Long  cpCrawlerEquipmentId;

    /**
     * 公告名称
     */
    private String announcementName;

    /**
     * 地域
     */
    private String cityName;

    /**
     * 采购人
     */
    private String purchaseName;

    /**
     * 采购时间
     */
    private String issueTime;

    /**
     * 设备名称
     */
    private String equipmentName;

    /**
     * 中标金额
     */
    private BigDecimal amount;

    /**
     * 单价
     */
    private BigDecimal price;

    /**
     * 数量
     */
    private Integer quantity;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 制造商
     */
    private String manufacturer;

    /**
     * 制造商品牌
     */
    private String manufacturerName;

    /**
     * 型号
     */
    private String model;

    /**
     * 公告链接
     */
    private String announcementLink;

    /**
     * 页码
     */
    private Integer pageNum;

    /**
     * 展示最大数
     */
    private Integer pageSize;


    /**
     * 数据状态
     */
    private Integer status;


    /**
     * 设备主键ids字符串集
     */
    private String  cpCrawlerEquipmentIds;

    /**
     * 排序参数 1 单价   2采购时间
     */
    private  Integer orderBy;


    /**
     * 正序 还是 倒序    1 正 2 倒
     */
    private  Integer  beforeOrAfter;

    /**
     * 文件id
     */
    private  Long fileId;


    /**
     * 最大最小单价
     */
    private BigDecimal minOrMax;


    /**
     * 关键字
     */
    private  String  keywordsName;


    /**
     * 是否处理 1是2否
     */
    private Integer isRelease;


    /**
     * 是否历史
     */
    private Integer isHistory;

    private  String  updateUser;


}
