package com.userdemo.demo.common.base;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Random;

/**
 * Created by Administrator on 2018/5/3 0003.
 */
public class Captcha {
    private ByteArrayInputStream image;
    private String str;
    private int imageW;
    private int imageH;
    private int fontSize;
    private Random random = new Random();
    private BufferedImage buffImage;
    private static final char[] randomSequence = new char[]{'0','1','2','3','4','5','6','7','8','9'};

    /**
     * 设置宽高大小
     * @param imageW
     * @param imageH
     * @param fontSize
     */
    public Captcha(int imageW, int imageH, int fontSize) {
        this.imageW = imageW;
        this.imageH = imageH;
        this.fontSize = fontSize;
        this.init();
    }

    /**
     * 获取字节码
     * @return
     */
    public ByteArrayInputStream getImage() {
        return this.image;
    }

    /**
     * 获取字符
     * @return
     */
    public String getString() {
        return this.str;
    }

    /**
     * 初始化
     */
    private void init() {
        this.buffImage = new BufferedImage(this.imageW, this.imageH, 1);
        Graphics2D g = this.buffImage.createGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, this.imageW, this.imageH);
        Font font = new Font("Courier", 1, this.fontSize);
        g.setFont(font);
        FontMetrics fm = g.getFontMetrics();
        int starX = (this.imageW - fm.stringWidth("A B C D")) / 2;
        int strW = fm.stringWidth(" A");
        String sRand = "";
        g.setColor(Color.BLUE);
        for(int input = 0; input < 4; ++input) {
            int output = this.random.nextInt(randomSequence.length - 1);
            String e = String.valueOf(randomSequence[output]);
            sRand = sRand + e;
            int strH = fm.stringWidth(e);
            int x = starX + strW * input;
            g.drawString(e, x, this.randY(strH));
        }

        this.str = sRand;
        g.dispose();
        this.buffImage = this.twistImage();
        ByteArrayInputStream var13 = null;
        ByteArrayOutputStream var14 = new ByteArrayOutputStream();

        try {
            ImageOutputStream var15 = ImageIO.createImageOutputStream(var14);
            ImageIO.write(this.buffImage, "JPEG", var15);
            var15.close();
            var13 = new ByteArrayInputStream(var14.toByteArray());
        } catch (Exception var12) {
            System.out.println("验证码图片产生出现错误：" + var12.toString());
        }

        this.image = var13;
    }

    private int randY(int strH) {
        boolean y = false;
        int count = 0;

        int var4;
        while(true) {
            var4 = this.random.nextInt(this.imageH - 5);
            if(var4 - strH > 5) {
                break;
            }

            if(count == 5) {
                var4 = strH + 6;
                break;
            }

            ++count;
        }

        return var4;
    }

    private int getXPosition4Twist(double dPhase, double dMultValue, int height, int xPosition, int yPosition) {
        double PI = 3.141592653589793D;
        double dx = PI * (double)yPosition / (double)height + dPhase;
        double dy = Math.sin(dx);
        return xPosition + (int)(dy * dMultValue);
    }

    private BufferedImage twistImage() {
        double dMultValue = 8.0D;
        double dPhase = (double)this.random.nextInt(6);
        BufferedImage destBi = new BufferedImage(this.buffImage.getWidth(), this.buffImage.getHeight(), 1);
        Graphics2D g = destBi.createGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, this.imageW, this.imageH);

        for(int i = 0; i < destBi.getWidth(); ++i) {
            for(int j = 0; j < destBi.getHeight(); ++j) {
                int nOldX = this.getXPosition4Twist(dPhase, dMultValue, destBi.getHeight(), i, j);
                if(nOldX >= 0 && nOldX < destBi.getWidth() && j >= 0 && j < destBi.getHeight()) {
                    destBi.setRGB(nOldX, j, this.buffImage.getRGB(i, j));
                }
            }
        }

        return destBi;
    }

    public int getImageW() {
        return this.imageW;
    }

    public void setImageW(int imageW) {
        this.imageW = imageW;
    }

    public int getImageH() {
        return this.imageH;
    }

    public void setImageH(int imageH) {
        this.imageH = imageH;
    }

    public BufferedImage getBuffImage() {
        return this.buffImage;
    }
    private Color getColor()
    {
        Random random = new Random();
        int red = 0, green = 0, blue = 0;
        // 产生随机的颜色分量来构造颜色值，这样输出的每位数字的颜色值都将不同。
        red = random.nextInt(255);
        green = random.nextInt(255);
        blue = random.nextInt(255);
        return new Color(red,green,blue);
    }
}
