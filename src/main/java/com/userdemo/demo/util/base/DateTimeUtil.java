package com.userdemo.demo.util.base;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by geely
 */
public class DateTimeUtil {

    //joda-time

    //str->Date
    //Date->str
    public static final String STANDARD_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String STANDARD_FORMAT2 = "yyyy-MM-dd";


    public static Date strToDate(String dateTimeStr,String formatStr){
        if(StringUtil.isEmpty(dateTimeStr)){
            return null;
        }
        try{
            DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(formatStr);
            DateTime dateTime = dateTimeFormatter.parseDateTime(dateTimeStr);
            return dateTime.toDate();
        }catch (Exception e){
            return null;
        }

    }

    public static String dateToStr(Date date,String formatStr){
        if(date == null){
            return StringUtils.EMPTY;
        }
        DateTime dateTime = new DateTime(date);
        return dateTime.toString(formatStr);
    }

    public static Date strToDate(String dateTimeStr){
        if(StringUtil.isEmpty(dateTimeStr)){
            return null;
        }
        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(STANDARD_FORMAT);
        DateTime dateTime = dateTimeFormatter.parseDateTime(dateTimeStr);
        return dateTime.toDate();
    }

   //只有年月日的
    public static Date strToDate2(String dateTimeStr){
        if(StringUtil.isEmpty(dateTimeStr)){
            return null;
        }
        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(STANDARD_FORMAT2);
        DateTime dateTime = dateTimeFormatter.parseDateTime(dateTimeStr);
        return dateTime.toDate();
    }

    public static String dateToStr(Date date){
        if(date == null){
            return StringUtils.EMPTY;
        }
        DateTime dateTime = new DateTime(date);
        return dateTime.toString(STANDARD_FORMAT);
    }

    
    public static String todayStr() {
    	return formatDateToStr(new Date(), STANDARD_FORMAT2);
    }
	public static String todayStrHMS() {
		return formatDateToStr(new Date(), STANDARD_FORMAT);
	}
	
	/**
	 * @param date
	 *            格式化的日期對像
	 * @param formatter
	 *            格式化的字符串形式
	 * @return 按照formatter指定的格式的日期字符
	 * @throws ParseException
	 *             無法解析的字符串格式
	 */
	public static String formatDateToStr(Date date, String formatter) {
		if (date == null)
			return "";
		try {
			return new SimpleDateFormat(formatter).format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	/**
	 *
	 * @return
	 */
	public static Date today() {
		return parseDate(todayStr(), STANDARD_FORMAT);
	}
    /**
     *
     * @return
     */
    public static Date todayHMS() {
		return parseDate(todayStrHMS(), STANDARD_FORMAT);
	}

    public static Date parseDate(String dateStr, String format) {
		if (dateStr==null || dateStr.trim().length()== 0 )
			return null;
		Date date = null;
		try {
			DateFormat df = new SimpleDateFormat(format);
			String dt = dateStr;// .replaceAll("-", "/");
			if ((!dt.equals("")) && (dt.length() < format.length())) {
				dt += format.substring(dt.length()).replaceAll("[YyMmDdHhSs]",
						"0");
			}
			date = (Date) df.parse(dt);
		} catch (Exception e) {
		}
		return date;
	}
    public static final long daysBetween(Date early, Date late) { 
    	 
		long i = (late.getTime()-early.getTime())/1000;
         
        return i;   
   }
    
    
    public static String getDaysBetweenHMS(Date d1,Date d2) {
    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
          long diff = d1.getTime() - d2.getTime();//这样得到的差值是微秒级别
          long days = diff / (1000 * 60 * 60 * 24);
     
          long hours = (diff-days*(1000 * 60 * 60 * 24))/(1000* 60 * 60);
          long minutes = (diff-days*(1000 * 60 * 60 * 24)-hours*(1000* 60 * 60))/(1000* 60);
          return ""+days+"天"+hours+"小时"+minutes+"分";
        }catch (Exception e)
        {
        	e.printStackTrace();
        	return "";
        }
    }
    
    
    public static void main(String[] args) {
    	
	}

     
}
