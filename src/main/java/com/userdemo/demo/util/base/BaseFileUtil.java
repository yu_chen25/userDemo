package com.userdemo.demo.util.base;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import com.userdemo.demo.constant.file.OssFileUrl;
import com.userdemo.demo.entity.base.vo.BaseFileVO;
import com.userdemo.demo.enumeration.sys.ResponseCode;
import com.xiaoleilu.hutool.http.HttpUtil;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public class BaseFileUtil {

    /**
     * 附件ID查询单个附件
     * @param fileId 附件ID
     * @return
     */
    public static BaseFileVO selectFileById(Long fileId){
        if(null == fileId ||  0 == fileId){
            return null;
        }
        //查询附件信息
        Map map = new HashMap(16);
        map.put("baseAliyunossFileId",fileId);
        String result = HttpUtil.get(OssFileUrl.URL3,map);
        if(StringUtil.isNotEmpty(result)){
            Map resultMap = (Map)JSONObject.parse(result);
            int status = NumberUtil.safeToNumber(resultMap.get("status"));
            if(ResponseCode.SUCCESS.getCode() == status){
                return JSON.parseObject(StringUtil.valueOfString(resultMap.get("data")),BaseFileVO.class);
            }
        }
        return null;
    }

    /**
     * 业务信息查询附件集合
     * @param entityName
     * @param entityType
     * @param entityId
     * @return
     */
    public static List<BaseFileVO> selectListFileByParam(String entityName,
                                                         String entityType,
                                                         Long entityId){
        if(StringUtil.isEmpty(entityName) || StringUtil.isEmpty(entityType)
                || null == entityId){
            return null;
        }
        //查询附件信息
        Map map = new HashMap(16);
        map.put("entityName",entityName);
        map.put("entityType",entityType);
        map.put("entityId",entityId);
        String result = HttpUtil.get(OssFileUrl.URL4,map);
        if(StringUtil.isNotEmpty(result)){
            Map resultMap = (Map)JSONObject.parse(result);
            int status = NumberUtil.safeToNumber(resultMap.get("status"));
            if(ResponseCode.SUCCESS.getCode() == status){
                Object obj = resultMap.get("data");
                if(null != obj){
                    return (List<BaseFileVO>) obj;
                }
            }
        }
        return null;
    }

    /**
     * 删除OSS附件
     * @param fileId 附件ID
     * @return
     */
    public static int delFileById(Long fileId){
        Map map = new HashMap(16);
        map.put("baseAliyunossFileId",fileId);
        String result = HttpUtil.post(OssFileUrl.URL2,map);
        if(StringUtil.isNotEmpty(result)){
            Map resultMap = (Map)JSONObject.parse(result);
            int status = NumberUtil.safeToNumber(resultMap.get("id"));
            if(ResponseCode.SUCCESS.getCode() == status){
                return 1;
            }
        }
        return 0;
    }

    /**
     * 批量更新附件业务信息
     * @param entityName
     * @param entityType
     * @param entityId
     * @param fileIds
     * @return
     */
    public static int updateFileBusinessData(String entityName,
                                             String entityType,
                                             Long entityId,
                                             String fileIds){
        //查询附件信息
        Map map = new HashMap(16);
        map.put("entityName",entityName);
        map.put("entityType",entityType);
        map.put("entityId",entityId);
        map.put("fileIds",fileIds);
        String result = HttpUtil.post(OssFileUrl.URL5,map);
        if(StringUtil.isNotEmpty(result)){
            Map resultMap = (Map)JSONObject.parse(result);
            int status = NumberUtil.safeToNumber(resultMap.get("status"));
            if(ResponseCode.SUCCESS.getCode() == status){
                return 1;
            }
        }
        return 0;
    }


    public static void uploadFileOss(String objectName ,File uploadFile){
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        String endpoint = OssFileUrl.ENDPOINT;
        // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
        String accessKeyId = OssFileUrl.ACCESSKEYID;
        String accessKeySecret = OssFileUrl.ACCESSKEYSECRET;

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        // 创建PutObjectRequest对象。
        PutObjectRequest putObjectRequest = new PutObjectRequest(OssFileUrl.YOURBUCKETNAME,objectName,uploadFile);

        // 上传文件。
        PutObjectResult putObjectResult = ossClient.putObject(putObjectRequest);
        String json = JSON.toJSONString(putObjectResult);
        System.out.println(json);
        // 关闭OSSClient。
        ossClient.shutdown();
    }

}
