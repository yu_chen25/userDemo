package com.userdemo.demo.dao.base;

import com.userdemo.demo.entity.base.BaseRole;
import com.userdemo.demo.entity.base.dto.BaseRoleDTO;
import com.userdemo.demo.entity.base.vo.BaseRoleVO;

import java.util.List;

public interface BaseRoleMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BaseRole record);

    int insertSelective(BaseRole record);

    BaseRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BaseRole record);

    int updateByPrimaryKey(BaseRole record);

    /**
     * 查询序列
     * @return
     */
    Long selectSeq();

    /**
     * 判断角色编码是否存在
     * @param roleCode
     * @return
     */
    int countNumByRoleCode(String roleCode);


    /**
     * 查询角色列表
     * @param baseRoleDTO
     * @return
     */
    List<BaseRoleVO> selectListRole(BaseRoleDTO baseRoleDTO);

    /**
     * ID查询角色详情
     * @param id
     * @return
     */
    BaseRoleVO selectVOById(Long id);


    /**
     * 用户查询角色id集合
     * @param userName
     * @return
     */
    List<Long> selectRoleIdByUserName(String userName);
}