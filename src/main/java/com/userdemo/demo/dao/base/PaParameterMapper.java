package com.userdemo.demo.dao.base;

import com.userdemo.demo.entity.base.PaParameter;
import com.userdemo.demo.entity.base.PaParameterKey;
import com.userdemo.demo.entity.base.vo.PaParameterVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PaParameterMapper {
    int deleteByPrimaryKey(PaParameterKey key);

    int insert(PaParameter record);

    int insertSelective(PaParameter record);

    PaParameter selectByPrimaryKey(PaParameterKey key);

    int updateByPrimaryKeySelective(PaParameter record);

    int updateByPrimaryKey(PaParameter record);

    /**
     * 根据枚举父参数ID删除
     * @param papaId
     * @return
     */
    int delByPapaId(Long papaId);

    /**
     * 查询参数详情
     * @param papaId
     * @param paraId
     * @return
     */
    PaParameter selectByPapaIdAndParaId(@Param(value = "papaId") Long papaId,
                                        @Param(value = "paraId") Long paraId);

    /**
     * 统计父参数下paraId是否重复
     * @param papaId
     * @param paraId
     * @return
     */
    int countNumByParaIdAndPapaId(@Param(value = "papaId") Long papaId,
                                  @Param(value = "paraId") Long paraId);

    /**
     * 删除父参数下枚举
     * @param papaId
     * @param paraId
     * @return
     */
    int delByParaIdAndPapaId(@Param(value = "papaId") Long papaId,
                             @Param(value = "paraId") Long paraId);

    /**
     * 父参数ID查询枚举集合
     * @param papaId
     * @return
     */
    List<PaParameterVO> selectListByPapaId(Long papaId);


    /**
     * 查询单个枚举详情
     * @param papaId
     * @param paraId
     * @return
     */
    PaParameterVO selectVoByPapaIdAndParaId(@Param(value = "papaId") Long papaId,
                                            @Param(value = "paraId") Long paraId);
}