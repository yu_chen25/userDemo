package com.userdemo.demo.dao.base;

import com.userdemo.demo.entity.base.BaseOrgMechanism;
import com.userdemo.demo.entity.base.dto.BaseOrgMechanismDTO;
import com.userdemo.demo.entity.base.vo.BaseOrgMechanismVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BaseOrgMechanismMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BaseOrgMechanism record);

    int insertSelective(BaseOrgMechanism record);

    BaseOrgMechanism selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BaseOrgMechanism record);

    int updateByPrimaryKey(BaseOrgMechanism record);

    /**
     * 查询序列
     * @return
     */
    Long selectSeq();

    /**
     * 更新数据为失效状态
     * @param orgId
     * @return
     */
    int updateStatusByOrgid(@Param(value = "orgId") Long orgId,
                            @Param(value = "userName") String userName);

    /**
     * 查询树状组织机构
     * @param mechanismDTO
     * @return
     */
    List<BaseOrgMechanismVO> selectTreeListOrg(BaseOrgMechanismDTO mechanismDTO);
}