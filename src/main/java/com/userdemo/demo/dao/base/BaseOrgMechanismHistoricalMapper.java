package com.userdemo.demo.dao.base;

import com.userdemo.demo.entity.base.BaseOrgMechanismHistorical;
import com.userdemo.demo.entity.base.vo.BaseOrgMechanismHistoricalVO;

import java.util.List;

public interface BaseOrgMechanismHistoricalMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BaseOrgMechanismHistorical record);

    int insertSelective(BaseOrgMechanismHistorical record);

    BaseOrgMechanismHistorical selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BaseOrgMechanismHistorical record);

    int updateByPrimaryKey(BaseOrgMechanismHistorical record);

    /**
     * 查询序列
     * @return
     */
    Long selectSeq();

    /**
     * 查看组织机构曾用名
     * @param orgId
     * @return
     */
    List<BaseOrgMechanismHistoricalVO> selectBaseOrgMechanismHistorical(Long orgId);
}