package com.userdemo.demo.dao.base;

import com.userdemo.demo.entity.base.BaseUserRole;
import com.userdemo.demo.entity.base.dto.BaseRoleDTO;

import java.util.List;

public interface BaseUserRoleMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BaseUserRole record);

    int insertSelective(BaseUserRole record);

    BaseUserRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BaseUserRole record);

    int updateByPrimaryKey(BaseUserRole record);

    /**
     * 查询最新ID
     * @return
     */
    Long selectSeq();

    /**
     * 删除用户角色
     * @param userName
     * @return
     */
    int delByUserName(String userName);

    /**
     * 统计角色编码数量
     * @param roleCode
     * @return
     */
    int countNumByRoleCode(String roleCode);

    /**
     * 删除角色与用户关联
     * @param baseRoleDTO
     */
    void deleteUserRole(BaseRoleDTO baseRoleDTO);

    /**
     * 用户分配角色
     * @param baseRoleDTO
     * @return
     */
    int addRoleUser(BaseRoleDTO baseRoleDTO);

    /**
     * 角色关联用户
     * @param baseRoleDTO
     * @return
     */
    int addUserRole(BaseRoleDTO baseRoleDTO);

    /**
     * 角色关联了哪些用户
     * @param roleId
     * @return
     */
    List<Long> getRoleListByUser(Long roleId);
}