package com.userdemo.demo.dao.base;

import com.userdemo.demo.entity.base.BaseMessageLog;
import com.userdemo.demo.entity.base.dto.BaseMessageLogDTO;
import com.userdemo.demo.entity.base.vo.BaseMessageLogVO;

import java.util.List;

public interface BaseMessageLogMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BaseMessageLog record);

    int insertSelective(BaseMessageLog record);

    BaseMessageLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BaseMessageLog record);

    int updateByPrimaryKey(BaseMessageLog record);

    /**
     * 查最大ID值
     * @return
     */
    long selectSeq();

    /**
     * 消息列表
     * @param baseMessageLogDTO
     * @return
     */
    List<BaseMessageLogVO> selectMassageList(BaseMessageLogDTO baseMessageLogDTO);

    /**
     * 统计未处理消息数量
     * @param baseMessageLogDTO
     * @return
     */
    int countMassageList(BaseMessageLogDTO baseMessageLogDTO);

    /**
     * 处理消息
     * @param baseMessageLogDTO
     * @return
     */
    int updateMassage(BaseMessageLogDTO baseMessageLogDTO);
}