package com.userdemo.demo.dao.base;

import com.userdemo.demo.entity.base.BaseRoleMechanism;
import com.userdemo.demo.entity.base.dto.BaseOrgMechanismDTO;
import com.userdemo.demo.entity.base.dto.BaseRoleDTO;

import java.util.List;

public interface BaseRoleMechanismMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BaseRoleMechanism record);

    int insertSelective(BaseRoleMechanism record);

    BaseRoleMechanism selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BaseRoleMechanism record);

    int updateByPrimaryKey(BaseRoleMechanism record);

    /**
     * 删除角色关联
     * @param baseRoleDTO
     * @return
     */
    int deleteRoleMechanism(BaseRoleDTO baseRoleDTO);

    /**
     * 批量插入角色权限
     * @param baseRoleDTO
     * @return
     */
    int addRoleOrg(BaseRoleDTO baseRoleDTO);

    /**
     * 角色ID集合查询组织机构ID集合（用于数据权限验证）
     * @param list
     * @return
     */
    List<Long> selectOrgIdListByRoleIds(List<Long> list);

    /**
     * 该角色是否有该组织权限
     * @param mechanismDTO1
     * @return
     */
    int countIsPass(BaseOrgMechanismDTO mechanismDTO1);

    /**
     * 查询单个角色的权限
     * @param roleId
     * @return
     */
    List<Long> getOrgIdListByRoleId(Long roleId);
}