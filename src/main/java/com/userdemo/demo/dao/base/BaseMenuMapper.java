package com.userdemo.demo.dao.base;

import com.userdemo.demo.entity.base.BaseMenu;
import com.userdemo.demo.entity.base.dto.BaseMenuDTO;
import com.userdemo.demo.entity.base.vo.BaseMenuVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BaseMenuMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BaseMenu record);

    int insertSelective(BaseMenu record);

    BaseMenu selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BaseMenu record);

    int updateByPrimaryKey(BaseMenu record);


    /**
     * 查询序列
     * @return
     */
    Long selectSeq();

    /**
     * 更新菜单为失效状态
     * @param id
     * @return
     */
    int updateMenuStatus(@Param(value = "id") Long id,
                         @Param(value = "userName") String userName);

    /**
     * 条件查询菜单
     * @param baseMenuDTO
     * @return
     */
    List<BaseMenuVO> selectListByParam(BaseMenuDTO baseMenuDTO);

    /**
     * 查询用户菜单
     * @param userName
     * @return
     */
    List<BaseMenuVO> selectByUserNameAndParentId(@Param(value = "userName") String userName,
                                                 @Param(value = "parentId") Long parentId,
                                                 @Param(value = "useType") String useType);

    /**
     * 查询供应商菜单
     * @return
     */
    List<BaseMenuVO> selectSuppliersMenu(@Param(value = "parentId") Long parentId,
                                         @Param(value = "useType") String useType);

    /**
     * 查询待审核供应商菜单
     * @return
     */
    List<BaseMenuVO> selectAuditedSuppliersMenu();

    /**
     * 父节点ID查询菜单
     * @param parentId
     * @return
     */
    List<BaseMenuVO> selectListByParentId(@Param(value = "parentId") Long parentId,
                                          @Param(value = "useType") String useType);

    /**
     * 父节点ID及用户名查询菜单
     * @param parentId
     * @return
     */
    List<BaseMenuVO> selectListByParentIdAndUserName(@Param(value = "parentId") Long parentId,
                                                     @Param(value = "userName") String userName,
                                                     @Param(value = "useType") String useType);


    /**
     * Id查询菜单内容详情
     * @param id
     * @return
     */
    BaseMenuVO selectById(@Param(value = "id") Long id,
                          @Param(value = "useType") String useType);


    /**
     * 判断用户企业信息是否通过
     * @param userName
     * @return
     */
    int selectCountCompanyByUserName(String userName);

}