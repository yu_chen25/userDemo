package com.userdemo.demo.dao.base;

import com.userdemo.demo.entity.base.BaseMessageIp;

public interface BaseMessageIpMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BaseMessageIp record);

    int insertSelective(BaseMessageIp record);

    BaseMessageIp selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BaseMessageIp record);

    int updateByPrimaryKey(BaseMessageIp record);

    /**
     * ip统计发送短信次数
     * @param ip
     * @return
     */
    int countNumByIp(String ip);
}