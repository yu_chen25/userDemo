package com.userdemo.demo.dao.base;

import com.userdemo.demo.entity.base.BaseLog;
import com.userdemo.demo.entity.base.dto.BaseLogDTO;
import com.userdemo.demo.entity.base.vo.BaseLogVO;

import java.util.List;

public interface BaseLogMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BaseLog record);

    int insertSelective(BaseLog record);

    BaseLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BaseLog record);

    int updateByPrimaryKey(BaseLog record);

    /**
     * 查询日志列表
     * @param baseLogDTO
     * @return
     */
    List<BaseLogVO> selectBaseLogList(BaseLogDTO baseLogDTO);

    /**
     * 查询内容ID和参数ID
     * @param list
     * @return
     */
    List<BaseLogVO> selectParamIdList(List<Long> list);


    /**
     * 批量删除日志记录
     * @param list
     * @return
     */
    int delByList(List<Long> list);
}