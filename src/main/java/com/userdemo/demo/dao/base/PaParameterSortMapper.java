package com.userdemo.demo.dao.base;

import com.userdemo.demo.entity.base.PaParameterSort;
import com.userdemo.demo.entity.base.dto.PaParameterSortDTO;
import com.userdemo.demo.entity.base.vo.PaParameterSortVO;

import java.util.List;

public interface PaParameterSortMapper {
    int deleteByPrimaryKey(Long papaId);

    int insert(PaParameterSort record);

    int insertSelective(PaParameterSort record);

    PaParameterSort selectByPrimaryKey(Long papaId);

    int updateByPrimaryKeySelective(PaParameterSort record);

    int updateByPrimaryKey(PaParameterSort record);

    /**
     * 判断papaId是否存在
     * @param papaId
     * @return
     */
    int countNumByPapaId(Long papaId);

    /**
     * 查询枚举类型列表
     * @param paParameterSortDTO
     * @return
     */
    List<PaParameterSortVO> selectListPaParameterSort(PaParameterSortDTO paParameterSortDTO);


    /**
     * 查询单个枚举类型详情
     * @param papaId
     * @return
     */
    PaParameterSortVO selectParameterSortByPapaId(Long papaId);
}