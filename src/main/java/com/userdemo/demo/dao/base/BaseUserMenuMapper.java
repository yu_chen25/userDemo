package com.userdemo.demo.dao.base;

import com.userdemo.demo.entity.base.BaseUserMenu;

public interface BaseUserMenuMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BaseUserMenu record);

    int insertSelective(BaseUserMenu record);

    BaseUserMenu selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BaseUserMenu record);

    int updateByPrimaryKey(BaseUserMenu record);

    /**
     * 删除用户菜单
     * @param userName
     * @return
     */
    int delByUserName(String userName);
}