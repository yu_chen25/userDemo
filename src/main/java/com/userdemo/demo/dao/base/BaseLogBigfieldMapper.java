package com.userdemo.demo.dao.base;

import com.userdemo.demo.entity.base.BaseLogBigfield;

import java.util.List;

public interface BaseLogBigfieldMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BaseLogBigfield record);

    int insertSelective(BaseLogBigfield record);

    BaseLogBigfield selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BaseLogBigfield record);

    int updateByPrimaryKeyWithBLOBs(BaseLogBigfield record);

    int updateByPrimaryKey(BaseLogBigfield record);

    /**
     * 查询序列号
     * @return
     */
    Long selectSeq();

    /**
     * 批量删除数据
     * @param list
     * @return
     */
    int delBaseLogBigfieldByList(List<Long> list);
}