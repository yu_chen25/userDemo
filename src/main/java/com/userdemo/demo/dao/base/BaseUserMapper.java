package com.userdemo.demo.dao.base;

import com.userdemo.demo.entity.base.BaseUser;
import com.userdemo.demo.entity.base.dto.BaseUserDTO;
import com.userdemo.demo.entity.base.vo.BaseUserVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface BaseUserMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BaseUser record);

    int insertSelective(BaseUser record);

    BaseUser selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BaseUser record);

    int updateByPrimaryKey(BaseUser record);


    /**
     * 查询最新序列号
     * @return
     */
    Long selectSeq();

    /**
     * 判断用户名是否存在（需在职并且是生效状态）
     * @param userName
     * @return
     */
    int countNumByUserName(String userName);

    /**
     * 判断手机号是否存在
     * @param phone
     * @return
     */
    int countNumByPhone(String phone);

    /**
     * 用户登录
     * @param userName
     * @param password
     * @return
     */
    BaseUserVO selectLogin(@Param(value = "userName") String userName,
                           @Param(value = "password") String password);

    /**
     * 更新用户信息状态
     * @param id
     * @return
     */
    int updateUserStatusById(@Param(value = "id") Long id,
                             @Param(value = "companyId") Long companyId);


    /**
     * 查询用户列表
     * @param baseUserDTO
     * @return
     */
    List<BaseUserVO> selectListUser(BaseUserDTO baseUserDTO);


    /**
     * 查询用户信息（供编辑）
     * @param id
     * @return
     */
    BaseUserVO selectUpdateUserInfoById(@Param(value = "id") Long id,
                                        @Param(value = "companyId") Long companyId);

    /**
     * 查询公司用户
     * @param companyId
     * @return
     */
    List<BaseUserVO> selectProjectLeader(Long companyId);


    /**
     * 修改用户密码
     * @param baseUserDTO
     * @return
     */
    int enterpriseUserUpdateTow(BaseUserDTO baseUserDTO);

    /**
     * 用户名查询用户基本信息
     * @param userName
     * @return
     */
    BaseUserVO selectVOByUserName(String userName);

    /**
     * 修改密码
     * @param password
     * @param userName
     * @return
     */
    int updatePassword(@Param(value = "password") String password,
                       @Param(value = "userName") String userName);


    /**
     * 更新用户手机号
     * @param userName
     * @return
     */
    int updatePhoneByUserName(@Param(value = "userName") String userName,
                              @Param(value = "phone") String phone);

    /**
     * 更新用户在线状态
     * @param userName
     * @param onlineStatus
     * @return
     */
    int updateOnlineStatus(@Param(value = "userName") String userName,
                           @Param(value = "onlineStatus") Integer onlineStatus);

    /**
     * 统计用户在线数量
     * @param list
     * @return
     */
    int countOnlineNumByUserList(List<String> list);

    /**
     * 查询供应商列表
     * @return
     */
    List<BaseUserVO> selectSuppliersList(BaseUserDTO baseUserDTO);


    /**
     * 查询公司联系数据
     * @param companyName
     * @return
     */
    Map selectConcatData(String companyName);

    /**
     * 同步密码
     * @param userNo
     * @param password
     * @return
     */
    int updateUserPassword(@Param("userNo") String userNo, @Param("password") String password);
}