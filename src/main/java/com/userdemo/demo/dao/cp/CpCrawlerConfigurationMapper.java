package com.userdemo.demo.dao.cp;

import com.userdemo.demo.entity.cp.CpCrawlerConfiguration;
import com.userdemo.demo.entity.cp.vo.CpCrawlerConfigurationVO;
import com.userdemo.demo.entity.cp.vo.CpCrawlerEquipmentVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CpCrawlerConfigurationMapper {
    int deleteByPrimaryKey(Long id);

    int insert(CpCrawlerConfiguration record);

    int insertSelective(CpCrawlerConfiguration record);

    CpCrawlerConfiguration selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(CpCrawlerConfiguration record);

    int updateByPrimaryKey(CpCrawlerConfiguration record);

    /**
     * 查询最新序列号
     * @return
     */
    Long selectSeq();

    /**
     *查找招标设备配置
     * @param num_id
     * @return
     */
    List<Map> getCrawlerConfiguration(@Param("numId") String num_id);

    /**
     * 招标参数列表
     * @param cpCrawlerEquipmentId
     * @return
     */
    List<CpCrawlerConfigurationVO> selectCrawlerConfigurationList(Long cpCrawlerEquipmentId);

    /**
     * 彻底删除配置文件
     * @param id
     * @return
     */
    int deleteCrawlerConfiguration(@Param("cpCrawlerEquipmentId") String id);

    /**
     * 查看是否有附件
     * @param cpCrawlerEquipmentId
     * @return
     */
    Long countCpCrawlerConfiguration(Long cpCrawlerEquipmentId);

    /**
     * 校验是否存在招标文件附件
     * @param synchronousId
     * @return
     */
    CpCrawlerConfiguration  countConfiguration(Long synchronousId);
}