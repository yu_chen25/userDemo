package com.userdemo.demo.dao.cp;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 　　* @description: TODO
 * 　　* @param
 * 　　* @return
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $ $
 *
 */
public interface SynEsDataMapper {

    /**
     * 查询oa录入数据
     * @param maxId
     * @param fileIdIsNull
     * @return
     */
    List<Map> selectAnnouncementEquipment(@Param(value = "maxId") Long maxId,
                                          @Param(value = "minId") Long minId,
                                          @Param(value = "fileIdIsNull") Integer fileIdIsNull);


    /**
     * 查询oa录入数据当前最大ID
     * @return
     */
    Long selectAnnouncementEquipmentMaxId();


    /**
     * 查询爬虫数据
     * @param maxId
     * @return
     */
    List<Map> selectCrawlerEquipment(@Param(value = "maxId") Long maxId,
                                     @Param(value = "minId") Long minId);


    /**
     * 查询爬虫数据最大ID
     * @return
     */
    Long selectCrawlerEquipmentMaxId();


    /**
     * 查询OA中标设备表词汇
     * @param maxId
     * @return
     */
    List<Map> selectAnnouncementEquipmentSuggest(@Param(value = "maxId") Long maxId,
                                                 @Param(value = "minId") Long minId);


    /**
     * 查询爬虫设备表词汇
     * @param maxId
     * @return
     */
    List<Map> selectCrawlerEquipmentSuggest(@Param(value = "maxId") Long maxId,
                                            @Param(value = "minId") Long minId);

}
