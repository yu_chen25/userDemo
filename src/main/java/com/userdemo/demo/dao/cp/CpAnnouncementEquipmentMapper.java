package com.userdemo.demo.dao.cp;

import com.userdemo.demo.entity.cp.CpAnnouncementEquipment;
import org.apache.ibatis.annotations.Param;

public interface CpAnnouncementEquipmentMapper {
    int deleteByPrimaryKey(Long id);

    int insert(CpAnnouncementEquipment record);

    int insertSelective(CpAnnouncementEquipment record);

    CpAnnouncementEquipment selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(CpAnnouncementEquipment record);

    int updateByPrimaryKey(CpAnnouncementEquipment record);

    /**
     * 是否存在这个设备id
     * @param equipmentId
     * @return
     */
    int checkEquipment(@Param("equipmentId") String equipmentId);
}