package com.userdemo.demo.dao.cp;

import com.userdemo.demo.entity.cp.CpCrawlerEquipment;
import com.userdemo.demo.entity.cp.dto.CpCrawlerEquipmentDTO;
import com.userdemo.demo.entity.cp.vo.CpCityNameVO;
import com.userdemo.demo.entity.cp.vo.CpCrawlerEquipmentVO;
import com.userdemo.demo.entity.cp.vo.CrawlerEquipmentSumVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CpCrawlerEquipmentMapper {
    int deleteByPrimaryKey(Long id);

    int insert(CpCrawlerEquipment record);

    int insertSelective(CpCrawlerEquipment record);

    CpCrawlerEquipment selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(CpCrawlerEquipment record);

    int updateByPrimaryKey(CpCrawlerEquipment record);

    /**
     * 查询最新序列号
     * @return
     */
    Long selectSeq();

    /**
     * 获取最大同步id值
     * @return
     */
    Long getMaxSynchronousId();

    /**
     * 获取爬虫设备数据
     * @param maxCrawlerEquipment
     * @return
     */
    List<Map> getProvinceEquipment(Long maxCrawlerEquipment);


    /**
     * 查询爬虫分页列表
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    List<CpCrawlerEquipmentVO> selectCrawlerEquipmentList(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO);


    /**
     * 查看爬虫单个数据
     * @param cpCrawlerEquipmentId
     * @return
     */
    CpCrawlerEquipmentVO selectCrawlerEquipmentById(Long cpCrawlerEquipmentId);

    /**
     * 前端查询接口
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    List<CpCrawlerEquipmentVO> getCrawlerEquipmentBeforelist(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO);

    /**
     *查看数据
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    CrawlerEquipmentSumVO getCrawlerEquipmentSum(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO);


    /**
     * 查时间
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    String getMinPriceDate(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO);

    /**
     * 获取省市数据
     * @return
     */
    List<CpCityNameVO> getCpCityNameVO(@Param("isRelease") Integer isRelease);

    /**
     * 同步型号缩写
     * @return
     */
    List<CpCrawlerEquipmentVO> mpdelAdd();

    /**
     * 获取制造商名称
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    List<CpCrawlerEquipmentVO> getManufacturerName(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO);

    /**
     * 查最近更新的数据
     * @return
     */
    List<Map> getUpdateProvinceEquipment();

    /**
     * 更新设备
     * @param cpCrawlerEquipment
     * @return
     */
    int updateSynchronousEquipment(CpCrawlerEquipment cpCrawlerEquipment);

    /**
     * 重置爬虫表
     * @param synchronousId
     * @return
     */
    int updateProvinceEquipment(Long synchronousId);

    /**
     * 通过同步ID找信息
     * @param numId
     * @return
     */
    CpCrawlerEquipmentVO getSysCpCrawlerEquipmentVO(String numId);
}