package com.userdemo.demo.controller.base;

import com.userdemo.demo.base.BaseController;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.entity.base.dto.BaseOrgMechanismDTO;
import com.userdemo.demo.entity.base.vo.BaseOrgMechanismHistoricalVO;
import com.userdemo.demo.entity.base.vo.BaseOrgMechanismVO;
import com.userdemo.demo.entity.base.vo.BaseUserVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 　　* @description: 组织机构控制层
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
@RestController
@RequestMapping("/org/")
public class BaseOrgMechanismController extends BaseController {

    /**
     * 新增/更新组织机构
     * @param mechanismDTO
     * @param request
     * @return
     */
    @PostMapping("addOrUpdateOrgMechanism.do")
    public ServerResponse<String> addOrUpdateOrgMechanism(BaseOrgMechanismDTO mechanismDTO, HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseOrgMechanismService.addOrUpdateOrgMechanism(mechanismDTO,baseUserVO);
    }

    /**
     * Id删除组织机构
     * @param mechanismDTO
     * @param request
     * @return
     */
    @PostMapping("delOrgMechanismById.do")
    public ServerResponse<String> delOrgMechanismById(BaseOrgMechanismDTO mechanismDTO, HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseOrgMechanismService.delOrgMechanismById(mechanismDTO,baseUserVO);
    }

    /**
     * 查询树状组织机构
     * @param mechanismDTO
     * @param request
     * @return
     */
    @GetMapping("selectTreeBaseOrgMechanism.do")
    public ServerResponse<List<BaseOrgMechanismVO>> selectTreeBaseOrgMechanism(BaseOrgMechanismDTO mechanismDTO, HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseOrgMechanismService.selectTreeBaseOrgMechanism(mechanismDTO,baseUserVO);
    }


    /**
     * 查询组织机构历史名
     * @param mechanismDTO
     * @param request
     * @return
     */
    @GetMapping("selectBaseOrgMechanismHistorical.do")
    public ServerResponse<List<BaseOrgMechanismHistoricalVO>> selectBaseOrgMechanismHistorical(BaseOrgMechanismDTO mechanismDTO, HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseOrgMechanismService.selectBaseOrgMechanismHistorical(mechanismDTO,baseUserVO);
    }


}
