package com.userdemo.demo.controller.base;

import com.github.pagehelper.PageInfo;
import com.userdemo.demo.base.BaseController;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.entity.base.dto.PaParameterDTO;
import com.userdemo.demo.entity.base.dto.PaParameterSortDTO;
import com.userdemo.demo.entity.base.vo.BaseUserVO;
import com.userdemo.demo.entity.base.vo.PaParameterSortVO;
import com.userdemo.demo.entity.base.vo.PaParameterVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 　　* @description: 枚举控制层
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
@RestController
@RequestMapping("/papa/")
public class BasePaParameterContrller extends BaseController {

    /**
     * 新增枚举类型
     * @param paParameterSortDTO
     * @param request
     * @return
     */
    @PostMapping(value = "addOrUpdateParameterSort.do")
    public ServerResponse<String> addOrUpdateParameterSort(PaParameterSortDTO paParameterSortDTO, HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iPaParameterService.addOrUpdateParameterSort(paParameterSortDTO,baseUserVO);
    }

    /**
     * 删除枚举类型
     * @param paParameterSortDTO
     * @param request
     * @return
     */
    @PostMapping(value = "delParameterSortByPapaId.do")
    public ServerResponse<String> delParameterSortByPapaId(PaParameterSortDTO paParameterSortDTO, HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iPaParameterService.delParameterSortByPapaId(paParameterSortDTO,baseUserVO);
    }

    /**
     * 查询枚举类型列表
     * @param paParameterSortDTO
     * @param request
     * @return
     */
    @GetMapping(value = "selectListPaParameterSort.do")
    public ServerResponse<PageInfo> selectListPaParameterSort(PaParameterSortDTO paParameterSortDTO, HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iPaParameterService.selectListPaParameterSort(paParameterSortDTO,baseUserVO);
    }

    /**
     * 查询单个枚举类型
     * @param paParameterSortDTO
     * @param request
     * @return
     */
    @GetMapping(value = "selectParameterSortByPapaId.do")
    public ServerResponse<PaParameterSortVO> selectParameterSortByPapaId(PaParameterSortDTO paParameterSortDTO, HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iPaParameterService.selectParameterSortByPapaId(paParameterSortDTO,baseUserVO);
    }

    /**
     * 新增/更新枚举参数
     * @param paParameterDTO
     * @param request
     * @return
     */
    @PostMapping(value = "addOrUpdateParameter.do")
    public ServerResponse<String> addOrUpdateParameter(PaParameterDTO paParameterDTO, HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iPaParameterService.addOrUpdateParameter(paParameterDTO,baseUserVO);
    }

    /**
     * 删除枚举参数
     * @param paParameterDTO
     * @param request
     * @return
     */
    @PostMapping(value = "delParameterByParaId.do")
    public ServerResponse<String> delParameterByParaId(PaParameterDTO paParameterDTO, HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iPaParameterService.delParameterByParaId(paParameterDTO,baseUserVO);
    }

    /**
     * 查询枚举参数根据枚举类型ID
     * @param paParameterDTO
     * @param request
     * @return
     */
    @GetMapping(value = "selectListPaParameterByPapaId.do")
    public ServerResponse<List<PaParameterVO>> selectListPaParameterByPapaId(PaParameterDTO paParameterDTO, HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iPaParameterService.selectListPaParameterByPapaId(paParameterDTO,baseUserVO);
    }

    /**
     * 查询单个枚举参数详情
     * @param paParameterDTO
     * @param request
     * @return
     */
    @GetMapping(value = "selectPaParameterByParaId.do")
    public ServerResponse<PaParameterVO> selectPaParameterByParaId(PaParameterDTO paParameterDTO, HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iPaParameterService.selectPaParameterByParaId(paParameterDTO,baseUserVO);
    }


}
