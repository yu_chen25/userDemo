package com.userdemo.demo.controller.base;

import com.github.pagehelper.PageInfo;
import com.userdemo.demo.base.BaseController;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.entity.base.dto.BaseLogDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 　　* @description: 系统日志控制层
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
@RestController
@RequestMapping("/log/")
public class BaseLogController extends BaseController {

    /**
     * 查询系统日志记录
     * @param baseLogDTO
     * @param request
     * @return
     */
    @GetMapping("selectBaseLogList.do")
    public ServerResponse<PageInfo> selectBaseLogList(BaseLogDTO baseLogDTO, HttpServletRequest request) {
        return iBaseLogService.selectBaseLogList(baseLogDTO,iBaseUserService.getRequestUser(request));
    }

    /**
     * 批量删除日志记录
     * @param baseLogDTO
     * @param request
     * @return
     */
    @PostMapping("delBaseLogByIdList.do")
    public ServerResponse<String> delBaseLogByIdList(BaseLogDTO baseLogDTO, HttpServletRequest request) {
        return iBaseLogService.delBaseLogByIdList(baseLogDTO,iBaseUserService.getRequestUser(request));
    }

}
