package com.userdemo.demo.controller.base;

import com.github.pagehelper.PageInfo;
import com.userdemo.demo.base.BaseController;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.entity.base.dto.BaseRoleDTO;
import com.userdemo.demo.entity.base.vo.BaseRoleVO;
import com.userdemo.demo.entity.base.vo.BaseUserVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 　　* @description: 角色控制层
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
@RestController
@RequestMapping("/role/")
public class BaseRoleController extends BaseController {

    /**
     * 新增/更新角色信息
     * @param baseRoleDTO
     * @param request
     * @return
     */
    @PostMapping(value = "addOrUpdateRole.do")
    public ServerResponse<String> addOrUpdateRole(BaseRoleDTO baseRoleDTO,
                                                  HttpServletRequest request) {
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseRoleService.addOrUpdateRole(baseRoleDTO,baseUserVO);
    }

    /**
     * ID删除用户角色
     * @param baseRoleDTO
     * @param request
     * @return
     */
    @PostMapping(value = "delRoleById.do")
    public ServerResponse<String> delRoleById(BaseRoleDTO baseRoleDTO,
                                                  HttpServletRequest request) {
        return iBaseRoleService.delRoleById(baseRoleDTO);
    }

    /**
     * 列表查询角色
     * @param baseRoleDTO
     * @param request
     * @return
     */
    @GetMapping(value = "selectListRole.do")
    public ServerResponse<PageInfo> selectListRole(BaseRoleDTO baseRoleDTO,
                                                   HttpServletRequest request) {
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseRoleService.selectListRole(baseRoleDTO,baseUserVO);
    }

    /**
     * 查询角色列表
     * @param baseRoleDTO
     * @param request
     * @return
     */
    @GetMapping(value = "selectAllRole.do")
    public ServerResponse<List<BaseRoleVO>> selectAllRole(BaseRoleDTO baseRoleDTO,
                                                          HttpServletRequest request) {
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseRoleService.selectAllRole(baseRoleDTO,baseUserVO);
    }

    /**
     * ID查询角色信息
     * @param baseRoleDTO
     * @param request
     * @return
     */
    @GetMapping(value = "selectRoleById.do")
    public ServerResponse<BaseRoleVO> selectRoleById(BaseRoleDTO baseRoleDTO,
                                                     HttpServletRequest request) {
        return iBaseRoleService.selectRoleById(baseRoleDTO);
    }


    /**
     * 启用禁用接口
     * @param baseRoleDTO
     * @param request
     * @return
     */
    @PostMapping(value = "updateIsPublish.do")
    public ServerResponse<String> updateIsPublish(BaseRoleDTO baseRoleDTO,
                                              HttpServletRequest request) {

        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseRoleService.updateIsPublish(baseRoleDTO,baseUserVO);
    }


    /**
     * 角色设置数据权限
     * @param baseRoleDTO
     * @param request
     * @return
     */
    @PostMapping(value = "addRoleOrg.do")
    public ServerResponse<String> addRoleOrg(BaseRoleDTO baseRoleDTO,
                                                  HttpServletRequest request) {

        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseRoleService.addRoleOrg(baseRoleDTO,baseUserVO);
    }



    /**
     * 用户分配角色
     * @param baseRoleDTO
     * @param request
     * @return
     */
    @PostMapping(value = "addRoleUser.do")
    public ServerResponse<String> addRoleUser(BaseRoleDTO baseRoleDTO,
                                             HttpServletRequest request) {

        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseRoleService.addRoleUser(baseRoleDTO,baseUserVO);
    }


    /**
     * 角色关联用户
     * @param baseRoleDTO
     * @param request
     * @return
     */
    @PostMapping(value = "addUserRole.do")
    public ServerResponse<String> addUserRole(BaseRoleDTO baseRoleDTO,
                                              HttpServletRequest request) {

        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseRoleService.addUserRole(baseRoleDTO,baseUserVO);
    }



}
