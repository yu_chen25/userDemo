package com.userdemo.demo.controller.base;

import com.userdemo.demo.base.BaseController;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.entity.base.dto.BaseMenuDTO;
import com.userdemo.demo.entity.base.vo.BaseMenuVO;
import com.userdemo.demo.entity.base.vo.BaseUserVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 　　* @description: 菜单控制层
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
@RestController
@RequestMapping("/menu/")
public class BaseMenuController extends BaseController {

    /**
     * 新增/更新菜单
     * @param baseMenuDTO
     * @param request
     * @return
     */
    @PostMapping(value = "addOrUpdateMenu.do")
    public ServerResponse<String> addOrUpdateMenu(BaseMenuDTO baseMenuDTO,
                                                  HttpServletRequest request) {
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseMenuService.addOrUpdateMenu(baseMenuDTO,baseUserVO);
    }

    /**
     * 删除菜单
     * @param baseMenuDTO
     * @param request
     * @return
     */
    @PostMapping(value = "delById.do")
    public ServerResponse<String> delById(BaseMenuDTO baseMenuDTO,
                                                  HttpServletRequest request) {
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseMenuService.delById(baseMenuDTO,baseUserVO);
    }

    /**
     * 列表查询菜单
     * @param baseMenuDTO
     * @param request
     * @return
     */
    @GetMapping(value = "selectListMenu.do")
    public ServerResponse<List<BaseMenuVO>> selectListMenu(BaseMenuDTO baseMenuDTO,
                                                   HttpServletRequest request) {
        return iBaseMenuService.selectListMenu(baseMenuDTO,iBaseUserService.getRequestUser(request));
    }

    /**
     * 查询树状用户菜单
     * @param request
     * @return
     */
    @GetMapping(value = "selectTreeMenuByUserName.do")
    public ServerResponse<List<BaseMenuVO>> selectTreeMenuByUserName(HttpServletRequest request) {
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseMenuService.selectTreeMenuByUserName(baseUserVO);
    }

    /**
     * 查询树形所有菜单
     * @param request
     * @return
     */
    @GetMapping(value = "selectTreeMenu.do")
    public ServerResponse<List<BaseMenuVO>> selectTreeMenu(HttpServletRequest request) {
        return iBaseMenuService.selectTreeMenu(iBaseUserService.getRequestUser(request));
    }

    /**
     * 查询单个菜单详情
     * @param request
     * @return
     */
    @GetMapping(value = "selectMenuById.do")
    public ServerResponse<BaseMenuVO> selectMenuById(BaseMenuDTO baseMenuDTO,HttpServletRequest request) {
        return iBaseMenuService.selectMenuById(baseMenuDTO,iBaseUserService.getRequestUser(request));
    }

}
