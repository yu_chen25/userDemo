package com.userdemo.demo.controller.base;

import com.github.pagehelper.PageInfo;
import com.userdemo.demo.base.BaseController;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.constant.base.Const;
import com.userdemo.demo.entity.base.dto.BaseUserDTO;
import com.userdemo.demo.entity.base.vo.BaseUserVO;
import com.userdemo.demo.util.base.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
@RestController
@RequestMapping("/user/")
public class BaseUserController extends BaseController {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 登录
     * @param request
     * @return
     */
    @PostMapping(value = "login.do")
    public ServerResponse<BaseUserVO> login(@RequestParam(value = "userName") String userName,
                                            @RequestParam(value = "password") String password,
                                            HttpServletRequest request){

        //判断用户名是否登录
        String loginToken = request.getParameter(Const.LOGINTOKEN);
        //这里的User是登陆时放入session的
        if(StringUtil.isNotEmpty(loginToken)){
            Object o = redisTemplate.opsForValue().get(Const.LOGINTOKEN);
            if( o!= null){
                BaseUserVO u = (BaseUserVO)o;
                if(null != u){
                    if(userName.equals(u.getUserName())){
                        return ServerResponse.createBySuccessMessage("用户已登录");
                    }
                }
            }
        }
        ServerResponse<BaseUserVO> response = iBaseUserService.login(userName,password);
        if(response.isSuccess()){
            //生成UUID
            String uuidToken = UUID.randomUUID().toString();
            uuidToken = uuidToken.replaceAll("-","");
            response.getData().setLoginToken(uuidToken);
            //建立token和用户登录态的联系
            redisTemplate.opsForValue().set(uuidToken,response.getData());
            //缓存一小时
            redisTemplate.expire(uuidToken,Const.CACHETIME,TimeUnit.HOURS);
        }
        return response;
    }

    /**
     * 新增/更新用户
     * @param baseUserDTO
     * @param request
     * @return
     */
    @PostMapping("addOrUpdateUser.do")
    public ServerResponse<String> addOrUpdateUser(BaseUserDTO baseUserDTO, HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseUserService.addOrUpdateUser(baseUserDTO,baseUserVO);
    }

    /**
     * 根据ID删除用户
     * @param baseUserDTO
     * @param request
     * @return
     */
    @PostMapping("delUserById.do")
    public ServerResponse<String> delUserById(BaseUserDTO baseUserDTO,HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseUserService.delUserById(baseUserDTO,baseUserVO);
    }

    /**
     * 查询用户列表
     * @param baseUserDTO
     * @param request
     * @return
     */
    @GetMapping("selectListUser.do")
    public ServerResponse<PageInfo> selectListUser(BaseUserDTO baseUserDTO, HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseUserService.selectListUser(baseUserDTO,baseUserVO);
    }

    /**
     * 查询单个用户详情
     * @param baseUserDTO
     * @param request
     * @return
     */
    @GetMapping("selectUpdateUserInfoById.do")
    public ServerResponse<BaseUserVO> selectUpdateUserInfoById(BaseUserDTO baseUserDTO, HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseUserService.selectUpdateUserInfoById(baseUserDTO,baseUserVO);
    }

    /**
     * 查询项目负责人接口
     * @param baseUserDTO
     * @param request
     * @return
     */
    @GetMapping("selectProjectLeader.do")
    public ServerResponse<PageInfo> selectProjectLeader(BaseUserDTO baseUserDTO, HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iBaseUserService.selectProjectLeader(baseUserDTO,baseUserVO);
    }

    /**
     * 用户名注册
     * @param baseUserDTO
     * @param request
     * @return
     */
    @PostMapping("userRegistration.do")
    public ServerResponse<String> userRegistration(BaseUserDTO baseUserDTO, HttpServletRequest request){
        return iBaseUserService.userRegistration(baseUserDTO);
    }

    /**
     * 查询登录用户详情
     * @param request
     * @return
     */
    @GetMapping("selectLoginUser.do")
    public ServerResponse<BaseUserVO> selectLoginUser(HttpServletRequest request){
        return ServerResponse.createBySuccess("查询成功",iBaseUserService.getRequestUser(request));
    }

    /**
     * 查询供应商列表
     * @param request
     * @return
     */
    @GetMapping("selectSuppliersList.do")
    public ServerResponse<PageInfo> selectSuppliersList(BaseUserDTO baseUserDTO, HttpServletRequest request){
        return iBaseUserService.selectSuppliersList(baseUserDTO,
                iBaseUserService.getRequestUser(request));
    }


    /**
     * 代理公司用户和医务工作者注册
     * @param baseUserDTO
     * @param request
     * @return
     */
    @PostMapping("userCompanyRegistration.do")
    public ServerResponse<String> userCompanyRegistration(BaseUserDTO baseUserDTO, HttpServletRequest request){
        return iBaseUserService.userCompanyRegistration(baseUserDTO);
    }

}
