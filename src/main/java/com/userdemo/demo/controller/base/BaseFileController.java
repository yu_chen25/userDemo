package com.userdemo.demo.controller.base;

import com.userdemo.demo.base.BaseController;
import com.userdemo.demo.constant.file.OssFileUrl;
import com.userdemo.demo.entity.base.dto.BaseFileDTO;
import com.xiaoleilu.hutool.http.HttpUtil;
import com.xiaoleilu.hutool.json.JSONUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */

@RestController
@RequestMapping("/baseFile/")
public class BaseFileController extends BaseController {

    /**
     * 上传附件
     * @param file
     * @return
     */
    @PostMapping("uploadFile.do")
    public String uploadFile(MultipartFile file){
        Map map = new HashMap(16);
        map.put("file",JSONUtil.toJsonPrettyStr(file));
        return HttpUtil.post(OssFileUrl.URL1,map);
    }

    /**
     * 删除附件
     * @param baseAliyunossFileId
     * @return
     */
    @PostMapping("delFileById.do")
    public String delFileById( Long baseAliyunossFileId){
        Map map = new HashMap(16);
        map.put("baseAliyunossFileId",baseAliyunossFileId);
        return HttpUtil.post(OssFileUrl.URL2,map);
    }

    /**
     * id查询单个附件
     * @param fileId
     * @return
     */
    @GetMapping("selectFileById.do")
    public String selectFileById(Long fileId){
        Map map = new HashMap(16);
        map.put("baseAliyunossFileId",fileId);
        return HttpUtil.get(OssFileUrl.URL3,map);
    }

    /**
     * 查询业务附件
     * @param baseFileDTO
     * @return
     */
    @GetMapping("selectFileVOByParam.do")
    public String selectFileVOByParam(BaseFileDTO baseFileDTO){
        Map map = new HashMap(16);
        map.put("entityName",baseFileDTO.getEntityName());
        map.put("entityType",baseFileDTO.getEntityType());
        map.put("entityId",baseFileDTO.getEntityId());
        return HttpUtil.post(OssFileUrl.URL4,map);
    }

    /**
     * 更新附件业务信息
     * @param baseFileDTO
     * @return
     */
    @PostMapping("updateBusinessData.do")
    public String updateBusinessData(BaseFileDTO baseFileDTO){
        Map map = new HashMap(16);
        map.put("entityName",baseFileDTO.getEntityName());
        map.put("entityType",baseFileDTO.getEntityType());
        map.put("entityId",baseFileDTO.getEntityId());
        map.put("fileIds",baseFileDTO.getFileIds());
        return HttpUtil.post(OssFileUrl.URL5,map);
    }

}
