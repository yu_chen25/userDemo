package com.userdemo.demo.controller.base;

import com.alibaba.fastjson.JSONObject;
import com.userdemo.demo.base.BaseController;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.constant.base.Const;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
@RestController
@RequestMapping("/elasticSearch/")
public class ElasticSearchController extends BaseController {

    /**
     * 测试创建索引
     * @param indexName
     * @return
     */
    @PostMapping("createIndex.do")
    public ServerResponse<String> createIndex(@RequestParam(value = "indexName") String indexName,
                                              HttpServletRequest request){

        return iElasticSearchService.createIndex(indexName);
    }

    /**
     * 测试新增索引数据
     * @param indexName
     * @return
     */
    @PostMapping("addIndexData.do")
    public ServerResponse<String> addIndexData(@RequestParam(value = "indexName") String indexName,
                                              HttpServletRequest request){
        JSONObject object = new JSONObject(16);
        //object.put("id",2);
        //object.put("data","SCBR4.0-38-100-P-NS-0SCBR4.0-3、8-100-P-NS-DAVSCBR4.0-38-100-P、-NS-JB1SCBR4.0-38-100-P-NS-KMP、SCBR4.0-38-100-P-NS-MPASCBR4.0、-38-100-P-NS-SIM1SCBR4.0-38-10、0-P-NS-SIM3SCBR4.0-38-40-P-NS-、KMPSCBR4.0-38-65-P-NS-C2SCBR4.、0-38-65-P-NS-TCSCBR4.0-38-80-P、-NS-RHSCBR4.0-35-65-P-NS-RC2SC、BR4.0-38-100-P-NS-C2SCBR4.0-38、-100-P-NS-H1SCBR4.0-38-100-P-N、S-JB2SCBR4.0-38-100-P-NS-MANSC、BR4.0-38-100-P-NS-MPBSCBR4.0-3、8-100-P-NS-SIM2SCBR4.0-38-100-、P-NS-VERTSCBR4.0-38-65-P-NS-0S、CBR4.0-38-65-P-NS-KMPSCBR4.0-3、8-65-P-NS-TC-BNK");
        //iElasticSearchService.addIndexData(indexName,object);
        return null;

    }

    /**
     * 测试查询所有索引数据
     * @param indexName
     * @return
     */
    @GetMapping("selectAll.do")
    public ServerResponse<Map> selectAll(@RequestParam(value = "indexName") String indexName,
                                         @RequestParam(value = "type") String type,
                                         HttpServletRequest request){
        return iElasticSearchService.selectAll(indexName,type);
    }

    /**
     * 测试添加查询索引数据
     * @param indexName
     * @return
     */
    @GetMapping("selectByMatch.do")
    public ServerResponse<String> selectByMatch(@RequestParam(value = "indexName") String indexName,
                                                @RequestParam(value = "id") String id){
        return iElasticSearchService.selectByMatch(indexName,Const.BaseLogEsIndex.TYPE,id);
    }

    /**
     * 条件删除数据
     * @param indexName
     * @return
     */
    @PostMapping("deleteByMatch.do")
    public ServerResponse<String> deleteByMatch(@RequestParam(value = "indexName") String indexName,
                                                @RequestParam(value = "type") String type,
                                                @RequestParam(value = "id") String id,
                                                HttpServletRequest request){
        return iElasticSearchService.deleteByMatch(indexName,type,id);
    }




}
