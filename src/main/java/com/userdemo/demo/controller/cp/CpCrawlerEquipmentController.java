package com.userdemo.demo.controller.cp;

import com.github.pagehelper.PageInfo;
import com.userdemo.demo.base.BaseController;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.entity.base.vo.BaseUserVO;
import com.userdemo.demo.entity.cp.dto.CpCrawlerEquipmentDTO;
import com.userdemo.demo.entity.cp.vo.BaseAttachmentVO;
import com.userdemo.demo.entity.cp.vo.CpCityNameVO;
import com.userdemo.demo.entity.cp.vo.CpCrawlerConfigurationVO;
import com.userdemo.demo.entity.cp.vo.CrawlerEquipmentSumVO;
import com.userdemo.demo.service.cp.ICpAnnouncementService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/crawler/equipment/")
public class CpCrawlerEquipmentController  extends BaseController {


    @Resource
    private ICpAnnouncementService iCpAnnouncementService;


    /**
     * 查看爬虫数据列表
     * @param cpCrawlerEquipmentDTO
     * @param request
     * @return
     */
    @GetMapping(value = "getCrawlerEquipmentBeforelist.do")
    public ServerResponse<PageInfo> getCrawlerEquipmentBeforelist(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO , HttpServletRequest request){
        return iCpAnnouncementService.getCrawlerEquipmentBeforelist(cpCrawlerEquipmentDTO);
    }


    /**
     * 查看oa招标参数
     * @param cpCrawlerEquipmentDTO
     * @param request
     * @return
     */
    @GetMapping(value = "getOaConfiguration.do")
    public ServerResponse<BaseAttachmentVO> getOaConfiguration(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO , HttpServletRequest request){
        return iCpAnnouncementService.getOaConfiguration(cpCrawlerEquipmentDTO);
    }


    /**
     * 查看爬虫招标参数
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    @GetMapping(value = "getCwConfiguration.do")
    public ServerResponse<List<CpCrawlerConfigurationVO>> getCwConfiguration(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO){
        return iCpAnnouncementService.getCwConfiguration(cpCrawlerEquipmentDTO);
    }


    /**
     * 数据统计
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    @GetMapping(value = "getCrawlerEquipmentSum.do")
    public ServerResponse<CrawlerEquipmentSumVO> getCrawlerEquipmentSum(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO){
        return iCpAnnouncementService.getCrawlerEquipmentSum(cpCrawlerEquipmentDTO);
    }


    /**
     * 获取地区筛选列表
     * @return
     */
    @GetMapping(value = "getCpCityNameVO.do")
    public ServerResponse<List<CpCityNameVO>> getCpCityNameVO(Integer isRelease){
        return iCpAnnouncementService.getCpCityNameVO(isRelease);
    }


    /**
     * 同步型号缩写
     */
    @GetMapping(value = "mpdelAdd.do")
    public ServerResponse<String> mpdelAdd(){
        return iCpAnnouncementService.mpdelAdd();
    }
}
