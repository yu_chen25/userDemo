package com.userdemo.demo.controller.cp;

import com.userdemo.demo.service.cp.ISynEsDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 　　* @description: es数据同步控制层
 * 　　* @param
 * 　　* @return
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $ $
 *
 */

@RestController
@RequestMapping("/synEsData/")
public class SynEsDataController {

    @Autowired
    private ISynEsDataService iSynEsDataService;

    /**
     * 查询OA中标设备表数据
     * @param maxId
     * @param request
     * @return
     */
    @GetMapping(value = "selectAnnouncementEquipment.do")
    public String selectAnnouncementEquipment(@RequestParam(value = "maxId") Long maxId,
                                              @RequestParam(value = "minId") Long minId, HttpServletRequest request){
        return iSynEsDataService.selectAnnouncementEquipment(maxId,minId);
    }

    /**
     * 查询爬虫设备表数据
     * @param maxId
     * @param request
     * @return
     */
    @GetMapping(value = "selectCrawlerEquipment.do")
    public String selectCrawlerEquipment(@RequestParam(value = "maxId") Long maxId,
                                         @RequestParam(value = "minId") Long minId,
                                         HttpServletRequest request){
        return iSynEsDataService.selectCrawlerEquipment(maxId,minId);
    }


    /**
     * 查询爬虫设备表数据
     * @param maxId
     * @return
     */
    @GetMapping(value = "selectAnnouncementEquipmentSuggest.do")
    public String selectAnnouncementEquipmentSuggest(@RequestParam(value = "maxId") Long maxId,
                                                     @RequestParam(value = "minId") Long minId,
                                                     HttpServletRequest request){
        return iSynEsDataService.selectAnnouncementEquipmentSuggest(maxId,minId);
    }

    /**
     * 查询爬虫设备表词汇
     * @param maxId
     * @return
     */
    @GetMapping(value = "selectCrawlerEquipmentSuggest.do")
    public String selectCrawlerEquipmentSuggest(@RequestParam(value = "maxId") Long maxId,
                                                @RequestParam(value = "minId") Long minId,
                                                     HttpServletRequest request){
        return iSynEsDataService.selectCrawlerEquipmentSuggest(maxId,minId);
    }
}
