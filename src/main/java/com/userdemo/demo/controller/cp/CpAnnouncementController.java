package com.userdemo.demo.controller.cp;

import com.github.pagehelper.PageInfo;
import com.userdemo.demo.base.BaseController;
import com.userdemo.demo.common.sys.ServerResponse;
import com.userdemo.demo.entity.base.vo.BaseUserVO;
import com.userdemo.demo.entity.cp.dto.CpCrawlerEquipmentDTO;
import com.userdemo.demo.entity.cp.vo.CpCrawlerConfigurationVO;
import com.userdemo.demo.entity.cp.vo.CpCrawlerEquipmentVO;
import com.userdemo.demo.service.cp.ICpAnnouncementService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/announcement/")
public class CpAnnouncementController  extends BaseController {

    @Resource
    private ICpAnnouncementService  iCpAnnouncementService;


    /**
     * 同步OA中标设备
     * @return
     */
    @GetMapping("addOaAnnouncementEquipment.do")
    public ServerResponse<String> addOaAnnouncementEquipment(){
        return iCpAnnouncementService.addOaAnnouncementEquipment();
    }


    /**
     * 同步爬虫设备数据
     * @return
     */
    @GetMapping("addCrawlerEquipment.do")
    public ServerResponse<String> addCrawlerEquipment() throws ParseException{
        return iCpAnnouncementService.addCrawlerEquipment();
    }


    /**
     * 同步更新爬虫设备数据
     * @return
     * @throws ParseException
     */
    @GetMapping("updateSynchronous.do")
    public ServerResponse<String> updateSynchronous() throws ParseException{
        return iCpAnnouncementService.updateSynchronous();
    }


    /**
     * 爬虫数据分页列表
     * @param cpCrawlerEquipmentDTO
     * @param request
     * @return
     */
    @GetMapping(value = "selectCrawlerEquipmentList.do")
    public ServerResponse<PageInfo> selectCrawlerEquipmentList(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO , HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iCpAnnouncementService.selectCrawlerEquipmentList(cpCrawlerEquipmentDTO,baseUserVO);
    }


    /**
     * 单个查询爬虫数据
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    @GetMapping(value = "selectCrawlerEquipmentById.do")
    public ServerResponse<CpCrawlerEquipmentVO> selectCrawlerEquipmentById(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO){
        return iCpAnnouncementService.selectCrawlerEquipmentById(cpCrawlerEquipmentDTO);
    }


    /**
     * 查询爬虫数据招标配置
     * @param cpCrawlerEquipmentDTO
     * @return
     */
    @GetMapping(value = "selectCrawlerConfigurationById.do")
    public ServerResponse<List<CpCrawlerConfigurationVO>> selectCrawlerConfigurationById(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO){
        return iCpAnnouncementService.selectCrawlerConfigurationById(cpCrawlerEquipmentDTO);
    }


    /**
     * 编辑爬虫数据接口
     * @param cpCrawlerEquipmentDTO
     * @param request
     * @return
     */
    @PostMapping(value = "updateCrawlerEquipment.do")
    public ServerResponse<String> updateCrawlerEquipment(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO,
                                                     HttpServletRequest request) {
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iCpAnnouncementService.updateCrawlerEquipment(cpCrawlerEquipmentDTO,baseUserVO);
    }


    /**
     * 新增爬虫数据接口
     * @param cpCrawlerEquipmentDTO
     * @param request
     * @return
     */
    @PostMapping(value = "xinAddCrawlerEquipment.do")
    public ServerResponse<String> xinAddCrawlerEquipment(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO,
                                                         HttpServletRequest request) {
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iCpAnnouncementService.xinAddCrawlerEquipment(cpCrawlerEquipmentDTO,baseUserVO);
    }


    /**
     * 批量移到回收站，或者批量恢复
     * @param cpCrawlerEquipmentDTO
     * @param request
     * @return
     */
    @PostMapping(value = "updateStatusCrawlerEquipment.do")
    public ServerResponse<String> updateStatusCrawlerEquipment(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO,
                                                         HttpServletRequest request) {
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iCpAnnouncementService.updateStatusCrawlerEquipment(cpCrawlerEquipmentDTO,baseUserVO);
    }


    /**
     * 彻底删除
     * @param cpCrawlerEquipmentDTO
     * @param request
     * @return
     */
    @PostMapping(value = "deleteCrawlerEquipment.do")
    public ServerResponse<String> deleteCrawlerEquipment(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO,
                                                               HttpServletRequest request) {
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iCpAnnouncementService.deleteCrawlerEquipment(cpCrawlerEquipmentDTO,baseUserVO);
    }


    /**
     * 处理数据
     * @param cpCrawlerEquipmentDTO
     * @param request
     * @return
     */
    @PostMapping(value = "setRelease.do")
    public ServerResponse<String> setRelease(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO,
                                                         HttpServletRequest request) {
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iCpAnnouncementService.setRelease(cpCrawlerEquipmentDTO,baseUserVO);
    }


    /**
     * 搜索制造商型号
     * @param cpCrawlerEquipmentDTO
     * @param request
     * @return
     */
    @GetMapping(value = "getManufacturerName.do")
    public ServerResponse<PageInfo> getManufacturerName(CpCrawlerEquipmentDTO cpCrawlerEquipmentDTO , HttpServletRequest request){
        BaseUserVO baseUserVO = iBaseUserService.getRequestUser(request);
        return iCpAnnouncementService.getManufacturerName(cpCrawlerEquipmentDTO,baseUserVO);
    }

}
