package com.userdemo.demo.constant.base;


/**
 * Created by geely
 */
public class Const {


    public static final String LOGINTOKEN = "loginToken";
    public static final String EMAIL = "email";
    public static final String USERNAME = "username";
    public static final String PHONE = "phone";
    public static final String BIDPROJECT = "BIDPROJECT";

    /**
     * 扣费关闭
     */
    public static final int DEDUCTIONOFF = 2;

    /**
     * 扣费打开
     */
    public static final int DEDUCTIONOON = 1;



    /**
     * 平台公司ID
     */
    public static final long PLATID = 1;

    public static final String PLATNAME = "湖南五一招标有限公司";


    /**
     * 小程序通讯标识
     */
    public static final String DATAPLAT = "inside";

    /**
     * 菜单缓存名
     */
    public static final String BASEMENUVO = "USERDEMOBASEMENUVO";

    /**
     * 树状菜单缓存名
     */
    public static final String TREEMENU = "USERDEMOTREEMENU";

    /**
     * 用户登录缓存时间
     */
    public static final long CACHETIME = 1;

    /**
     * 用户菜单缓存时间
     */
    public static final long MENUCACHETIME = 24;

    /**
     * 初始ID主键
     */
    public static final long INITID = 1;

    /**
     * 注册手机短信验证码
     *
     */
    public static final String REFINSTERPHOENCODE = "registerPhoneCode";

    /**
     * 初始页码
     */
    public static final int INITPAGENUM = 1;

    /**
     * 初始展示最大数
     */
    public static final int INITPAGESIZE = 10;


    /**
     * 短信验证码缓存名称
     */
    public interface MessageCodeCacheName{
        String  REGISTERMESSAGE = "registerMessage";
        String UPDATEPASSWORD = "updatePasswordSendMessageCode";
    }

    public interface CodeType{
        String PCLOGINSTRCODE = "pcLoginStrCode";//电脑端网页验证码
        String APPLOGINSTRCODE = "appLoginStrCode";//移动网页验证码
        String MAILSERVERCODE = "mailServerCode";//邮寄申请验证码
        String PHONECODE = "phoneCode";//手机短信验证码
        String NOCODE = "nocode";//没有验证码
    }

    /**
     * 关联类型，1项目，2包
     */
    public interface AssociatedType{
        Integer ONE = 1;
        Integer TWO = 2;
    }

    /**
     * 系统日志ES索引
     */
    public interface BaseLogEsIndex{
       String INDEXNAME = "logbigfield";
       String TYPE = "_doc";
    }
}
