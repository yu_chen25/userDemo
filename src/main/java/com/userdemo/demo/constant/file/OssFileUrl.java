package com.userdemo.demo.constant.file;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 　　* @description:   oss附件管理路径
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
@Component
public class OssFileUrl implements InitializingBean {


    /**
     * 附件上传
     */
    @Value("${file.url1}")
    private String url1;

    /**
     * 附件删除
     */
    @Value("${file.url2}")
    private String url2;

    /**
     * id查询单个附件
     */
    @Value("${file.url3}")
    private String url3;

    /**
     * 业务多个附件信息接口
     */
    @Value("${file.url4}")
    private String url4;

    /**
     * 更新业务附件接口
     */
    @Value("${file.url5}")
    private String url5;

    @Value("${Oss.endpoint}")
    private String endpoint;

    @Value("${Oss.accessKeyId}")
    private String accessKeyId;

    @Value("${Oss.accessKeySecret}")
    private String accessKeySecret;

    @Value("${Oss.yourBucketName}")
    private String yourBucketName;

    @Value("${Oss.filehost}")
    private String filehost;

    @Value("${Oss.fileDownPath}")
    private String fileDownPath;




    public static String URL1;
    public static String URL2;
    public static String URL3;
    public static String URL4;
    public static String URL5;
    public static String ENDPOINT;
    public static String ACCESSKEYID;
    public static String ACCESSKEYSECRET;
    public static String YOURBUCKETNAME;
    public static String FILEHOST;
    public static String FILEDOWNPATH;

    /**
     * Invoked by the containing {@code BeanFactory} after it has set all bean properties
     * and satisfied {@link BeanFactoryAware}, {@code ApplicationContextAware} etc.
     * <p>This method allows the bean instance to perform validation of its overall
     * configuration and final initialization when all bean properties have been set.
     *
     * @throws Exception in the event of misconfiguration (such as failure to set an
     *                   essential property) or if initialization fails for any other reason
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        URL1 = url1;
        URL2 = url2;
        URL3 = url3;
        URL4 = url4;
        URL5 = url5;
        ENDPOINT = endpoint;
        ACCESSKEYID = accessKeyId;
        ACCESSKEYSECRET = accessKeySecret;
        YOURBUCKETNAME = yourBucketName;
        FILEHOST = filehost;
        FILEDOWNPATH = fileDownPath;
    }
}
