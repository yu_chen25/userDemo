package com.userdemo.demo.constant.wx;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 　　* @description: 微信支付配置常量
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
@Component
public class WxPayParameter implements InitializingBean {

    @Value("${wx.pay_pre_order_url}")
    private String preOrderUrl;

    @Value("${wx.appid}")
    private String appid;

    @Value("${wx.mch_id}")
    private String mchId;

    @Value("${wx.pay_notify_url}")
    private String notifyUrl;

    @Value("${wx.trade_type}")
    private String tradeType;

    @Value("${wx.key}")
    private String key;

    @Value("${wx.orderqueryurl}")
    private String orderqueryurl;

    @Value("${wx.ordercloseurl}")
    private String ordercloseurl;


    public static String PREORDERURL;
    public static String APPID;
    public static String MCHID;
    public static String NOTIFYURL;
    public static String TRADETYPE;
    public static String KEY;
    public static String ORDERQUERYURL;
    public static String ORDERCLOSEURL;


    /**
     * Invoked by the containing {@code BeanFactory} after it has set all bean properties
     * and satisfied {@link BeanFactoryAware}, {@code ApplicationContextAware} etc.
     * <p>This method allows the bean instance to perform validation of its overall
     * configuration and final initialization when all bean properties have been set.
     *
     * @throws Exception in the event of misconfiguration (such as failure to set an
     *                   essential property) or if initialization fails for any other reason
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        PREORDERURL = preOrderUrl;
        APPID = appid;
        MCHID = mchId;
        NOTIFYURL = notifyUrl;
        TRADETYPE = tradeType;
        KEY = key;
        ORDERQUERYURL = orderqueryurl;
        ORDERCLOSEURL = ordercloseurl;
    }
}
