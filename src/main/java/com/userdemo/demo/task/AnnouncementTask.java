package com.userdemo.demo.task;


import com.userdemo.demo.base.BaseController;
import com.userdemo.demo.service.cp.ICpAnnouncementService;
import com.userdemo.demo.service.cp.impl.CpAnnouncementServiceImpl;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.ParseException;

@Component
public class AnnouncementTask extends BaseController {


    @Resource
    private CpAnnouncementServiceImpl cpAnnouncementServiceImpl;

    @Resource
    private ICpAnnouncementService iCpAnnouncementService;


    /**
     * 同步oa设备库数据
     */
    @Scheduled(cron="0 0 1 * * ?")
    private void process(){
        cpAnnouncementServiceImpl.addOaAnnouncementEquipment();
    }



//    /**
//     * 同步爬虫数据
//     */
//    @Scheduled(cron="0 0 2 * * ?")
//    private void addCrawlerEquipment() throws ParseException {
//        cpAnnouncementServiceImpl.addCrawlerEquipment();
//    }



    /**
     * 同步爬虫数据型号
     */
    @Scheduled(cron="0 0 2 * * ?")
    private void mpdelAdd()  {
        iCpAnnouncementService.mpdelAdd();
    }
}
