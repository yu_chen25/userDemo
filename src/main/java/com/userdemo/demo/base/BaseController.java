package com.userdemo.demo.base;

import com.userdemo.demo.service.base.*;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseController {

    /**
     * 用户服务层
     */
    @Autowired
    protected IBaseUserService iBaseUserService;


    /**
     * 角色服务层
     */
    @Autowired
    protected IBaseRoleService iBaseRoleService;


    /**
     * 菜单服务层
     */
    @Autowired
    protected IBaseMenuService iBaseMenuService;

    /**
     * 枚举服务层
     */
    @Autowired
    protected IPaParameterService iPaParameterService;


    /**
     * 系统日志服务层
     */
    @Autowired
    protected IBaseLogService iBaseLogService;

    @Autowired
    protected IBaseOrgMechanismService iBaseOrgMechanismService;

    @Autowired
    protected IElasticSearchService iElasticSearchService;

}
