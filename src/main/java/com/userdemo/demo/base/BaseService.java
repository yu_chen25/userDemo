package com.userdemo.demo.base;


import com.userdemo.demo.dao.base.*;

import com.userdemo.demo.dao.cp.SynEsDataMapper;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseService {

    /**
     * 用户mapper
     */
    @Autowired
    protected BaseUserMapper baseUserMapper;

    /**
     * 角色mapper
     */
    @Autowired
    protected BaseRoleMapper baseRoleMapper;

    /**
     * 用户角色mapper
     */
    @Autowired
    protected BaseUserRoleMapper baseUserRoleMapper;


    /**
     * 用户角色mapper
     */
    @Autowired
    protected BaseMenuMapper baseMenuMapper;

    /**
     * 用户菜单mapper
     */
    @Autowired
    protected BaseUserMenuMapper baseUserMenuMapper;


    /**
     * 枚举类型mapper
     */
    @Autowired
    protected PaParameterSortMapper paParameterSortMapper;

    /**
     * 枚举参数mapper
     */
    @Autowired
    protected PaParameterMapper paParameterMapper;


    /**
     * 日志mapper
     */
    @Autowired
    protected BaseLogMapper baseLogMapper;

    /**
     * 日志大字段mapper
     */
    @Autowired
    protected BaseLogBigfieldMapper baseLogBigfieldMapper;

    @Autowired
    protected BaseRoleMechanismMapper baseRoleMechanismMapper;

    @Autowired
    protected BaseOrgMechanismHistoricalMapper baseOrgMechanismHistoricalMapper;

    @Autowired
    protected BaseOrgMechanismMapper baseOrgMechanismMapper;

    @Autowired
    protected SynEsDataMapper synEsDataMapper;
}
