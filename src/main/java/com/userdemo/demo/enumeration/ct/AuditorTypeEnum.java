package com.userdemo.demo.enumeration.ct;

/**
 * 　　* @description: 审核人类型(1.部门,2采购人用户,3供应商用户)
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public enum  AuditorTypeEnum {

    DEPT(1,"部门"),
    PURCHASERUSER(2,"采购人用户"),
    SUPPLIERS(3,"供应商用户"),
    ;

    private final int code;
    private final String desc;


    AuditorTypeEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
