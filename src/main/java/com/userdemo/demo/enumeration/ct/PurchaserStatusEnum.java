package com.userdemo.demo.enumeration.ct;

/**
 * 　　* @description: 采购人合同发布状态(1.未处理,2.已发布,3.已起草，papa_id:48)
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public enum PurchaserStatusEnum {

    UNTREATED(1,"未处理"),
    LANCHED(2,"已发布"),
    DRAFTED(3,"已起草"),
    ;

    private final int code;
    private final String desc;


    PurchaserStatusEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
