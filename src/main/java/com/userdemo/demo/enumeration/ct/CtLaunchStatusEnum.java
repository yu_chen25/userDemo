package com.userdemo.demo.enumeration.ct;

/**
 * 　　* @description: 供应商合同状态(1.待起草,2.审批中,3.未通过,4.已通过,5.待审批,papa_id:49)
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public enum CtLaunchStatusEnum {

    ONE(1,"待起草"),
    TWO(2,"审批中"),
    THREE(3,"未通过"),
    FOUR(4,"已通过"),
    FIVE(5,"待审批"),
    ;

    private final int code;
    private final String desc;


    CtLaunchStatusEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
