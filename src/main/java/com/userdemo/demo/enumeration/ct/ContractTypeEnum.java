package com.userdemo.demo.enumeration.ct;

/**
 * 　　* @description: 合同类型（1.院内,2.院外）
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public enum ContractTypeEnum {

    ONE(1,"院内"),
    TWO(2,"院外"),
    ;

    private final int code;
    private final String desc;


    ContractTypeEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }
}
