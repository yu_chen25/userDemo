package com.userdemo.demo.enumeration.mp;

public enum PdfTransResultEnum {
	//处理状态(0:未处理,1:处理完成,2:处理失败)
	NO_DEAL(0,"未处理"),
	DEAL_SUCCESS(1,"处理完成"),
	DEAL_FAIL(2,"处理失败");
	
	private final Integer code;
    private final String desc;

    PdfTransResultEnum(Integer code,String desc){
    	this.code = code;
    	this.desc = desc;
    }
    
    public Integer getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
