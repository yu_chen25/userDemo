package com.userdemo.demo.enumeration.mp;

public enum HandleTypeEnum {
	
	NEW_MAKE(1,"新建"),
	SUPPLY_AGAIN(2,"补发"),
	AGAIN_MAKE(3,"重新招标");
	
	private final int code;
    private final String desc;

    HandleTypeEnum(int code,String desc){
    	this.code = code;
    	this.desc = desc;
    }
    
    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
