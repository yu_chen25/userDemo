package com.userdemo.demo.enumeration.mp;

public enum BtreviewType {

	PRICE(1,"价格"),
	BUSINESS(2,"商务"),
	TECHNOLOGY(3,"技术");
	
	private final Integer code;
    private final String desc;

    BtreviewType(Integer code,String desc){
    	this.code = code;
    	this.desc = desc;
    }
    
    public Integer getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }
}
