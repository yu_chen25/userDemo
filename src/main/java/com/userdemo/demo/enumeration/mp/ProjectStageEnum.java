package com.userdemo.demo.enumeration.mp;

public enum ProjectStageEnum {
	//（1.招标，2.投标，3.开标，4.评标，5.定标）
	INVITE_TENDERS(1,"招标"),
	TENDER(2,"投标"),
	OPNE_TENDER(3,"开标"),
	EVALUATION_OF_BID(4,"评标"),
	CALIBRATE(5,"定标");
	
	private final Integer code;
    private final String desc;

    ProjectStageEnum(Integer code,String desc){
    	this.code = code;
    	this.desc = desc;
    }
    
    public Integer getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
