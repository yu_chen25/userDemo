package com.userdemo.demo.enumeration.mp;

public enum ProjectStatusEnum {
	//状态（1.待发布，2.投标，3.开标，4.评标，5.定标，6.已完成，7.已撤项,PAPA_ID:'26'）
	READ_PUBLIC(1,"待发布"),
	TENDER(2,"投标"),
	OPEN_TENDER(3,"开标"),
	EVALUATION_OF_BID(4,"评标"),
	COMPLETED(5,"定标"),
	HAV_CANCLE(6,"已完成"),
	BACK(7,"已撤项"),;
	
	private final Integer code;
    private final String desc;

    ProjectStatusEnum(Integer code,String desc){
    	this.code = code;
    	this.desc = desc;
    }
    
    public Integer getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
