package com.userdemo.demo.enumeration.mp;

public enum BtreviewPriceType {

	TYPE1(1,"算法1"),
	TYPE2(2,"算法2"),
	TYPE3(3,"算法3");
	
	private final Integer code;
    private final String desc;

    BtreviewPriceType(Integer code,String desc){
    	this.code = code;
    	this.desc = desc;
    }
    
    public Integer getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }
}
