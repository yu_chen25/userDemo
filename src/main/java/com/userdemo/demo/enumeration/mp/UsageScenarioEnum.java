package com.userdemo.demo.enumeration.mp;

public enum UsageScenarioEnum {
		OPEN_TENDER_BASE("0","开标一览表基本字段"),
		OPEN_TENDER("1","开标一览表使用");
		
		private final String code;
	    private final String desc;

	    UsageScenarioEnum(String code,String desc){
	    	this.code = code;
	    	this.desc = desc;
	    }
	    
	    public String getCode(){
	        return code;
	    }
	    public String getDesc(){
	        return desc;
	    }
}
