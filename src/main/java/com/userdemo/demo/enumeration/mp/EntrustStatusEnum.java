package com.userdemo.demo.enumeration.mp;

public enum EntrustStatusEnum {
	
	READ_ACCEPT(1,"待接受"),
	HAV_ACCEPT(2,"已接受"),
	HAV_BACK(3,"已退回"),
	HAV_CANCEL(4,"已撤回");
	
	private final Integer code;
    private final String desc;

    EntrustStatusEnum(Integer code,String desc){
    	this.code = code;
    	this.desc = desc;
    }
    
    public Integer getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
