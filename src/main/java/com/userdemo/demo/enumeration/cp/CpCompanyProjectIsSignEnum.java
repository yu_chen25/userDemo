package com.userdemo.demo.enumeration.cp;

/**
 * 　　* @description: 状态  1:报名,2:中标，3:评审,4:其他,papa_id:23
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public enum CpCompanyProjectIsSignEnum {
    SIGNUP(1,"报名"),
    WINBID(2,"中标"),
    REVIEW(3,"评审"),
    OTHER(4,"其他,"),
    ;

    private final int code;
    private final String desc;


    CpCompanyProjectIsSignEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
