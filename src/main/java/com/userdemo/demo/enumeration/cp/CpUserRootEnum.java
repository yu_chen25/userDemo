package com.userdemo.demo.enumeration.cp;

/**
 * 用户权限枚举
 */
public enum CpUserRootEnum {

    ROOT(1,"管理员"),
    DEPTLEADER(2,"部门负责人"),
    GROUPLEADER(3,"小组负责人"),
    PROJECTLEADER(4,"项目负责人"),
    USER(5,"普通员工"),
    ;
    private final int code;
    private final String desc;


    CpUserRootEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }
}
