package com.userdemo.demo.enumeration.cp;

/**
 * 项目状态枚举
 */
public enum CpProjectStatusEnum {
    BACKLOG(1,"待办"),
    END(2,"完成"),
    ABANDON(3,"放弃"),
    ;

    private final int code;
    private final String desc;


    CpProjectStatusEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
