package com.userdemo.demo.enumeration.cp;

public enum CpProjectIncomeEnum {

    FILECOST(1,"招标文件费"),
    AGENCYCOST(2,"招标代理费"),
    IMPORTCOST(3,"进口论证费"),
    NUMBERCOST(4,"数字CA费"),
    ;

    private final int code;
    private final String desc;


    CpProjectIncomeEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }
}
