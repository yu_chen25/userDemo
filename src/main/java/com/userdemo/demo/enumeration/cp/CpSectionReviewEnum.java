package com.userdemo.demo.enumeration.cp;

public enum CpSectionReviewEnum {

    PASS(2,"中标"),
    REJECT(1,"废标"),
    ;
    private final int code;
    private final String desc;


    CpSectionReviewEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }
}
