package com.userdemo.demo.enumeration.cp;

public enum CpProjectTypeEnum {
    GUONEI(1,"国内招标"),
    GUOJI(2,"国际招标"),
    ZHENGFU(3,"政府采购"),
    DIANZI(4,"电子平台"),
    ;

    private final int code;
    private final String desc;


    CpProjectTypeEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
