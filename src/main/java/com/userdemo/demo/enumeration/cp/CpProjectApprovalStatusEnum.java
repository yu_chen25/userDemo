package com.userdemo.demo.enumeration.cp;

/**
 * 　　* @description: 审批状态枚举 papa_id:11
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public enum CpProjectApprovalStatusEnum {
    PASS(1,"通过"),
    REJECT(2,"驳回"),
    AUDITED(3,"待审核"),
    SAVE(4,"保存"),
    ;

    private final int code;
    private final String desc;


    CpProjectApprovalStatusEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
