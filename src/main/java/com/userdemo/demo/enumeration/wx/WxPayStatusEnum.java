package com.userdemo.demo.enumeration.wx;

/**
 * 　　* @description: 微信支付订单状态
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public enum WxPayStatusEnum {

    CANCELLED(0,"已取消"),
    UNPAID(1,"未付款"),
    PAID(2,"已付款"),
    ;

    private final int code;
    private final String desc;


    WxPayStatusEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }
}
