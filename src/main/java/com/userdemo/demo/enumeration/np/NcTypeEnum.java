package com.userdemo.demo.enumeration.np;

/**
 * 　　* @description: 步骤类型（1:评标，2:定标）
 * 　　* @param
 * 　　* @return
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $ $
 *
 */
public enum NcTypeEnum {

    ONE(1,"评标"),
    TWO(2,"定标"),;

    private final Integer code;
    private final String desc;

    NcTypeEnum(Integer code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }
}
