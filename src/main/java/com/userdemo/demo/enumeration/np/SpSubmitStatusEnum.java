package com.userdemo.demo.enumeration.np;

/**
 * 　　* @description: 提交状态（1:未提交,2:已提交,3:已结束。papa_id:58）
 * 　　* @param
 * 　　* @return
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $ $
 *
 */
public enum SpSubmitStatusEnum {

    ONE(1,"未提交"),
    TWO(2,"已提交"),
    THREE(3,"已结束"),
    ;

    private final Integer code;
    private final String desc;

    SpSubmitStatusEnum(Integer code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
