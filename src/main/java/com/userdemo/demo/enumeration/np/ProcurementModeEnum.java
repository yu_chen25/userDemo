package com.userdemo.demo.enumeration.np;

/**
 * 　　* @description: 采购形式（1.招标，2.竞价，papa_id:30）
 * 　　* @param
 * 　　* @return
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $ $
 *
 */
public enum ProcurementModeEnum {

    ONE(1,"招标"),
    TWO(2,"竞价"),;

    private final Integer code;
    private final String desc;

    ProcurementModeEnum(Integer code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
