package com.userdemo.demo.enumeration.base;

/**
 * 　　* @description: TODO
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public enum DataStatusEnum {

    NORMAL(1,"正常"),
    INVALID(2,"失效"),
    ;

    private final int code;
    private final String desc;


    DataStatusEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
