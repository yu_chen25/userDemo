package com.userdemo.demo.enumeration.base;

/**
 * 　　* @description: 是否
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public enum YesOrNoEnum {
    YES(1,"是"),
    NO(2,"否"),
    ;

    private final int code;
    private final String desc;


    YesOrNoEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }
}
