package com.userdemo.demo.enumeration.base;

/**
 * 　　* @description: 适用类型枚举 papa_id 21
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public enum UseTypeEnum {

    PLATFORM(1,"平台"),
    HOSPITAL(2,"医院"),
    AC(3,"代理公司"),
    SUPPLIERS(4,"供应商"),
    MEDICAL(5,"医务工作者"),
    ;

    private final int code;
    private final String desc;


    UseTypeEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }
}
