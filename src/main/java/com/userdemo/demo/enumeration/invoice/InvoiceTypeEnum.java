package com.userdemo.demo.enumeration.invoice;

/**
 * 　　* @description: 发票类型枚举 papa_id 24
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public enum InvoiceTypeEnum {
    ORDINARY(1,"普通发票"),
    SPECIAL(2,"驳回"),
    ;

    private final int code;
    private final String desc;


    InvoiceTypeEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
