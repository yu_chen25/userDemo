package com.userdemo.demo.enumeration.syn;

/**
 * 　　* @description: 同步数据类型 papa_id:20
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public enum  SynDataTypeEnum {

    ONE(1,"小程序报名费用"),
    TWO(2,"耗材平台报名费用"),
    THREE(3,"耗材平台CA新办费用"),
    FOUR(4,"耗材平台CA续费费用"),
    ;

    private final int code;
    private final String desc;


    SynDataTypeEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
