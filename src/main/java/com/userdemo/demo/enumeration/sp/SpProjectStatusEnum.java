package com.userdemo.demo.enumeration.sp;

/**
 * 　　* @description: 报名项目状态（1.投标，2.开标，3.已中标，4.未中标，5.已撤项，papa_id:34）
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public enum SpProjectStatusEnum {

    BID(1,"待审核"),
    OPEN_BID(2,"开标"),
    HAVE_BID(3,"已中标"),
    UN_BID(4,"未中标"),
    WITHDRAWAL(5,"已撤项"),
    ;
    private final Integer code;
    private final String desc;

    SpProjectStatusEnum(Integer code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }


}
