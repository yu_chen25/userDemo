package com.userdemo.demo.enumeration.sp;

/**
 * 　　* @description: 报名状态(1.待审核，2.未通过，3.待付款，4.报名成功，papa_id:33)
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public enum EnrollmentStatusEnum {

    AUDITED(1,"待审核"),
    NOT_PASS(2,"未通过"),
    HAVE_PASSED(3,"待付款"),
    SUCCESS(4,"报名成功"),
    ;
    private final Integer code;
    private final String desc;

    EnrollmentStatusEnum(Integer code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
