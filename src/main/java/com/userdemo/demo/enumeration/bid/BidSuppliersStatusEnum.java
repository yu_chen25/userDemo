package com.userdemo.demo.enumeration.bid;

/**
 * 　　* @description: 竞价状态1.进行中,2.已截止,3.已成交,4.未成交,5.已撤销(papa_id:55)
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public enum BidSuppliersStatusEnum {

    ONE(1,"进行中"),
    TWO(2,"已截止"),
    THREE(3,"已成交"),
    FOUR(4,"未成交"),
    FIVE(5,"已撤销"),
    ;

    private final int code;
    private final String desc;


    BidSuppliersStatusEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
