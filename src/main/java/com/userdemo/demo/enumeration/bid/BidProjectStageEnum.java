package com.userdemo.demo.enumeration.bid;

/**
 * 　　* @description: 竞价项目阶段(1.发布,2.竞价,3.定价（papa_id:54）)
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public enum BidProjectStageEnum {

    RELEASE(1,"发布"),
    BIDDING_PRICE(2,"竞价"),
    SET_PRICE(3,"待定价"),
    ;

    private final int code;
    private final String desc;


    BidProjectStageEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
