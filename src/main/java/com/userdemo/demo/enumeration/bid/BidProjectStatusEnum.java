package com.userdemo.demo.enumeration.bid;

/**
 * 　　* @description: 竞价项目状态(1.待发布,2.竞价中,3.待定价,4.已完成,5.已撤项(papa_id:53))
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public enum BidProjectStatusEnum {

    TO_BE_RELEASED(1,"待发布"),
    BIDDING(2,"竞价中"),
    PENDING_PRICING(3,"待定价"),
    COMPLETED(4,"已完成"),
    WITHDRAWAL(5,"已撤项"),
            ;

    private final int code;
    private final String desc;


    BidProjectStatusEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
