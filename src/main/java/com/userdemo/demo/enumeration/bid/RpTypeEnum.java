package com.userdemo.demo.enumeration.bid;

/**
 * 　　* @description: 最小降价幅度类型 1.按金额,2按比例（papa_id:50）
 * 　　* @param ${tags}
 * 　　* @return ${return_type}
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $date$ $time$
 *
 */
public enum RpTypeEnum {
    ONE(1,"按金额"),
    TWO(2,"2按比例"),
    ;

    private final int code;
    private final String desc;


    RpTypeEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }
}
